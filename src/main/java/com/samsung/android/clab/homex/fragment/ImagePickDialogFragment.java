package com.samsung.android.clab.homex.fragment;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Toast;

import com.samsung.android.clab.homex.R;
import com.soundcloud.android.crop.Crop;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;

public class ImagePickDialogFragment extends DialogFragment {

    private static final int PICK_FROM_CAMEAR       = 0;
    private static final int PICK_FROM_GALLERY      = 1;

    private OnPickCompleteListener mListener;
    private File profileIconFile;

    interface OnPickCompleteListener {
        void onCompleted(File profileFile);
    }

    public static ImagePickDialogFragment getInstance() {
        return new ImagePickDialogFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_pick_image, container, false);
        ButterKnife.bind(this, view);

        if(getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //ask write permission
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED ||
            ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, 2121);

            return;
        }
    }

    @OnClick({R.id.buttonGallery, R.id.buttonCamera, R.id.buttonCancel})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonGallery:
                getImageFromAlbum();
                break;

            case R.id.buttonCamera:
                getImageFromCamera();
                break;

            case R.id.buttonCancel:
                dismiss();
                break;

            default:
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if(resultCode != RESULT_OK) {
            Toast.makeText(getActivity(), "취소 되었습니다.", Toast.LENGTH_SHORT).show();

            if(profileIconFile != null) {
                if (profileIconFile.exists()) {
                    if (profileIconFile.delete()) {
                        Log.d("Log", profileIconFile.getAbsolutePath() + " 삭제 성공");
                        profileIconFile = null;
                    }
                }
            }
            return;
        }

        if(requestCode == PICK_FROM_CAMEAR) {
            Uri fileUri = FileProvider.getUriForFile(getActivity(), "com.samsung.android.clab.homex.fileprovider", profileIconFile);
            cropImage(fileUri);
        } else if(requestCode == PICK_FROM_GALLERY && intent != null) {
            Uri photoUri = intent.getData();
            if(photoUri != null) {
                cropImage(photoUri);
            }

//            Cursor cursor = null;
//
//            if(photoUri != null) {
//                try {
//                    // Uri 스키마를 content:// 에서 file://로 변경
//                    String[] proj = {MediaStore.Images.Media.DATA};
//                    cursor = getActivity().getContentResolver().query(photoUri, proj, null, null, null);
//
//                    assert cursor != null;
//                    int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
//
//                    cursor.moveToFirst();
//
//                    profileIconFile = new File(cursor.getString(column_index));
//                } catch (Exception e) {
//                    e.printStackTrace();
//                } finally {
//                    if(cursor != null) {
//                        cursor.close();
//                    }
//
//                    mListener.onCompleted(profileIconFile);
//                    dismiss();
//                }
//            }
        } else if(requestCode == Crop.REQUEST_CROP) {
            mListener.onCompleted(profileIconFile);
            dismiss();
        }
    }

    public void setListener(OnPickCompleteListener listener) {
        this.mListener = listener;
    }

    /**
     * 갤러리에서 사진을 가져옴
     */
    private void getImageFromAlbum() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
        startActivityForResult(intent, PICK_FROM_GALLERY);
    }

    /**
     * 사진 촬영
     */
    private void getImageFromCamera() {
        try {
            profileIconFile = createImageFile();
        } catch (IOException e) {
            Toast.makeText(getActivity(), "이미지 처리 오류! 다시 시도해주세요.", Toast.LENGTH_SHORT).show();
            getActivity().finish();
            e.printStackTrace();
        }

        Uri fileUri = FileProvider.getUriForFile(getActivity(), "com.samsung.android.clab.homex.fileprovider", profileIconFile);
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        startActivityForResult(intent, PICK_FROM_CAMEAR);
    }

    /**
     * 사진파일 생성
     * @return
     */
    private File createImageFile() throws IOException {
        // 이미지 파일 이름 ( temp_{시간}_ )
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "temp_" + timeStamp + "_";

        // 이미지가 저장될 폴더 이름 ( temp )
        File storageDir = new File(Environment.getExternalStorageDirectory() + "/temp/");
        if (!storageDir.exists()) storageDir.mkdirs();

        // 빈 파일 생성
        File image = File.createTempFile(imageFileName, ".png", storageDir);

        return image;
    }

    /**
     * 이미지를 크롭한다.
     * @param photoUri
     */
    private void cropImage(Uri photoUri) {
        Log.d("Log", "profileIconFile : " + profileIconFile);

        /**
         * 갤러리에서 선택한 경우에는 profileIconFile이 없으므로 새로 생성해야 함.
         */
        if(profileIconFile == null) {
            try {
                profileIconFile = createImageFile();
            } catch (Exception e) {
                Toast.makeText(getActivity(), "이미지 처리 오류! 다시 시도해주세요.", Toast.LENGTH_SHORT).show();
                getActivity().finish();
                e.printStackTrace();
            }
        }

        // 크롭 후 저장할 Uri
        Uri uri = FileProvider.getUriForFile(getActivity(), "com.samsung.android.clab.homex.fileprovider", profileIconFile);
        Crop.of(photoUri, uri).asSquare().start(getActivity(), ImagePickDialogFragment.this);

    }
}
