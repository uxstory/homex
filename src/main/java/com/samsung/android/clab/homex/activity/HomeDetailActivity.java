package com.samsung.android.clab.homex.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.samsung.android.clab.homex.CardViewPager;
import com.samsung.android.clab.homex.R;
import com.samsung.android.clab.homex.adapter.HomeDetailAdapter;
import com.samsung.android.clab.homex.fragment.HomeDetailFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeDetailActivity extends FragmentActivity implements HomeDetailFragment.Listener {

    @BindView(R.id.viewPager)
    CardViewPager mViewPager;

    private HomeDetailAdapter mAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_detail);
        ButterKnife.bind(this);

        initViewPager();
    }

    @Override
    public void onConfirm() {
        finish();
    }

    @Override
    public void onDelete(int position) {
        Log.d("Log", "delete position : " + position);
        mAdapter.removePage(position);
    }

    private void initViewPager() {
        mAdapter = new HomeDetailAdapter(getSupportFragmentManager(), this);
        mViewPager.setAdapter(mAdapter);
        mViewPager.setAnimationEnabled(true);
        mViewPager.setFadeEnabled(true);
        mViewPager.setFadeFactor(0.6f);
    }
}
