package com.samsung.android.clab.homex.fragment;

import android.content.res.XmlResourceParser;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.samsung.android.clab.homex.Course;
import com.samsung.android.clab.homex.CourseWorkout;
import com.samsung.android.clab.homex.R;
import com.samsung.android.clab.homex.Workout;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HomeDetailFragment extends Fragment {

    @BindView(R.id.imageViewIcon) ImageView imageViewIcon;
    @BindView(R.id.textViewDate) TextView textViewDate;
    @BindView(R.id.textViewTitle) TextView textViewTitle;
    @BindView(R.id.textViewKcal) TextView textViewKcal;
    @BindView(R.id.textViewResultSuccessCnt) TextView textViewResultSuccessCnt;
    @BindView(R.id.textViewResultRepeatCnt) TextView textViewResultRepeatCnt;
    @BindView(R.id.textViewLowHeartRate) TextView textViewLowHeartRate;
    @BindView(R.id.textViewAvgHeartRate) TextView textViewAvgHeartRate;
    @BindView(R.id.textViewMaxHeartRate) TextView textViewMaxHeartRate;
    @BindView(R.id.workout) LinearLayout courseWorkout;
    @BindView(R.id.bpmGraph) LineChart bpmGraph;

    private ArrayList<Workout> workouts;
    private ArrayList<Integer> heartBeats;
    private Course course;
    private Workout workout;
    private int repeatCnt = 0;
    private ArrayList<Integer> bpmData = new ArrayList<>();
    private int min;
    private int max;
    private int sum;
    private int position;

    private Listener mListener;

    public static HomeDetailFragment newInstance(int position) {
        HomeDetailFragment fragment = new HomeDetailFragment();

        Bundle bundle = new Bundle();
        bundle.putInt("position", position);
        fragment.setArguments(bundle);

        return fragment;
    }

    public interface Listener {
        void onConfirm();
        void onDelete(int position);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ViewGroup view = (ViewGroup) inflater.inflate(R.layout.fragment_home_detail_item,container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if(getArguments() != null) {
            position = getArguments().getInt("position");
            Log.d("Log", "fragment position : " + position);
        }

        course = readCourse(getResources().getXml(R.xml.course_balance_stretching_full));

        courseWorkout.removeAllViews();
        for(int i = 0 ; i < course.getWorkouts().size() ; i++) {
            repeatCnt += course.getWorkouts().get(i).getRepeat();
            courseWorkout.addView(new CourseWorkout(getActivity(), course.getWorkouts().get(i), "5"));
        }

        bpmData.add(72);
        bpmData.add(100);
        bpmData.add(130);
        bpmData.add(125);
        bpmData.add(155);
        bpmData.add(187);
        bpmData.add(190);
        bpmData.add(100);
        bpmData.add(110);
        bpmData.add(118);

        min = bpmData.get(0);
        max = bpmData.get(0);
        for(int i=0; i < bpmData.size() ; i++) {
            if(max < bpmData.get(i)) {
                max = bpmData.get(i);
            }

            if(min > bpmData.get(i)) {
                min = bpmData.get(i);
            }

            sum += bpmData.get(i);
        }

        textViewResultRepeatCnt.setText(String.valueOf(repeatCnt));
        textViewResultSuccessCnt.setText("5");
        textViewLowHeartRate.setText(String.valueOf(min));
        textViewMaxHeartRate.setText(String.valueOf(max));
        textViewAvgHeartRate.setText(String.valueOf(sum / bpmData.size()));

        drawBPMGraph(bpmData);
    }

    @OnClick({R.id.buttonDel, R.id.buttonConfirm})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonDel:
                if(mListener != null) {
                    mListener.onDelete(position);
                }
                break;

            case R.id.buttonConfirm:
                if(mListener != null) {
                    mListener.onConfirm();
                }
                break;

            default:
                break;
        }
    }

    public void setListener(Listener listener) {
        this.mListener = listener;
    }

    private void drawBPMGraph(ArrayList<Integer> data) {
        ArrayList<Entry> entries = new ArrayList<>();
        for(int i = 0 ; i < data.size() ; i++) {
            entries.add(new Entry(i, data.get(i)));
        }

        LineDataSet lineDataSet = new LineDataSet(entries, "심박수");
        lineDataSet.setLineWidth(1);
        lineDataSet.setColor(Color.parseColor("#333333"));
        lineDataSet.setCircleHoleColor(Color.parseColor("#333333"));
        lineDataSet.setCircleColor(Color.parseColor("#333333"));

        LineData lineData = new LineData(lineDataSet);
        bpmGraph.setData(lineData);

        XAxis xAxis = bpmGraph.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setTextColor(Color.BLACK);
        xAxis.enableGridDashedLine(8, 24, 0);
        xAxis.setDrawLabels(false);

        YAxis yLAxis = bpmGraph.getAxisLeft();
        yLAxis.setTextColor(Color.BLACK);

        YAxis yRAxis = bpmGraph.getAxisRight();
        yRAxis.setDrawLabels(false);
        yRAxis.setDrawAxisLine(false);
        yRAxis.setDrawGridLines(false);

        Description description = new Description();
        description.setText("");

        bpmGraph.setDescription(description);
        bpmGraph.setDoubleTapToZoomEnabled(false);
        bpmGraph.setDrawGridBackground(false);
        bpmGraph.getAxisRight().setDrawGridLines(false);
        bpmGraph.getAxisLeft().setDrawGridLines(false);
        bpmGraph.getXAxis().setDrawGridLines(false);
        bpmGraph.getData().setHighlightEnabled(false);
        bpmGraph.setTouchEnabled(false);
        bpmGraph.invalidate();
    }

    public Course readCourse(XmlResourceParser parser)
    {
        Log.d("START", "read Course Start");
        try {
            int eventType = parser.getEventType();
            boolean isEnd = false;
            boolean isCos = false;
            boolean isRest = false;

            boolean isHeart = false;

            while (eventType != XmlResourceParser.END_DOCUMENT && isEnd == false) {
                String tagname;

                switch(eventType)
                {
                    case XmlResourceParser.START_TAG:
                        tagname = parser.getName();

                        if(tagname != null)
                        {
                            Log.d("tagName", tagname);
                        }

                        if(tagname.equalsIgnoreCase("course"))
                        {
                            course = new Course();
                            workouts = new ArrayList<>();
                            heartBeats = new ArrayList<>();

                            isCos = true;
                        }

                        else if(tagname.equalsIgnoreCase("rest"))
                        {
                            isRest = true;
                        }
                        else if(tagname.equalsIgnoreCase("heartbeat"))
                        {
                            isCos = false;
                            isHeart = true;
                        }

                        else if(tagname.equalsIgnoreCase("param"))
                        {
                            if(isCos)
                            {
                                String paramName = parser.getAttributeValue(0);
                                String paramValue;

                                if(paramName.equalsIgnoreCase("title"))
                                {
                                    paramValue = parser.getAttributeValue(null, "value");
                                    course.setTitle(paramValue);
                                }
                                else if(paramName.equalsIgnoreCase("desc"))
                                {
                                    paramValue = parser.getAttributeValue(null, "value");
                                    course.setDesc(paramValue);
                                }
                                else if(paramName.equalsIgnoreCase("level"))
                                {
                                    paramValue = parser.getAttributeValue(null, "value");
                                    course.setLevel(paramValue);
                                }
                                else if(paramName.equalsIgnoreCase("steps"))
                                {
                                    course.setStep(parser.getAttributeIntValue(null, "value", 0));
                                }
                                else if(paramName.equalsIgnoreCase("icon"))
                                {
                                    paramValue = parser.getAttributeValue(null, "value");
                                    course.setIcon(paramValue);
                                }
                                else if(paramName.equalsIgnoreCase("video"))
                                {
                                    paramValue = parser.getAttributeValue(null, "value");
                                    course.setVideo(paramValue);
                                }
                                else if(paramName.equalsIgnoreCase("set"))
                                {
                                    course.setSet(parser.getAttributeIntValue(null, "value", 0));
                                }

                            }
                            else if(isRest)
                            {
                                String paramName = parser.getAttributeValue(0);

                                if(paramName.equalsIgnoreCase("resttime"))
                                {
                                    course.setResttime(parser.getAttributeIntValue(null, "value", 0));
                                }
                                else if(paramName.equalsIgnoreCase("desc"))
                                {
                                    course.setRestdesc(parser.getAttributeValue(null, "value"));
                                }
                            }
                            else if(isHeart)
                            {
                                String paramName = parser.getAttributeValue(0);

                                if(paramName.equalsIgnoreCase("1"))
                                {
                                    heartBeats.add(parser.getAttributeIntValue(null, "value", 0));
                                }

                                else if(paramName.equalsIgnoreCase("2"))
                                {
                                    heartBeats.add(parser.getAttributeIntValue(null, "value", 0));
                                }

                                else if(paramName.equalsIgnoreCase("3"))
                                {
                                    heartBeats.add(parser.getAttributeIntValue(null, "value", 0));
                                }

                                else if(paramName.equalsIgnoreCase("4"))
                                {
                                    heartBeats.add(parser.getAttributeIntValue(null, "value", 0));
                                }

                                else if(paramName.equalsIgnoreCase("5"))
                                {
                                    heartBeats.add(parser.getAttributeIntValue(null, "value", 0));
                                }

                                else if(paramName.equalsIgnoreCase("6"))
                                {
                                    heartBeats.add(parser.getAttributeIntValue(null, "value", 0));
                                }

                                else if(paramName.equalsIgnoreCase("7"))
                                {
                                    heartBeats.add(parser.getAttributeIntValue(null, "value", 0));
                                }
                                else if(paramName.equalsIgnoreCase("8"))
                                {
                                    heartBeats.add(parser.getAttributeIntValue(null, "value", 0));
                                }
                                else if(paramName.equalsIgnoreCase("9"))
                                {
                                    heartBeats.add(parser.getAttributeIntValue(null, "value", 0));
                                }
                                else if(paramName.equalsIgnoreCase("10"))
                                {
                                    heartBeats.add(parser.getAttributeIntValue(null, "value", 0));
                                }
                                else
                                {

                                }
                            }
                            else
                            {
                                // workout param
                                String paramName = parser.getAttributeValue(0);

                                if(paramName.equalsIgnoreCase("title"))
                                {
                                    workout.setTitle(parser.getAttributeValue(null, "value"));
                                }
                                else if(paramName.equalsIgnoreCase("repeat"))
                                {
                                    workout.setRepeat(parser.getAttributeIntValue(null, "value", 0));
                                }
                                else if(paramName.equalsIgnoreCase("heart_avg"))
                                {
                                    workout.setHerart_avg(parser.getAttributeIntValue(null, "value", 0));
                                }
                                else if(paramName.equalsIgnoreCase("icon"))
                                {
                                    workout.setIcon(parser.getAttributeValue(null, "value"));
                                }
                                else if(paramName.equalsIgnoreCase("video"))
                                {
                                    workout.setVideo(parser.getAttributeValue(null, "value"));
                                }
                                else if(paramName.equalsIgnoreCase("tracker"))
                                {
                                    workout.setTracker(parser.getAttributeValue(null, "value"));
                                }
                            }

                        }
                        else if(tagname.equalsIgnoreCase("workout"))
                        {
                            isCos = false;
                            workout = new Workout();
                        }

                        break;

                    case XmlResourceParser.TEXT:
                        break;

                    case XmlResourceParser.END_TAG:
                        tagname = parser.getName();

                        if(tagname.equalsIgnoreCase("course"))
                        {
                            course.setWorkouts(workouts);
                            course.setHeartBeats(heartBeats);

                            isEnd = true;
                        }
                        else if(tagname.equalsIgnoreCase("rest"))
                        {
                            isRest = false;
                        }

                        else if(tagname.equalsIgnoreCase("param"))
                        {

                        }
                        else if(tagname.equalsIgnoreCase("heartbeat"))
                        {
                            isHeart = false;
                        }

                        else if(tagname.equalsIgnoreCase("workout"))
                        {

                            if(workout != null)
                            {
                                workouts.add(workout);
                            }
                        }
                        break;

                    default:
                        break;
                }
                eventType = parser.next();
            }

            return course;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }
}
