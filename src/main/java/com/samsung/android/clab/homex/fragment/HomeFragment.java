package com.samsung.android.clab.homex.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.samsung.android.clab.homex.HistoryData;
import com.samsung.android.clab.homex.HistoryDetailData;
import com.samsung.android.clab.homex.R;
import com.samsung.android.clab.homex.activity.HomeDetailActivity;
import com.samsung.android.clab.homex.adapter.HomeAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeFragment extends Fragment implements HomeAdapter.Listener {

    @BindView(R.id.homeList) RecyclerView homeList;

    private HomeAdapter mAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        homeList.setHasFixedSize(true);
        homeList.setLayoutManager(new LinearLayoutManager(getActivity()));

        setDummyData();
    }

    @Override
    public void onItemClicked(int position) {
        Log.d("Log", "position : " + position);
        startActivity(new Intent(getActivity(), HomeDetailActivity.class));
    }

    private void setDummyData() {
        ArrayList<HistoryData> hd = new ArrayList<>();

        HistoryData historyData1 = new HistoryData();
        historyData1.setYearMonth("2019년 10월");

        ArrayList<HistoryDetailData> historyDetailDataList = new ArrayList<>();
        HistoryDetailData historyDetailData1 = new HistoryDetailData();
        historyDetailData1.setDate("10월 25일");
        historyDetailData1.setDayOfWeek("금요일");
        historyDetailData1.setProgram("탄력있는 다리를 위한 근력운동");
        historyDetailData1.setExerciseTime(83);
        historyDetailData1.setHeartRate(new int[]{75, 100, 120});
        historyDetailData1.setKcal(120);

        historyDetailDataList.add(historyDetailData1);

        HistoryDetailData historyDetailData2 = new HistoryDetailData();
        historyDetailData2.setDate("10월 15일");
        historyDetailData2.setDayOfWeek("월요일");
        historyDetailData2.setProgram("바디 밸런스 스트레칭");
        historyDetailData2.setExerciseTime(90);
        historyDetailData2.setHeartRate(new int[]{80, 110, 150});
        historyDetailData2.setKcal(150);

        historyDetailDataList.add(historyDetailData2);
        historyData1.setHistoryDetailData(historyDetailDataList);

        hd.add(historyData1);

        HistoryData historyData2 = new HistoryData();
        historyData2.setYearMonth("2019년 9월");

        ArrayList<HistoryDetailData> historyDetailDataList1 = new ArrayList<>();
        HistoryDetailData historyDetailData5 = new HistoryDetailData();
        historyDetailData5.setDate("9월 10일");
        historyDetailData5.setDayOfWeek("수요일");
        historyDetailData5.setProgram("맨몸으로 지방 태우기");
        historyDetailData5.setExerciseTime(40);
        historyDetailData5.setHeartRate(new int[]{80, 90, 100});
        historyDetailData5.setKcal(50);

        historyDetailDataList1.add(historyDetailData5);
        historyData2.setHistoryDetailData(historyDetailDataList1);

        hd.add(historyData2);

        HistoryData historyData3 = new HistoryData();
        historyData3.setYearMonth("2019년 8월");

        ArrayList<HistoryDetailData> historyDetailDataList2 = new ArrayList<>();
        HistoryDetailData historyDetailData3 = new HistoryDetailData();
        historyDetailData3.setDate("8월 5일");
        historyDetailData3.setDayOfWeek("화요일");
        historyDetailData3.setProgram("맨몸으로 지방 태우기");
        historyDetailData3.setExerciseTime(83);
        historyDetailData3.setHeartRate(new int[]{75, 100, 120});
        historyDetailData3.setKcal(120);

        historyDetailDataList2.add(historyDetailData3);

        HistoryDetailData historyDetailData4 = new HistoryDetailData();
        historyDetailData4.setDate("8월 15일");
        historyDetailData4.setDayOfWeek("목요일");
        historyDetailData4.setProgram("바디 밸런스 스트레칭");
        historyDetailData4.setExerciseTime(90);
        historyDetailData4.setHeartRate(new int[]{80, 110, 150});
        historyDetailData4.setKcal(150);

        historyDetailDataList2.add(historyDetailData4);
        historyData3.setHistoryDetailData(historyDetailDataList2);

        hd.add(historyData3);

        mAdapter = new HomeAdapter(getActivity(), this);
        mAdapter.setItems(hd);
        homeList.setAdapter(mAdapter);
    }
}
