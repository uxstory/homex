package com.samsung.android.clab.homex.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.samsung.android.clab.homex.Constants;
import com.samsung.android.clab.homex.dialog.QuitTimeDialog;
import com.samsung.android.clab.homex.R;
import com.samsung.android.clab.homex.dialog.ShowResultTimeDialog;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SettingActivity extends AppCompatActivity {

    @BindView(R.id.quitTime) TextView textViewQuitTime;
    @BindView(R.id.resultTime) TextView textViewResultTime;
    @BindView(R.id.switch1) Switch switchVoice;
    @BindView(R.id.switch2) Switch switchCallBlock;

    boolean isListenVoice, isCallBlock;
    String quitTime, resutTime;

    private CompoundButton.OnCheckedChangeListener voiceOnCheckedChangeListener = (buttonView, isChecked) -> {
        isListenVoice = isChecked;
        setPreference(Constants.PREF_KEY_LISTEN_VOICE, isChecked);
    };

    private CompoundButton.OnCheckedChangeListener callBlockOnCheckedChangeListener = (buttonView, isChecked) -> {
        isCallBlock = isChecked;
        setPreference(Constants.PREF_KEY_CALL_BLOCK, isChecked);
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        ButterKnife.bind(this);

        switchVoice.setOnCheckedChangeListener(voiceOnCheckedChangeListener);
        switchCallBlock.setOnCheckedChangeListener(callBlockOnCheckedChangeListener);

        switchVoice.setChecked(getPreference(Constants.PREF_KEY_LISTEN_VOICE));
        switchCallBlock.setChecked(getPreference(Constants.PREF_KEY_CALL_BLOCK));

        quitTime = getPreferenceString(Constants.PREF_KEY_QUIT_TIME);
        if(quitTime.equals("3")) {
            textViewQuitTime.setText(R.string.three_minutes);
        } else if(quitTime.equals("5")) {
            textViewQuitTime.setText(R.string.five_minutes);
        } else if(quitTime.equals("10")) {
            textViewQuitTime.setText(R.string.ten_minutes);
        } else {
            textViewQuitTime.setText(R.string.three_minutes);
        }

        resutTime = getPreferenceString(Constants.PREF_KEY_RESULT_SHOW_TIME);
        if(resutTime.equals("15")) {
            textViewResultTime.setText(getString(R.string.fifteen_sec));
        } else if(resutTime.equals("30")) {
            textViewResultTime.setText(getString(R.string.thirty_sec));
        } else if(resutTime.equals("60")) {
            textViewResultTime.setText(getString(R.string.one_min));
        } else {
            textViewResultTime.setText(getString(R.string.fifteen_sec));
        }
    }

    @OnClick({R.id.layoutQuitTime, R.id.layoutResultTime, R.id.clearData, R.id.buttonBack})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.layoutQuitTime:
                QuitTimeDialog dialog = new QuitTimeDialog(this, new QuitTimeDialog.OnQuitTimeListener() {
                    @Override
                    public void onQuitTime(String min) {
                        if(min.equals("3")) {
                            textViewQuitTime.setText(R.string.three_minutes);
                        } else if(min.equals("5")) {
                            textViewQuitTime.setText(R.string.five_minutes);
                        } else if(min.equals("10")) {
                            textViewQuitTime.setText(R.string.ten_minutes);
                        } else {
                            textViewQuitTime.setText(R.string.three_minutes);
                        }
                    }
                });

                dialog.show();
                break;

            case R.id.layoutResultTime:
                ShowResultTimeDialog dialog1 = new ShowResultTimeDialog(this, new ShowResultTimeDialog.OnShowTimeListener() {
                    @Override
                    public void onShowTime(String sec) {
                        if(sec.equals("15")) {
                            textViewResultTime.setText(getString(R.string.fifteen_sec));
                        } else if(sec.equals("30")) {
                            textViewResultTime.setText(getString(R.string.thirty_sec));
                        } else if(sec.equals("60")) {
                            textViewResultTime.setText(getString(R.string.one_min));
                        } else {
                            textViewResultTime.setText(getString(R.string.fifteen_sec));
                        }
                    }
                });

                dialog1.show();
                break;

            case R.id.clearData:
                clearApplicationData();
                break;

            case R.id.buttonBack:
                finish();
                break;

            default:
                break;
        }
    }

    /**
     * 데이터를 저장한다.
     * @param key
     * @param value
     */
    private void setPreference(String key, boolean value) {
        SharedPreferences pref = getSharedPreferences(Constants.PREFERENCE, MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    /**
     * 데이터를 가져오는 함수
     * @param key
     * @return
     */
    public boolean getPreference(String key){
        SharedPreferences pref = getSharedPreferences(Constants.PREFERENCE, MODE_PRIVATE);
        return pref.getBoolean(key, false);
    }

    public String getPreferenceString(String key){
        SharedPreferences pref = getSharedPreferences(Constants.PREFERENCE, MODE_PRIVATE);
        return pref.getString(key, "");
    }

    /**
     * 모든 데이터 삭제
     */
    private void clearApplicationData() {
        File cache = getCacheDir();
        File appDir = new File(cache.getParent());
        if(appDir.exists()) {
            String[] child = appDir.list();
            for(String s : child) {
                if(!s.equals("lib") && !s.equals("files")) {
                    deleteDir(new File(appDir, s));
                    Log.d("Log", "File /data/data/"+getPackageName()+"/" + s + " DELETED");
                }
            }
        }
    }

    private boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
        return dir.delete();
    }
}
