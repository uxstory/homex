package com.samsung.android.clab.homex;

public class ContentsItem {

    private int mImageResource;
    private String mText1;
    private String mText2;


    public ContentsItem(int imageResource, String text1, String text2)
    {
        mImageResource = imageResource;

        mText1 = text1;

        mText2 = text2;
    }

    public  int getmImageResource()
    {
        return  mImageResource;

    }

    public  String getmText1()
    {
        return mText1;

    }

    public  String getmText2()
    {
        return  mText2;
    }


}
