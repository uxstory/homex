package com.samsung.android.clab.homex;

import android.graphics.drawable.Drawable;

public class GuideView {

    private Drawable guideIcon;
    private String guideTitle;

    public GuideView()
    {

    }

    public Drawable getGuideIcon() {
        return guideIcon;
    }

    public void setGuideIcon(Drawable _guideIcon) {
        this.guideIcon = _guideIcon;
    }

    public String getGuideTitle() {
        return guideTitle;
    }

    public void setGuideTitle(String _guideTitle) {
        this.guideTitle = _guideTitle;
    }

}
