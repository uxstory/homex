package com.samsung.android.clab.homex;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

public class CourseWorkout extends FrameLayout {

    TextView textViewWorkout;
    TextView textViewSuccessCnt;
    TextView textViewRepeatCnt;

    public CourseWorkout(Context context, Workout workout, String successCnt) {
        super(context);
        init(context, workout, successCnt);
    }

    private void init(Context context, Workout workout, String successCnt) {
        View view = LayoutInflater.from(context).inflate(R.layout.view_workout, null, false);

        textViewWorkout = view.findViewById(R.id.textViewWorkout);
        textViewSuccessCnt = view.findViewById(R.id.textViewSuccessCnt);
        textViewRepeatCnt = view.findViewById(R.id.textViewRepeatCnt);

        textViewWorkout.setText(workout.getTitle());
        textViewRepeatCnt.setText(String.valueOf(workout.getRepeat()));
        textViewSuccessCnt.setText(successCnt);

        addView(view);
    }
}
