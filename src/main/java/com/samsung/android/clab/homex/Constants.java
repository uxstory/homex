package com.samsung.android.clab.homex;

public class Constants {

    public static final String PREFERENCE = "com.samsung.android.clab.homex";
    public static final String PREF_KEY_USER_THUMBNAIL = "com.samsung.android.clab.homex.PREF_KEY_USER_THUMBNAIL";
    public static final String PREF_KEY_USER_NAME = "com.samsung.android.clab.homex.PREF_KEY_USER_NAME";
    public static final String PREF_KEY_USER_AGE = "com.samsung.android.clab.homex.PREF_KEY_USER_AGE";
    public static final String PREF_KEY_USER_TALL = "com.samsung.android.clab.homex.PREF_KEY_USER_TALL";
    public static final String PREF_KEY_USER_WEIGHT = "com.samsung.android.clab.homex.PREF_KEY_USER_WEIGHT";
    public static final String PREF_KEY_USER_GENDER = "com.samsung.android.clab.homex.PREF_KEY_USER_GENDER";
    public static final String PREF_KEY_QUIT_TIME = "com.samsung.android.clab.homex.PREF_KEY_QUIT_TIME";
    public static final String PREF_KEY_RESULT_SHOW_TIME = "com.samsung.android.clab.homex.PREF_KEY_RESULT_SHOW_TIME";
    public static final String PREF_KEY_LISTEN_VOICE = "com.samsung.android.clab.homex.PREF_KEY_LISTEN_VOICE";
    public static final String PREF_KEY_CALL_BLOCK = "com.samsung.android.clab.homex.PREF_KEY_CALL_BLOCK";
}
