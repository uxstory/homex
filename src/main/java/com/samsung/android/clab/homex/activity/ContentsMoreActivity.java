package com.samsung.android.clab.homex.activity;

import android.content.Intent;
import android.content.res.XmlResourceParser;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.samsung.android.clab.homex.adapter.ContentsAdapter;
import com.samsung.android.clab.homex.Course;
import com.samsung.android.clab.homex.R;
import com.samsung.android.clab.homex.Workout;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ContentsMoreActivity extends AppCompatActivity implements ContentsAdapter.AdapterListener {

    @BindView(R.id.contentsList)
    RecyclerView contentsList;

    private ContentsAdapter mAdapter;

    ArrayList<Course> courses = new ArrayList<>();
    ArrayList<Workout> workouts;

    private Course course;
    private Workout workout;

    private ArrayList<Integer> heartBeats;

    // XML Parser
    XmlResourceParser[] xrp;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contents_more);
        ButterKnife.bind(this);

        xrp = new XmlResourceParser[6];

        xrp[0] = getResources().getXml(R.xml.course_muscle_leg_full);
        xrp[1] = getResources().getXml(R.xml.course_muscle_leg_demo);

        xrp[2] = getResources().getXml(R.xml.course_diet_fat_burn_full);
        xrp[3] = getResources().getXml(R.xml.course_diet_fat_burn_demo);

        xrp[4] = getResources().getXml(R.xml.course_balance_stretching_full);
        xrp[5] = getResources().getXml(R.xml.course_balance_stretching_demo);

        for(int i =0; i < xrp.length; i++)
        {
            readCourse(xrp[i]);
        }

        contentsList.setHasFixedSize(true);
        contentsList.setLayoutManager(new LinearLayoutManager(this));

        mAdapter = new ContentsAdapter(this, courses);
        mAdapter.setAdapterListener(this);
        contentsList.setAdapter(mAdapter);
    }

    @OnClick(R.id.btn_backcategory)
    public void onClick(View view) {
        finish();
    }

    @Override
    public void onItemClick(Course item , int position) {
        Intent intent = new Intent(this, CategoryDetailActivity.class);
        intent.putExtra("position", position);
        intent.putExtra("course", item);
        startActivity(intent);
    }

    public void readCourse(XmlResourceParser parser)
    {
        Log.d("START", "read Course Start");
        try {
            int eventType = parser.getEventType();
            boolean isEnd = false;
            boolean isCos = false;
            boolean isRest = false;

            boolean isHeart = false;

            while (eventType != XmlResourceParser.END_DOCUMENT && isEnd == false) {
                String tagname;

                switch(eventType)
                {
                    case XmlResourceParser.START_TAG:
                        tagname = parser.getName();

                        if(tagname != null)
                        {
                            Log.d("tagName", tagname);
                        }

                        if(tagname.equalsIgnoreCase("course"))
                        {
                            course = new Course();
                            heartBeats = new ArrayList<>();
                            workouts = new ArrayList<>();

                            isCos = true;
                        }

                        else if(tagname.equalsIgnoreCase("rest"))
                        {
                            isRest = true;
                        }
                        else if(tagname.equalsIgnoreCase("heartbeat"))
                        {
                            isCos = false;
                            isHeart = true;
                        }

                        else if(tagname.equalsIgnoreCase("param"))
                        {
                            if(isCos)
                            {
                                String paramName = parser.getAttributeValue(0);
                                String paramValue;

                                if(paramName.equalsIgnoreCase("title"))
                                {
                                    paramValue = parser.getAttributeValue(null, "value");
                                    course.setTitle(paramValue);
                                }
                                else if(paramName.equalsIgnoreCase("desc"))
                                {
                                    paramValue = parser.getAttributeValue(null, "value");
                                    course.setDesc(paramValue);
                                }
                                else if(paramName.equalsIgnoreCase("level"))
                                {
                                    paramValue = parser.getAttributeValue(null, "value");
                                    course.setLevel(paramValue);
                                }
                                else if(paramName.equalsIgnoreCase("steps"))
                                {
                                    course.setStep(parser.getAttributeIntValue(null, "value", 0));
                                }
                                else if(paramName.equalsIgnoreCase("icon"))
                                {
                                    paramValue = parser.getAttributeValue(null, "value");
                                    course.setIcon(paramValue);
                                }
                                else if(paramName.equalsIgnoreCase("video"))
                                {
                                    paramValue = parser.getAttributeValue(null, "value");
                                    course.setVideo(paramValue);
                                }
                                else if(paramName.equalsIgnoreCase("set"))
                                {
                                    course.setSet(parser.getAttributeIntValue(null, "value", 0));
                                }

                            }
                            else if(isRest)
                            {
                                String paramName = parser.getAttributeValue(0);

                                if(paramName.equalsIgnoreCase("resttime"))
                                {
                                    course.setResttime(parser.getAttributeIntValue(null, "value", 0));
                                }
                                else if(paramName.equalsIgnoreCase("desc"))
                                {
                                    course.setRestdesc(parser.getAttributeValue(null, "value"));
                                }
                            }
                            else if(isHeart)
                            {
                                String paramName = parser.getAttributeValue(0);

                                if(paramName.equalsIgnoreCase("1"))
                                {
                                    heartBeats.add(parser.getAttributeIntValue(null, "value", 0));
                                }

                                else if(paramName.equalsIgnoreCase("2"))
                                {
                                    heartBeats.add(parser.getAttributeIntValue(null, "value", 0));
                                }

                                else if(paramName.equalsIgnoreCase("3"))
                                {
                                    heartBeats.add(parser.getAttributeIntValue(null, "value", 0));
                                }

                                else if(paramName.equalsIgnoreCase("4"))
                                {
                                    heartBeats.add(parser.getAttributeIntValue(null, "value", 0));
                                }

                                else if(paramName.equalsIgnoreCase("5"))
                                {
                                    heartBeats.add(parser.getAttributeIntValue(null, "value", 0));
                                }

                                else if(paramName.equalsIgnoreCase("6"))
                                {
                                    heartBeats.add(parser.getAttributeIntValue(null, "value", 0));
                                }

                                else if(paramName.equalsIgnoreCase("7"))
                                {
                                    heartBeats.add(parser.getAttributeIntValue(null, "value", 0));
                                }
                                else if(paramName.equalsIgnoreCase("8"))
                                {
                                    heartBeats.add(parser.getAttributeIntValue(null, "value", 0));
                                }
                                else if(paramName.equalsIgnoreCase("9"))
                                {
                                    heartBeats.add(parser.getAttributeIntValue(null, "value", 0));
                                }
                                else if(paramName.equalsIgnoreCase("10"))
                                {
                                    heartBeats.add(parser.getAttributeIntValue(null, "value", 0));
                                }
                                else
                                {

                                }
                            }
                            else
                            {
                                // workout param
                                String paramName = parser.getAttributeValue(0);

                                if(paramName.equalsIgnoreCase("title"))
                                {
                                    workout.setTitle(parser.getAttributeValue(null, "value"));
                                }
                                else if(paramName.equalsIgnoreCase("repeat"))
                                {
                                    workout.setRepeat(parser.getAttributeIntValue(null, "value", 0));
                                }
                                else if(paramName.equalsIgnoreCase("heart_avg"))
                                {
                                    workout.setHerart_avg(parser.getAttributeIntValue(null, "value", 0));
                                }
                                else if(paramName.equalsIgnoreCase("icon"))
                                {
                                    workout.setIcon(parser.getAttributeValue(null, "value"));
                                }
                                else if(paramName.equalsIgnoreCase("video"))
                                {
                                    workout.setVideo(parser.getAttributeValue(null, "value"));
                                }
                                else if(paramName.equalsIgnoreCase("tracker"))
                                {
                                    workout.setTracker(parser.getAttributeValue(null, "value"));
                                }
                            }

                        }
                        else if(tagname.equalsIgnoreCase("workout"))
                        {
                            isCos = false;
                            workout = new Workout();
                        }

                        break;

                    case XmlResourceParser.TEXT:
                        break;

                    case XmlResourceParser.END_TAG:
                        tagname = parser.getName();

                        if(tagname.equalsIgnoreCase("course"))
                        {
                            course.setWorkouts(workouts);
                            course.setHeartBeats(heartBeats);

                            isEnd = true;
                            if(course !=null)
                            {
                                courses.add(course);
                            }
                        }
                        else if(tagname.equalsIgnoreCase("rest"))
                        {
                            isRest = false;
                        }

                        else if(tagname.equalsIgnoreCase("param"))
                        {

                        }
                        else if(tagname.equalsIgnoreCase("heartbeat"))
                        {
                            isHeart = false;
                            heartBeats.clear();
                        }

                        else if(tagname.equalsIgnoreCase("workout"))
                        {

                            if(workout != null)
                            {
                                workouts.add(workout);
                            }
                        }
                        break;

                    default:
                        break;
                }
                eventType = parser.next();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
