package com.samsung.android.clab.homex;

import android.content.Context;
import android.speech.tts.TextToSpeech;
import android.util.Log;

import java.util.Locale;

public class TTSSingleton {
    public static final String TAG = TTSSingleton.class.getSimpleName();

    private static TTSSingleton ttsSingleton = null;
    private TextToSpeech textToSpeech;
    private Context mContext;

    public static TTSSingleton getInstance(Context pContext) {
        if(ttsSingleton == null) {
            synchronized (TTSSingleton.class) {
                if(ttsSingleton == null) {
                    ttsSingleton = new TTSSingleton(pContext);
                }
            }
        }
        return ttsSingleton;
    }

    private TTSSingleton(Context pContext){
        mContext = pContext;
        textToSpeech = new TextToSpeech(mContext, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.SUCCESS) {
                    int ttsLang = textToSpeech.setLanguage(Locale.KOREA);

                    if (ttsLang == TextToSpeech.LANG_MISSING_DATA
                            || ttsLang == TextToSpeech.LANG_NOT_SUPPORTED) {
                        Log.e(TAG, "The Language is not supported!");
                    }
                    Log.i(TAG, "Initialization success.");
                } else {
                    Log.d(TAG, "TTS Initialization failed!");
                }
            }
        });
    }

    public boolean speakGuide(String msg) {
        Log.i("TTS", "tts text:" + msg);
        if(ttsSingleton == null || textToSpeech == null ) {
            Log.e(TAG, "speakGuide not initialized");
            return false;
        }

        int speechStatus = TextToSpeech.ERROR;

        if(!textToSpeech.isSpeaking()) {
            speechStatus = textToSpeech.speak(msg, TextToSpeech.QUEUE_FLUSH, null, "homexTTS");
        }

        if (speechStatus == TextToSpeech.ERROR) {
            Log.e("TTS", "Error in converting Text to Speech!");
            return false;
        }
        return true;
    }

    public void shutdownTTS() {
        if (textToSpeech != null) {
            textToSpeech.stop();
            textToSpeech.shutdown();
        }
        ttsSingleton = null;
        mContext = null;
    }
}
