package com.samsung.android.clab.homex;

public class CategoryDetailItem {
    public int resId;
    public String title;
    public String repeat;

    public CategoryDetailItem(int resId, String title, String repeat) {
        this.resId = resId;
        this.title = title;
        this.repeat = repeat;
    }

    public int getResId() {
        return resId;
    }

    public void setResId(int resId) {
        this.resId = resId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getRepeat() {
        return repeat;
    }

    public void setRepeat(String repeat) {
        this.repeat = repeat;
    }
}
