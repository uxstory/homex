package com.samsung.android.clab.homex;

public class SuccessMethod {

    private String method;
    private int skeleton_a;
    private int skeleton_b;

    private String check_position;


    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public int getSkeleton_a() {
        return skeleton_a;
    }

    public void setSkeleton_a(String skeleton_a) {
        if(skeleton_a == null) {
            this.skeleton_a = 0;
        }else{
            this.skeleton_a = Integer.parseInt(skeleton_a);
        }
    }

    public int getSkeleton_b() {
        return skeleton_b;
    }

    public void setSkeleton_b(String skeleton_b) {
        if(skeleton_b == null) {

            this.skeleton_b = 0;
        }
        else
        {
            this.skeleton_b = Integer.parseInt(skeleton_b);
        }
    }

    public String getCheck_position() {
        return check_position;
    }

    public void setCheck_position(String check_position) {
        this.check_position = check_position;
    }


    @Override
    public String toString() {
        return "method : " + method + "\n"
                + "skeleton_a : " + skeleton_a + "\n"
                + "skeleton_b : " + skeleton_b + "\n"
                + "check_position : " + check_position;
    }
}
