package com.samsung.android.clab.homex.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.samsung.android.clab.homex.Constants;
import com.samsung.android.clab.homex.fragment.ProfileDialogFragment;
import com.samsung.android.clab.homex.R;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.content.Context.MODE_PRIVATE;

public class ProfileActivity extends Fragment implements ProfileDialogFragment.OnCompleteListener {

    @BindView(R.id.text_name) TextView textViewName;
    @BindView(R.id.text_body) TextView textViewBody;
    @BindView(R.id.IconPic) ImageView imageViewThumbnail;

    private String thumbNail, name, age, tall, weight, gender;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_profile, container,false);
        ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setUserData();
    }

    @Override
    public void onCompleted() {
        setUserData();
    }

    @OnClick({R.id.layoutProfile, R.id.btn_settings, R.id.btn_legalinfo})
    public void onClick(View view) {
        switch(view.getId()) {
            case R.id.layoutProfile:
                ProfileDialogFragment fragment = ProfileDialogFragment.getInstance();
                Bundle bundle = new Bundle();
                bundle.putString(ProfileDialogFragment.BUNDLE_EXTRA_THUMBNAIL, getPreferenceString(Constants.PREF_KEY_USER_THUMBNAIL, null));
                bundle.putString(ProfileDialogFragment.BUNDLE_EXTRA_NAME, getPreferenceString(Constants.PREF_KEY_USER_NAME, "홍길동"));
                bundle.putString(ProfileDialogFragment.BUNDLE_EXTRA_AGE, getPreferenceString(Constants.PREF_KEY_USER_AGE, "31"));
                bundle.putString(ProfileDialogFragment.BUNDLE_EXTRA_TALL, getPreferenceString(Constants.PREF_KEY_USER_TALL, "161"));
                bundle.putString(ProfileDialogFragment.BUNDLE_EXTRA_WEIGHT, getPreferenceString(Constants.PREF_KEY_USER_WEIGHT, "53"));
                bundle.putString(ProfileDialogFragment.BUNDLE_EXTRA_GENDER, getPreferenceString(Constants.PREF_KEY_USER_GENDER, " F"));
                fragment.setArguments(bundle);
                fragment.setListener(this);
                fragment.show(getChildFragmentManager(), "Profile");
                break;

            case R.id.btn_settings:
                startActivity(new Intent(getActivity(), SettingActivity.class));
                break;

            case R.id.btn_legalinfo:
                Toast.makeText(getActivity(), "Btn_legalinfo", Toast.LENGTH_SHORT).show();
                break;

            default:
                break;
        }
    }

    /**
     * 데이터를 가져오는 함수
     * @param key
     * @return
     */
    public String getPreferenceString(String key, String defValue){
        SharedPreferences pref = getActivity().getSharedPreferences(Constants.PREFERENCE, MODE_PRIVATE);
        return pref.getString(key, "");
    }

    private void setUserData() {
        name = getPreferenceString(Constants.PREF_KEY_USER_NAME, "홍길동");
        age = getPreferenceString(Constants.PREF_KEY_USER_AGE, "31");
        tall = getPreferenceString(Constants.PREF_KEY_USER_TALL, "161");
        weight = getPreferenceString(Constants.PREF_KEY_USER_WEIGHT, "53");
        gender = getPreferenceString(Constants.PREF_KEY_USER_GENDER,"F");
        thumbNail = getPreferenceString(Constants.PREF_KEY_USER_THUMBNAIL, null);

        StringBuilder sb = new StringBuilder();
        sb.append(age);
        sb.append("세, ");
        sb.append(tall);
        sb.append("cm, ");
        sb.append(weight);
        sb.append("kg, ");
        sb.append(gender);

        textViewName.setText(name);
        textViewBody.setText(sb.toString());

        if(TextUtils.isEmpty(thumbNail)) {
            imageViewThumbnail.setBackgroundResource(R.drawable.img_profile_empty);
        } else {
            File profileFile = new File(thumbNail);
            Glide.with(this)
                    .load(profileFile)
                    .apply(RequestOptions.circleCropTransform())
                    .into(imageViewThumbnail);
        }
    }
}
