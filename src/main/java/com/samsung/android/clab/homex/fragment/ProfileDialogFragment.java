package com.samsung.android.clab.homex.fragment;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.samsung.android.clab.homex.Constants;
import com.samsung.android.clab.homex.R;
import com.warkiz.widget.IndicatorSeekBar;
import com.warkiz.widget.OnSeekChangeListener;
import com.warkiz.widget.SeekParams;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.content.Context.MODE_PRIVATE;

public class ProfileDialogFragment extends DialogFragment implements ImagePickDialogFragment.OnPickCompleteListener {

    public static final String BUNDLE_EXTRA_THUMBNAIL = "thumbnail";
    public static final String BUNDLE_EXTRA_NAME = "name";
    public static final String BUNDLE_EXTRA_AGE = "age";
    public static final String BUNDLE_EXTRA_TALL = "tall";
    public static final String BUNDLE_EXTRA_WEIGHT = "weight";
    public static final String BUNDLE_EXTRA_GENDER = "gender";

    @BindView(R.id.thumbnail) ImageView imageViewThumbnail;

    @BindView(R.id.name) EditText editTextName;
    @BindView(R.id.age) TextView textViewAge;
    @BindView(R.id.tall) TextView textViewTall;
    @BindView(R.id.weight) TextView textViewWeight;

    @BindView(R.id.buttonMale) RadioButton btnMale;
    @BindView(R.id.buttonFemale) RadioButton btnFemale;
    @BindView(R.id.gender) RadioGroup radioGender;

    @BindView(R.id.ageBar) com.warkiz.widget.IndicatorSeekBar ageBar;
    @BindView(R.id.tallBar) com.warkiz.widget.IndicatorSeekBar tallBar;
    @BindView(R.id.weightBar) com.warkiz.widget.IndicatorSeekBar weightBar;

    private String thumbNail, name, age, tall, weight, gender;
    private File imageFile;
    private OnCompleteListener mListener;

    public interface OnCompleteListener {
        void onCompleted();
    }

    private OnSeekChangeListener ageOnSeekChangeListener = new OnSeekChangeListener() {
        @Override
        public void onSeeking(SeekParams seekParams) {
            textViewAge.setText(String.valueOf(seekParams.progress));
            age = String.valueOf(seekParams.progress);
        }

        @Override
        public void onStartTrackingTouch(IndicatorSeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(IndicatorSeekBar seekBar) {

        }
    };

    private OnSeekChangeListener tallOnSeekChangeListener = new OnSeekChangeListener() {
        @Override
        public void onSeeking(SeekParams seekParams) {
            textViewTall.setText(seekParams.progress + " cm");
            tall = String.valueOf(seekParams.progress);
        }

        @Override
        public void onStartTrackingTouch(IndicatorSeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(IndicatorSeekBar seekBar) {

        }
    };

    private OnSeekChangeListener weightOnSeekChangeListener = new OnSeekChangeListener() {
        @Override
        public void onSeeking(SeekParams seekParams) {
            textViewWeight.setText(seekParams.progress + " kg");
            weight = String.valueOf(seekParams.progress);
        }

        @Override
        public void onStartTrackingTouch(IndicatorSeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(IndicatorSeekBar seekBar) {

        }
    };

    RadioGroup.OnCheckedChangeListener genderCheckedChangeListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            if(checkedId == R.id.buttonMale) {
                gender = "M";
            } else if(checkedId == R.id.buttonFemale) {
                gender = "F";
            }
        }
    };

    public static ProfileDialogFragment getInstance() {
        return new ProfileDialogFragment();
    }

    public void setListener(OnCompleteListener listener) {
        this.mListener = listener;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_profile, container, false);
        ButterKnife.bind(this, view);

        if(getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle bundle = getArguments();
        if(bundle != null) {
            thumbNail = bundle.getString(BUNDLE_EXTRA_THUMBNAIL);
            name = bundle.getString(BUNDLE_EXTRA_NAME);
            age = bundle.getString(BUNDLE_EXTRA_AGE);
            tall = bundle.getString(BUNDLE_EXTRA_TALL);
            weight = bundle.getString(BUNDLE_EXTRA_WEIGHT);
            gender = bundle.getString(BUNDLE_EXTRA_GENDER);
        }

        ageBar.setOnSeekChangeListener(ageOnSeekChangeListener);
        tallBar.setOnSeekChangeListener(tallOnSeekChangeListener);
        weightBar.setOnSeekChangeListener(weightOnSeekChangeListener);
        radioGender.setOnCheckedChangeListener(genderCheckedChangeListener);

        setUserData();
    }

    @OnClick({R.id.buttonConfirm, R.id.thumbnail})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonConfirm:
                setPreference(Constants.PREF_KEY_USER_AGE, age);
                setPreference(Constants.PREF_KEY_USER_TALL, tall);
                setPreference(Constants.PREF_KEY_USER_WEIGHT, weight);
                setPreference(Constants.PREF_KEY_USER_GENDER, gender);
                setPreference(Constants.PREF_KEY_USER_NAME, editTextName.getText().toString());

                if(imageFile != null) {
                    setPreference(Constants.PREF_KEY_USER_THUMBNAIL, imageFile.getAbsolutePath());
                } else {
                    setPreference(Constants.PREF_KEY_USER_THUMBNAIL, "");
                }

                mListener.onCompleted();
                dismiss();
                break;

            case R.id.thumbnail:
                ImagePickDialogFragment fragment = ImagePickDialogFragment.getInstance();
                fragment.setListener(this);
                fragment.show(getChildFragmentManager(), "ImagePick");
                break;

            default:
                break;
        }
    }

    @Override
    public void onCompleted(File profileFile) {
        Glide.with(this)
                .load(profileFile)
                .apply(RequestOptions.circleCropTransform())
                .into(imageViewThumbnail);

        imageFile = profileFile;
    }

    /**
     * 데이터를 저장한다.
     * @param key
     * @param value
     */
    private void setPreference(String key, String value) {
        SharedPreferences pref = getActivity().getSharedPreferences(Constants.PREFERENCE, MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(key, value);
        editor.commit();
    }

    private void setUserData() {
        if(gender.equals("F")) {
            btnFemale.setChecked(true);
        } else {
            btnMale.setChecked(true);
        }

        editTextName.setText(name);

        textViewAge.setText(age);
        if(TextUtils.isEmpty(age)) {
            ageBar.setProgress(0);
        } else {
            ageBar.setProgress(Integer.valueOf(age));

        }

        textViewTall.setText(tall + " cm");
        if(TextUtils.isEmpty(tall)) {
            tallBar.setProgress(0);
        } else {
            tallBar.setProgress(Integer.valueOf(tall));
        }


        textViewWeight.setText(weight + " kg");
        if(TextUtils.isEmpty(weight)) {
            weightBar.setProgress(0);
        } else {
            weightBar.setProgress(Integer.valueOf(weight));
        }

        if(TextUtils.isEmpty(thumbNail)) {
            imageViewThumbnail.setBackgroundResource(R.drawable.img_profile_empty);
        } else {
            File profileFile = new File(thumbNail);
            Glide.with(this)
                    .load(profileFile)
                    .apply(RequestOptions.circleCropTransform())
                    .into(imageViewThumbnail);
        }
    }
}
