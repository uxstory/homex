package com.samsung.android.clab.homex.fragment;

import android.content.Intent;
import android.content.res.XmlResourceParser;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.samsung.android.clab.homex.Category;
import com.samsung.android.clab.homex.Course;
import com.samsung.android.clab.homex.Program;
import com.samsung.android.clab.homex.R;
import com.samsung.android.clab.homex.Workout;
import com.samsung.android.clab.homex.activity.CategoryActivity;
import com.samsung.android.clab.homex.activity.CategoryDetailActivity;
import com.samsung.android.clab.homex.activity.CategoryMoreActivity;
import com.samsung.android.clab.homex.activity.ContentsMoreActivity;
import com.samsung.android.clab.homex.adapter.RecyclerMycontentsAdapter;

import java.util.ArrayList;

public class ContentsFragment extends Fragment implements RecyclerMycontentsAdapter.AdapterListener {

    public static ContentsFragment newInstance() {
        return new ContentsFragment();
    }

    private RecyclerView recyclerView;
    private RecyclerMycontentsAdapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    private ArrayList<Course> courses = new ArrayList<>();
    private ArrayList<Workout> workouts;
    private Course course;
    private Workout workout;

    private ArrayList<Program> programList;
    private Program program;

    private ArrayList<Category> categoryList = new ArrayList<>();
    private Category category;

    private ArrayList<Integer> heartBeats;

    // XML Parser
    XmlResourceParser[] xrp;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_contents, container, false);

        recyclerView = (RecyclerView)v.findViewById(R.id.recycler_mycontents);
        //recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        ((LinearLayoutManager) layoutManager).setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerView.setLayoutManager(layoutManager);

        RelativeLayout btn_categorymore = (RelativeLayout)v.findViewById(R.id.btn_categorymore);
        RelativeLayout btn_mycontentsmore = v.findViewById(R.id.btn_mycontentsmore);

        btn_categorymore.setOnClickListener(new View.OnClickListener()
        {

            public void onClick(View v) {
                startActivity(new Intent(getActivity(), CategoryMoreActivity.class));
            }

        });

        btn_mycontentsmore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), ContentsMoreActivity.class));
            }
        });


        CardView card_exercise_01 = (CardView)v.findViewById(R.id.card_exercise01);

        card_exercise_01.setOnClickListener(new View.OnClickListener()
        {

            public void onClick(View v) {
                ArrayList<Program> programList = readProgram(getResources().getXml(R.xml.program_list_muscle));

                Intent intent = new Intent(getActivity(), CategoryActivity.class);
                intent.putParcelableArrayListExtra("program", programList);
                intent.putExtra("title", categoryList.get(0).getTitle());
                intent.putExtra("description", categoryList.get(0).getDesc());
                startActivity(intent);
            }

        });


        CardView card_exercise_02 = (CardView)v.findViewById(R.id.card_exercise02);

        card_exercise_02.setOnClickListener(new View.OnClickListener()
        {

            public void onClick(View v) {
                ArrayList<Program> programList = readProgram(getResources().getXml(R.xml.program_list_diet));

                Intent intent = new Intent(getActivity(), CategoryActivity.class);
                intent.putParcelableArrayListExtra("program", programList);
                intent.putExtra("title", categoryList.get(1).getTitle());
                intent.putExtra("description", categoryList.get(1).getDesc());
                startActivity(intent);
            }

        });

        CardView card_exercise_03 = (CardView)v.findViewById(R.id.card_exercise03);

        card_exercise_03.setOnClickListener(new View.OnClickListener()
        {

            public void onClick(View v) {
                ArrayList<Program> programList = readProgram(getResources().getXml(R.xml.program_list_balance));

                Intent intent = new Intent(getActivity(), CategoryActivity.class);
                intent.putParcelableArrayListExtra("program", programList);
                intent.putExtra("title", categoryList.get(2).getTitle());
                intent.putExtra("description", categoryList.get(2).getDesc());
                startActivity(intent);
            }

        });

        CardView card_exercise_04 = (CardView)v.findViewById(R.id.card_exercise04);

        card_exercise_04.setOnClickListener(new View.OnClickListener()
        {

            public void onClick(View v) {
                Toast.makeText(getActivity(), "준비 중입니다...", Toast.LENGTH_SHORT).show();
            }

        });

        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        xmlParsing();

        adapter = new RecyclerMycontentsAdapter(getActivity(), courses, this);
        recyclerView.setAdapter(adapter);



        Log.d("Log", "course : " + courses.size() + " workouts : " + workouts.size() + " category : " + categoryList.size());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        courses.clear();
        workouts.clear();
        categoryList.clear();
    }

    @Override
    public void onItemClick(Course item, int position) {
        Intent intent = new Intent(getActivity(), CategoryDetailActivity.class);
        intent.putExtra("position", position);
        intent.putExtra("course", item);
        startActivity(intent);
    }

    private void xmlParsing() {
        // 코스 파싱
        xrp = new XmlResourceParser[6];
        xrp[0] = getResources().getXml(R.xml.course_muscle_leg_full);
        xrp[1] = getResources().getXml(R.xml.course_muscle_leg_demo);
        xrp[2] = getResources().getXml(R.xml.course_diet_fat_burn_full);
        xrp[3] = getResources().getXml(R.xml.course_diet_fat_burn_demo);
        xrp[4] = getResources().getXml(R.xml.course_balance_stretching_full);
        xrp[5] = getResources().getXml(R.xml.course_balance_stretching_demo);

        for(int i =0; i < xrp.length; i++)
        {
            readCourse(xrp[i]);
        }

        // 카테고리 파싱
        readCategory(getResources().getXml(R.xml.categories));
    }

    public void readCategory(XmlResourceParser parser)
    {
        Log.d("START", "read Program Start");
        try {
            int eventType = parser.getEventType();
            boolean isEnd = false;

            while (eventType != XmlResourceParser.END_DOCUMENT && isEnd == false) {
                String tagname;

                switch(eventType)
                {
                    case XmlResourceParser.START_TAG:
                        tagname = parser.getName();

                        if(tagname != null)
                        {
                            Log.d("tagName", tagname);
                        }

                        if(tagname.equalsIgnoreCase("param"))
                        {
                            String paramName = parser.getAttributeValue(0);

                            if(paramName.equalsIgnoreCase("title"))
                            {
                                category.setTitle(parser.getAttributeValue(null, "value"));
                            }
                            else if(paramName.equalsIgnoreCase("desc"))
                            {
                                category.setDesc(parser.getAttributeValue(null, "value"));
                            }
                            else if(paramName.equalsIgnoreCase("icon"))
                            {
                                category.setIcon(parser.getAttributeValue(null, "value"));
                            }
                            else if(paramName.equalsIgnoreCase("contents"))
                            {
                                category.setContents(parser.getAttributeValue(null, "value"));
                            }
                            else if(paramName.equalsIgnoreCase("ready"))
                            {
                                category.setReady(parser.getAttributeValue(null, "value"));
                            }
                        }
                        else if(tagname.equalsIgnoreCase("item"))
                        {
                            category = new Category();
                        }

                        break;

                    case XmlResourceParser.TEXT:
                        break;

                    case XmlResourceParser.END_TAG:
                        tagname = parser.getName();

                        if(tagname.equalsIgnoreCase("categories"))
                        {
                            isEnd = true;
                        }
                        else if(tagname.equalsIgnoreCase("item"))
                        {

                            if(category != null)
                            {
                                categoryList.add(category);
                            }
                        }
                        break;

                    default:
                        break;
                }
                eventType = parser.next();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public ArrayList<Program> readProgram(XmlResourceParser parser)
    {
        Log.d("START", "read Program Start");
        try {
            int eventType = parser.getEventType();
            boolean isEnd = false;

            while (eventType != XmlResourceParser.END_DOCUMENT && isEnd == false) {
                String tagname;

                switch(eventType)
                {
                    case XmlResourceParser.START_TAG:
                        tagname = parser.getName();

                        if(tagname != null)
                        {
                            Log.d("tagName", tagname);
                        }

                        if(tagname.equalsIgnoreCase("contents"))
                        {
                            programList = new ArrayList<>();
                        }

                        else if(tagname.equalsIgnoreCase("param"))
                        {
                            String paramName = parser.getAttributeValue(0);

                            if(paramName.equalsIgnoreCase("title"))
                            {
                                program.setTitle(parser.getAttributeValue(null, "value"));
                            }
                            else if(paramName.equalsIgnoreCase("desc"))
                            {
                                program.setDesc(parser.getAttributeValue(null, "value"));
                            }
                            else if(paramName.equalsIgnoreCase("level"))
                            {
                                program.setLevel(parser.getAttributeValue(null, "value"));
                            }
                            else if(paramName.equalsIgnoreCase("steps"))
                            {
                                program.setSteps(parser.getAttributeValue(null, "value"));
                            }
                            else if(paramName.equalsIgnoreCase("icon"))
                            {
                                program.setIcon(parser.getAttributeValue(null, "value"));
                            }
                            else if(paramName.equalsIgnoreCase("course"))
                            {
                                program.setCourse(parser.getAttributeValue(null, "value"));
                            }

                        }
                        else if(tagname.equalsIgnoreCase("item"))
                        {
                            program = new Program();
                        }

                        break;

                    case XmlResourceParser.TEXT:
                        break;

                    case XmlResourceParser.END_TAG:
                        tagname = parser.getName();

                        if(tagname.equalsIgnoreCase("contents"))
                        {
                            isEnd = true;
                        }
                        else if(tagname.equalsIgnoreCase("item"))
                        {

                            if(program != null)
                            {
                                programList.add(program);
                            }
                        }
                        break;

                    default:
                        break;
                }
                eventType = parser.next();
            }

            return programList;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    public ArrayList<Program> readProgram(XmlResourceParser parser, ArrayList<Program> programs)
    {
        Log.d("START", "read Program Start");
        try {
            int eventType = parser.getEventType();
            boolean isEnd = false;

            while (eventType != XmlResourceParser.END_DOCUMENT && isEnd == false) {
                String tagname;

                switch(eventType)
                {
                    case XmlResourceParser.START_TAG:
                        tagname = parser.getName();

                        if(tagname != null)
                        {
                            Log.d("tagName", tagname);
                        }

                        if(tagname.equalsIgnoreCase("contents"))
                        {
                            programs = new ArrayList<>();
                        }

                        else if(tagname.equalsIgnoreCase("param"))
                        {
                            String paramName = parser.getAttributeValue(0);

                            if(paramName.equalsIgnoreCase("title"))
                            {
                                program.setTitle(parser.getAttributeValue(null, "value"));
                            }
                            else if(paramName.equalsIgnoreCase("desc"))
                            {
                                program.setDesc(parser.getAttributeValue(null, "value"));
                            }
                            else if(paramName.equalsIgnoreCase("level"))
                            {
                                program.setLevel(parser.getAttributeValue(null, "value"));
                            }
                            else if(paramName.equalsIgnoreCase("steps"))
                            {
                                program.setSteps(parser.getAttributeValue(null, "value"));
                            }
                            else if(paramName.equalsIgnoreCase("icon"))
                            {
                                program.setIcon(parser.getAttributeValue(null, "value"));
                            }
                            else if(paramName.equalsIgnoreCase("course"))
                            {
                                program.setCourse(parser.getAttributeValue(null, "value"));
                            }

                        }
                        else if(tagname.equalsIgnoreCase("item"))
                        {
                            program = new Program();
                        }

                        break;

                    case XmlResourceParser.TEXT:
                        break;

                    case XmlResourceParser.END_TAG:
                        tagname = parser.getName();

                        if(tagname.equalsIgnoreCase("contents"))
                        {
                            isEnd = true;
                        }
                        else if(tagname.equalsIgnoreCase("item"))
                        {

                            if(program != null)
                            {
                                programs.add(program);
                            }
                        }
                        break;

                    default:
                        break;
                }
                eventType = parser.next();
            }

            return programs;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    public void readCourse(XmlResourceParser parser)
    {
        Log.d("START", "read Course Start");
        try {
            int eventType = parser.getEventType();
            boolean isEnd = false;
            boolean isCos = false;
            boolean isRest = false;

            boolean isHeart = false;

            while (eventType != XmlResourceParser.END_DOCUMENT && isEnd == false) {
                String tagname;

                switch(eventType)
                {
                    case XmlResourceParser.START_TAG:
                        tagname = parser.getName();

                        if(tagname != null)
                        {
                            Log.d("tagName", tagname);
                        }

                        if(tagname.equalsIgnoreCase("course"))
                        {
                            course = new Course();
                            workouts = new ArrayList<>();
                            heartBeats = new ArrayList<>();

                            isCos = true;
                        }

                        else if(tagname.equalsIgnoreCase("rest"))
                        {
                            isRest = true;
                        }
                        else if(tagname.equalsIgnoreCase("heartbeat"))
                        {
                            isCos = false;
                            isHeart = true;
                        }

                        else if(tagname.equalsIgnoreCase("param"))
                        {
                            if(isCos)
                            {
                                String paramName = parser.getAttributeValue(0);
                                String paramValue;

                                if(paramName.equalsIgnoreCase("title"))
                                {
                                    paramValue = parser.getAttributeValue(null, "value");
                                    course.setTitle(paramValue);
                                }
                                else if(paramName.equalsIgnoreCase("desc"))
                                {
                                    paramValue = parser.getAttributeValue(null, "value");
                                    course.setDesc(paramValue);
                                }
                                else if(paramName.equalsIgnoreCase("level"))
                                {
                                    paramValue = parser.getAttributeValue(null, "value");
                                    course.setLevel(paramValue);
                                }
                                else if(paramName.equalsIgnoreCase("steps"))
                                {
                                    course.setStep(parser.getAttributeIntValue(null, "value", 0));
                                }
                                else if(paramName.equalsIgnoreCase("icon"))
                                {
                                    paramValue = parser.getAttributeValue(null, "value");
                                    course.setIcon(paramValue);
                                }
                                else if(paramName.equalsIgnoreCase("video"))
                                {
                                    paramValue = parser.getAttributeValue(null, "value");
                                    course.setVideo(paramValue);
                                }
                                else if(paramName.equalsIgnoreCase("set"))
                                {
                                    course.setSet(parser.getAttributeIntValue(null, "value", 0));
                                }

                            }
                            else if(isRest)
                            {
                                String paramName = parser.getAttributeValue(0);

                                if(paramName.equalsIgnoreCase("resttime"))
                                {
                                    course.setResttime(parser.getAttributeIntValue(null, "value", 0));
                                }
                                else if(paramName.equalsIgnoreCase("desc"))
                                {
                                    course.setRestdesc(parser.getAttributeValue(null, "value"));
                                }
                            }
                            else if(isHeart)
                            {
                                String paramName = parser.getAttributeValue(0);

                                if(paramName.equalsIgnoreCase("1"))
                                {
                                    heartBeats.add(parser.getAttributeIntValue(null, "value", 0));
                                }

                                else if(paramName.equalsIgnoreCase("2"))
                                {
                                    heartBeats.add(parser.getAttributeIntValue(null, "value", 0));
                                }

                                else if(paramName.equalsIgnoreCase("3"))
                                {
                                    heartBeats.add(parser.getAttributeIntValue(null, "value", 0));
                                }

                                else if(paramName.equalsIgnoreCase("4"))
                                {
                                    heartBeats.add(parser.getAttributeIntValue(null, "value", 0));
                                }

                                else if(paramName.equalsIgnoreCase("5"))
                                {
                                    heartBeats.add(parser.getAttributeIntValue(null, "value", 0));
                                }

                                else if(paramName.equalsIgnoreCase("6"))
                                {
                                    heartBeats.add(parser.getAttributeIntValue(null, "value", 0));
                                }

                                else if(paramName.equalsIgnoreCase("7"))
                                {
                                    heartBeats.add(parser.getAttributeIntValue(null, "value", 0));
                                }
                                else if(paramName.equalsIgnoreCase("8"))
                                {
                                    heartBeats.add(parser.getAttributeIntValue(null, "value", 0));
                                }
                                else if(paramName.equalsIgnoreCase("9"))
                                {
                                    heartBeats.add(parser.getAttributeIntValue(null, "value", 0));
                                }
                                else if(paramName.equalsIgnoreCase("10"))
                                {
                                    heartBeats.add(parser.getAttributeIntValue(null, "value", 0));
                                }
                                else
                                {

                                }
                            }
                            else
                            {
                                // workout param
                                String paramName = parser.getAttributeValue(0);

                                if(paramName.equalsIgnoreCase("title"))
                                {
                                    workout.setTitle(parser.getAttributeValue(null, "value"));
                                }
                                else if(paramName.equalsIgnoreCase("repeat"))
                                {
                                    workout.setRepeat(parser.getAttributeIntValue(null, "value", 0));
                                }
                                else if(paramName.equalsIgnoreCase("heart_avg"))
                                {
                                    workout.setHerart_avg(parser.getAttributeIntValue(null, "value", 0));
                                }
                                else if(paramName.equalsIgnoreCase("icon"))
                                {
                                    workout.setIcon(parser.getAttributeValue(null, "value"));
                                }
                                else if(paramName.equalsIgnoreCase("video"))
                                {
                                    workout.setVideo(parser.getAttributeValue(null, "value"));
                                }
                                else if(paramName.equalsIgnoreCase("tracker"))
                                {
                                    workout.setTracker(parser.getAttributeValue(null, "value"));
                                }
                            }

                        }
                        else if(tagname.equalsIgnoreCase("workout"))
                        {
                            isCos = false;
                            workout = new Workout();
                        }

                        break;

                    case XmlResourceParser.TEXT:
                        break;

                    case XmlResourceParser.END_TAG:
                        tagname = parser.getName();

                        if(tagname.equalsIgnoreCase("course"))
                        {
                            course.setWorkouts(workouts);
                            course.setHeartBeats(heartBeats);

                            isEnd = true;
                            if(course !=null)
                            {
                                courses.add(course);
                            }
                        }
                        else if(tagname.equalsIgnoreCase("rest"))
                        {
                            isRest = false;
                        }

                        else if(tagname.equalsIgnoreCase("param"))
                        {

                        }
                        else if(tagname.equalsIgnoreCase("heartbeat"))
                        {
                            isHeart = false;
                        }

                        else if(tagname.equalsIgnoreCase("workout"))
                        {

                            if(workout != null)
                            {
                                workouts.add(workout);
                            }
                        }
                        break;

                    default:
                        break;
                }
                eventType = parser.next();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
