package com.samsung.android.clab.homex;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Course implements Parcelable {
    private String title;
    private String desc;
    private String level;
    private int step;

    private String icon;
    private String video;
    private int set;

    List<Workout> workoutList;

    private int resttime;
    private String restdesc;
    private int bestTime;
    private ArrayList<Integer> heartBeats = new ArrayList<>();
    private ArrayList<Workout> workouts = new ArrayList<>();

    public ArrayList<Integer> getHeartBeats() {
        return heartBeats;
    }

    public void setHeartBeats(ArrayList<Integer> heartBeats) {
        this.heartBeats = heartBeats;
    }

    public ArrayList<Workout> getWorkouts() {
        return workouts;
    }

    public void setWorkouts(ArrayList<Workout> workouts) {
        this.workouts = workouts;
    }	
    public int getResttime() {
        return resttime;
    }

    public void setResttime(int resttime) {
        this.resttime = resttime;
    }

    public String getRestdesc() {
        return restdesc;
    }

    public void setRestdesc(String restdesc) {
        this.restdesc = restdesc;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public int getStep() {
        return step;
    }

    public void setStep(int step) {
        this.step = step;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public int getSet() {
        return set;
    }

    public void setSet(int set) {
        this.set = set;
    }

    public int getBestTime() { return bestTime; }

    public void setBestTime (int second ) {
        bestTime = second;
    }

    public List<Workout> getWorkoutList() {
        return workoutList;
    }

    public void setWorkoutList(List<Workout> workoutList) {
        this.workoutList = workoutList;
    }


    @Override
    public String toString() {
        return "title : " + title + "\n"
                + "desc :" + desc + "\n"
                + "level : " + level + "\n"
                + "icon : " + icon + "\n"
                + "video : " + video + "\n"
                + "step : " + step + "\n"
                + "set : " + set;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeString(this.desc);
        dest.writeString(this.level);
        dest.writeInt(this.step);
        dest.writeString(this.icon);
        dest.writeString(this.video);
        dest.writeInt(this.set);
        dest.writeInt(this.resttime);
        dest.writeString(this.restdesc);
        dest.writeList(this.heartBeats);
        dest.writeList(this.workouts);
    }

    public Course() {
    }

    protected Course(Parcel in) {
        this.title = in.readString();
        this.desc = in.readString();
        this.level = in.readString();
        this.step = in.readInt();
        this.icon = in.readString();
        this.video = in.readString();
        this.set = in.readInt();
        this.resttime = in.readInt();
        this.restdesc = in.readString();
        this.heartBeats = new ArrayList<Integer>();
        in.readList(this.heartBeats, Integer.class.getClassLoader());
        this.workouts = new ArrayList<Workout>();
        in.readList(this.workouts, Workout.class.getClassLoader());
    }

    public static final Parcelable.Creator<Course> CREATOR = new Parcelable.Creator<Course>() {
        @Override
        public Course createFromParcel(Parcel source) {
            return new Course(source);
        }

        @Override
        public Course[] newArray(int size) {
            return new Course[size];
        }
    };
}
