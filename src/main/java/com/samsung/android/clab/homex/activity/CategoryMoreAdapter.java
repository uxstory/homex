package com.samsung.android.clab.homex.activity;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.samsung.android.clab.homex.Category;
import com.samsung.android.clab.homex.R;

import java.util.ArrayList;

public class CategoryMoreAdapter extends RecyclerView.Adapter<CategoryMoreAdapter.ViewHolder> {

    private Context mContext;
    private ArrayList<Category> mCategoryList;
    private AdapterListener mListener;

    public interface AdapterListener {
        void onItemClick(Category item, int position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder
    {
        public ImageView mImagerView;
        public TextView mTextView1;

        public ViewHolder(View itemView)
        {
            super(itemView);
            mImagerView = itemView.findViewById(R.id.icon_mycontents);

            mTextView1 = itemView.findViewById(R.id.title_exercise);
        }

    }


    public CategoryMoreAdapter(Context context, ArrayList<Category> exampleList)
    {
        this.mContext = context;
        mCategoryList = exampleList;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewtype) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_more_item, parent, false);
        ViewHolder cvh = new ViewHolder(v);

        return  cvh;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Category currentItem = mCategoryList.get(position);

        int resID = mContext.getResources().getIdentifier(currentItem.getIcon(), "drawable", mContext.getPackageName());
        holder.mImagerView.setImageResource(resID);
        holder.mTextView1.setText(currentItem.getTitle());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mListener != null) {
                    mListener.onItemClick(currentItem, position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mCategoryList.size();
    }

    public void setAdapterListener(AdapterListener listener) {
        this.mListener = listener;
    }
}
