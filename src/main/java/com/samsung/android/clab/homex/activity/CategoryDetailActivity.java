package com.samsung.android.clab.homex.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.samsung.android.clab.homex.Course;
import com.samsung.android.clab.homex.R;
import com.samsung.android.clab.homex.adapter.ProgramDetailAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CategoryDetailActivity extends AppCompatActivity {

    @BindView(R.id.categoryDetailList)
    RecyclerView categoryDetailList;
    @BindView(R.id.textViewTitle)
    TextView textViewTitle;
    @BindView(R.id.textViewSet)
    TextView textViewSet;

    private Course course;
    private ProgramDetailAdapter mAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_detail);
        ButterKnife.bind(this);

        if(getIntent() != null) {
            course = getIntent().getParcelableExtra("course");

            textViewTitle.setText(course.getTitle());
            textViewSet.setText("총 " + course.getSet() + "세트");
        }

        categoryDetailList.setHasFixedSize(true);
        categoryDetailList.setLayoutManager(new LinearLayoutManager(this));

        mAdapter = new ProgramDetailAdapter(course.getWorkouts(), course);
        categoryDetailList.setAdapter(mAdapter);
    }

    @OnClick(R.id.buttonBack)
    public void onClick() {
        finish();
    }
}
