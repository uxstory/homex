package com.samsung.android.clab.homex;

import android.graphics.drawable.Drawable;

public class MyContentsItem {

    private int iconExercise;

    private String titleExercise;


    public MyContentsItem(int icon, String title)
    {
        this.iconExercise = icon;
        this.titleExercise = title;
    }
    public void setIconExercise(int icon)
    {
        iconExercise = icon;
    }

    public void setTitleExercise(String title)
    {
        titleExercise = title;
    }

    public int getIconExercise()
    {
        return iconExercise;
    }

    public String getTitleExercise()
    {
        return titleExercise;
    }

}
