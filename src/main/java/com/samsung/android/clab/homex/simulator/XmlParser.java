package com.samsung.android.clab.homex.simulator;

import android.util.Log;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class XmlParser {
    private static final String TAG = "XmlParser";

    private Node mRoot;

    private Document mDoc;

    public static class CscNodeList implements NodeList {
        private ArrayList<Node> children = new ArrayList<Node>();

        void appendChild(Node newChild) {
            children.add(newChild);
        }

        @Override
        public int getLength() {
            return children.size();
        }

        @Override
        public Node item(int index) {
            return children.get(index);
        }
    }

    public XmlParser(String fileName) {
        // mFileName = fileName;
        try {
            update(fileName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void update(String fileName) throws ParserConfigurationException, SAXException,
            IOException {

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();

        File fe = new File(fileName);
        if (fe.exists()) {
            // 20100420 sylee null check for filename
            // if(fileName != null)
            // Log.e(TAG, "fileName + " + fileName); // logging test file, User
            // Information
            Log.e(TAG, "update(): xml file exist");
            mDoc = builder.parse(new File(fileName));
            mRoot = mDoc.getDocumentElement();
        } else {
            Log.e(TAG, "update(): xml file not exist");
        }
    }

    public String get(String path) {
        Node node = search(path);

        if (null == node)
            return null;

        Node firstChild = node.getFirstChild();
        return (null != firstChild ? firstChild.getNodeValue() : null);
    }

    public Node search(String path) {
        if (path == null)
            return null;

        Node node = mRoot;
        StringTokenizer tokenizer = new StringTokenizer(path, ".");

        while (tokenizer.hasMoreTokens()) {
            String token = tokenizer.nextToken();

            if (node == null) {
                return null;
            }
            node = search(node, token);
        }

        return node;
    }

    public Node search(Node parent, String name) {
        if (parent == null)
            return null;

        NodeList children = parent.getChildNodes();

        if (children != null) {
            int n = children.getLength();

            for (int i = 0; i < n; i++) {
                Node child = children.item(i);

                if (child.getNodeName().equals(name)) {
                    return child;
                }
            }
        }

        return null;
    }

    public NodeList searchList(Node parent, String name) {
        if (parent == null)
            return null;

        try {
            CscNodeList list = new CscNodeList();
            NodeList children = parent.getChildNodes();

            if (children != null) {
                int n = children.getLength();

                for (int i = 0; i < n; i++) {
                    Node child = children.item(i);

                    if (child.getNodeName().equals(name)) {
                        try {
                            list.appendChild(child);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            return list;
        } catch (Exception e) {
            return null;
        }
    }

    public String getValue(Node node) {
        if (node == null) {
            return null;
        }
        // Moon seung-bae add for & parsing problem
        if (node.getChildNodes().getLength() > 1) {
            String stringValue = new String();
            int idx;

            for (idx = 0; idx < node.getChildNodes().getLength(); idx++) {
                stringValue = stringValue + node.getChildNodes().item(idx).getNodeValue();
            }
            return stringValue;
        }
        // Moon seung-bae add for & parsing problem

        Node firstChild = node.getFirstChild();
        return (null != firstChild ? firstChild.getNodeValue() : null);
    }

    public String getAttrbute(String tagPath, int index, int mode) {
        String attribute = null;
        String[] tagSplit = tagPath.split("[.]");
        int tagCount = tagSplit.length;

        if (tagCount-- < 3)
            return attribute;

        // tagNode.tagList.tagAttr
        // ex)Settings.Main.Sound.MessageTone.src
        String tagAttr = tagSplit[tagCount--];
        String tagList = tagSplit[tagCount];
        String tagNode = null;

        for (int i = 0; i < tagCount; i++) {
            if (tagNode == null)
                tagNode = tagSplit[i];
            else
                tagNode = tagNode + "." + tagSplit[i];
        }

        NodeList nodeList = searchList(search(tagNode), tagList);
        if (nodeList != null && nodeList.getLength() > index) {
            Element list = (Element) nodeList.item(index);
            // Element attr = (Element)
            // list.getElementsByTagName(tagList).item(0);
            attribute = list.getAttribute(tagAttr);
        }

        // Only file name(ex, RingTone, MessageTone)
        // ex)RingTone src = /customer/RingTone.mp3
        if (attribute != null && mode == 1) {
            String[] attrSlash = attribute.split("/");
            int cntSlash = attrSlash.length - 1;

            if (attrSlash[cntSlash] != null) {
                String[] attrSplit = attrSlash[cntSlash].split("[.]");
                if (attrSplit[0] != null)
                    attribute = attrSplit[0];
            }
        }

        Log.d(TAG, tagList + ": " + attribute);
        return attribute;
    }

}

