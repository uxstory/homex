package com.samsung.android.clab.homex.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.samsung.android.clab.homex.Constants;
import com.samsung.android.clab.homex.R;
import com.samsung.android.clab.homex.UnityPlayerActivity;
import com.samsung.android.clab.homex.fragment.ContentsFragment;
import com.samsung.android.clab.homex.fragment.HomeFragment;

import java.util.HashMap;
import java.util.Iterator;

public class MainActivity extends AppCompatActivity {
    
    private FragmentManager fragmentManager = getSupportFragmentManager();

    private HomeFragment homeFragment = new HomeFragment();
    private ContentsFragment contentsFragment = new ContentsFragment();
    private ProfileActivity profileFragment = new ProfileActivity();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        BottomNavigationView navView = findViewById(R.id.nav_view);


        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.frame_layout2, homeFragment).commitAllowingStateLoss();

        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        // USB HOST Receiver
        registerReceiver(mUsbDeviceReceiver, new IntentFilter(UsbManager.ACTION_USB_DEVICE_ATTACHED));
        registerReceiver(mUsbDeviceReceiver, new IntentFilter(UsbManager.ACTION_USB_DEVICE_DETACHED));

        setPreference(Constants.PREF_KEY_USER_GENDER, "F");
        setPreference(Constants.PREF_KEY_USER_WEIGHT, "53");
        setPreference(Constants.PREF_KEY_USER_TALL, "161");
        setPreference(Constants.PREF_KEY_USER_NAME, "홍길동");
        setPreference(Constants.PREF_KEY_USER_AGE, "31");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mUsbDeviceReceiver);
    }


    private final BroadcastReceiver mUsbDeviceReceiver =
            new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    String action = intent.getAction();
                    if (UsbManager.ACTION_USB_DEVICE_ATTACHED.equals(action)) {
                        Log.d("Log","USB_DEVICE_ATTACHED");
                    }else if (UsbManager.ACTION_USB_DEVICE_DETACHED.equals(action)) {
                        Log.d("Log","USB_DEVICE_DETACHED");
                        finish();
                    }
                }
            };

    public void replaceFragment(Fragment fragment)
    {
        FragmentManager fragmentManager = getSupportFragmentManager();

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        fragmentTransaction.replace(R.id.frame_layout2, fragment).commitAllowingStateLoss();

    }

    public void Start_Activity()
    {
        Intent home = new Intent(getApplicationContext(), UnityPlayerActivity.class);

        startActivity(home);

//        finish();
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {

            FragmentTransaction transaction = fragmentManager.beginTransaction();

            switch (item.getItemId()) {
                case R.id.navigation_home:
                    transaction.replace(R.id.frame_layout2, homeFragment).commitAllowingStateLoss();

                    return true;
                case R.id.navigation_contents:
                    transaction.replace(R.id.frame_layout2, contentsFragment).commitAllowingStateLoss();

                    return true;
                case R.id.navigation_profile:
                    transaction.replace(R.id.frame_layout2, profileFragment).commitAllowingStateLoss();

                    return true;
            }
            return false;
        }
    };

    /**
     * 데이터를 저장한다.
     * @param key
     * @param value
     */
    private void setPreference(String key, String value) {
        SharedPreferences pref = getSharedPreferences(Constants.PREFERENCE, MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(key, value);
        editor.commit();
    }
}
