package com.samsung.android.clab.homex;

public class Rest {

    private int resttime = 0;

    private String desc = "";


    public int getResttime() {
        return resttime;
    }

    public void setResttime(int resttime) {
        this.resttime = resttime;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
