package com.samsung.android.clab.homex.simulator;

import android.os.SystemClock;
import android.util.Log;
import android.util.Xml;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xmlpull.v1.XmlSerializer;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class MotionTester {
    private static final String TAG = "MotionData";

    private XmlParser mXmlParser = null;

    public static String PATH_TEST_XML_FILE_PATH = "/sdcard/HomexSimulator";
    public static String PATH_TEST_XML_FILE_NAME = "homextest.xml";

    static final String PATH_MOTION_DATA = "MotionData";
    static final String PATH_JOINT_DATA = "JointData";

    static final String PATH_JOINT_DATA_TIME = "Time";

    private List<Joint> mJointList;

    public List<Joint> getSavedJointList() {
        return mJointList;
    }


    public float mMinX = 0;

    public float mMaxX = 0;

    public float mMinY = 0;

    public float mMaxY = 0;

    public MotionTester() {
    }

    public void loadJointData(String fileName) {
        //JointList.add()

        if (fileName != null) {
            mXmlParser = new XmlParser(PATH_TEST_XML_FILE_PATH + "/" + fileName);
        } else {
            mXmlParser = new XmlParser(PATH_TEST_XML_FILE_PATH + "/" + PATH_TEST_XML_FILE_NAME);
        }
        mJointList = new ArrayList<Joint>();

        Node jointDataNode = mXmlParser.search(PATH_MOTION_DATA);
        NodeList jointNodeList = mXmlParser.searchList(jointDataNode, PATH_JOINT_DATA);
        if (jointNodeList != null && jointNodeList.getLength() > 0) {
            for (int index=0; index < jointNodeList.getLength(); index++) {

                Joint joint = new Joint();
                Node node = jointNodeList.item(index);

                Node node_time = mXmlParser.search(node, PATH_JOINT_DATA_TIME);
                String sNodeTime = mXmlParser.getValue(node_time);
                joint.mTime = Long.parseLong(sNodeTime);

                for (int i = 0; i < JointName.length; i++)
                {
                    Node node_head = mXmlParser.search(node, JointName[i]);
                    String nodedata = mXmlParser.getValue(node_head);
                    nodedata = nodedata.trim().replace("(", "").replace(")", "");
                    String sNodeHeadSp[] = nodedata.split(",");
                    if (sNodeHeadSp.length >= 2) {
                        float x = Float.parseFloat(sNodeHeadSp[0]);
                        float y = Float.parseFloat(sNodeHeadSp[1]);
                        joint.setJoint(i,
                                x,
                                y,
                                Float.parseFloat(sNodeHeadSp[2]));
                        if (x < mMinX) {
                            mMinX = x;
                        }
                        if (x > mMaxX) {
                            mMaxX = x;
                        }
                        if (y < mMinY) {
                            mMinY = y;
                        }
                        if (y > mMaxY) {
                            mMaxY = y;
                        }
                    }
                }
                mJointList.add(joint);
            }
        }

    }

    static public String[] JointName = {"Head", "Neck", "Torso", "Waist",
            "LeftCollar", "LeftShoulder", "LeftElbow", "LeftWrist", "LeftHand", "LeftFingertip",
            "RightCollar","RightShoulder","RightElbow","RightWrist","RightHand", "RightFingertip",
            "LeftHip","LeftKnee","LeftAnkle","LeftFoot",
            "RightHip","RightKnee","RightAnkle","RightFoot"};

    private FileWriter mFileWriter = null;

    private XmlSerializer mXmlserializer = null;

    private StringWriter mStringWriter = null;

    public void openXmlForStoring(String filename) {
        try {
            //new folder
            File homexFoler = makeFolder(PATH_TEST_XML_FILE_PATH);
            File file = makeFile(homexFoler, (PATH_TEST_XML_FILE_PATH + "/" + filename + ".xml"));

            mFileWriter = new FileWriter(file);
            mXmlserializer = Xml.newSerializer();
            mStringWriter = new StringWriter();
            mXmlserializer.setOutput(mStringWriter);

            mXmlserializer.startDocument("UTF-8", true);
            mXmlserializer.startTag("", "HomexTest");
            mXmlserializer.startTag("", "MotionData");
            final long currentTime = SystemClock.uptimeMillis();
            SimpleDateFormat dateformat = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss");
            String formatedTime = dateformat.format(System.currentTimeMillis());
            mXmlserializer.attribute("", "time", formatedTime);
            mXmlserializer.attribute("", "description", "test");

        } catch (IOException e) {
            Log.e(TAG, "openXmlForStoring IOException " + e.toString());
        }
    }

    private File makeFolder(String folerPath){
        File folder = new File(folerPath);
        if (!folder.exists())
        {
            folder.mkdirs();
        }else{
        }
        return folder;
    }

    private File makeFile(File dir , String file_path){
        File file = null;
        if(dir.isDirectory()){
            file = new File(file_path);
            if(file != null && !file.exists()){
                Log.i( TAG , "!file.exists" );
                try {
                    file.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return file;
    }

    public void closeXmlForStoring() {
        try {
            mXmlserializer.endTag("", "MotionData");
            mXmlserializer.endTag("", "HomexTest");
            mXmlserializer.endDocument();

            mFileWriter.write(mStringWriter.toString());
            mFileWriter.close();
        } catch (IOException e) {
            Log.e(TAG, "closeXmlForStoring IOException " + e.toString());
        }

    }

    public void writeJointDataToXml(String[] type) {
        final long currentTime = SystemClock.uptimeMillis();

        try {
            mXmlserializer.startTag("", "JointData");
            mXmlserializer.startTag("", "Time");
            mXmlserializer.text(Long.toString(currentTime));
            mXmlserializer.endTag("", "Time");
            for (int i = 0; i < type.length; i++)
            {
                Log.d("body", JointName[i] + " : " + type[i].toString());
                mXmlserializer.startTag("", JointName[i]);
                mXmlserializer.attribute("", "index", Integer.toString(i));
                mXmlserializer.text(type[i].toString());
                mXmlserializer.endTag("", JointName[i]);
            }

            mXmlserializer.endTag("", "JointData");
        } catch (IOException e) {
            Log.e(TAG, "writeJointDataToXml IOException " + e.toString());
        }
    }


    public void writeJointDataToXml(String[] type, String time) {
        try {
            mXmlserializer.startTag("", "JointData");
            mXmlserializer.startTag("", "Time");
            mXmlserializer.text(time);
            mXmlserializer.endTag("", "Time");
            for (int i = 0; i < type.length; i++)
            {
                Log.d("body", JointName[i] + " : " + type[i].toString());
                mXmlserializer.startTag("", JointName[i]);
                mXmlserializer.attribute("", "index", Integer.toString(i));
                mXmlserializer.text(type[i].toString());
                mXmlserializer.endTag("", JointName[i]);
            }

            mXmlserializer.endTag("", "JointData");
        } catch (IOException e) {
            Log.e(TAG, "writeJointDataToXml IOException " + e.toString());
        }
    }

}

