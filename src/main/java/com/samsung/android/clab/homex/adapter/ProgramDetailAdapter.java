package com.samsung.android.clab.homex.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.samsung.android.clab.homex.Course;
import com.samsung.android.clab.homex.R;
import com.samsung.android.clab.homex.Workout;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProgramDetailAdapter extends RecyclerView.Adapter<ProgramDetailAdapter.ProgramDetailHolder> {
    private ArrayList<Workout> mItems;
    private Course mCourse;

    public ProgramDetailAdapter(ArrayList<Workout> items, Course course) {
        this.mItems = items;
        this.mCourse = course;
    }

    @NonNull
    @Override
    public ProgramDetailHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        Context context = viewGroup.getContext() ;

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) ;
        View view = inflater.inflate(R.layout.program_detail_item, viewGroup, false) ;

        return new ProgramDetailHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProgramDetailHolder holder, int position) {
        Workout item = mItems.get(position);

        Context mContext = holder.itemView.getContext();
        int resID = mContext.getResources().getIdentifier(item.getIcon(), "drawable", mContext.getPackageName());
        if(resID != 0) {
            holder.imageViewIcon.setImageResource(resID);
        } else {
            holder.imageViewIcon.setBackgroundResource(R.drawable.img_sample);
        }
        holder.textViewTitle.setText(item.getTitle());
        holder.textViewRepeat.setText(item.getRepeat() + "회 반복");

        if(position == mItems.size() - 1) {
            holder.divider.setVisibility(View.GONE);
        } else {
            holder.divider.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    class ProgramDetailHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imageViewIcon)
        ImageView imageViewIcon;
        @BindView(R.id.textViewTitle)
        TextView textViewTitle;
        @BindView(R.id.textViewRepeat)
        TextView textViewRepeat;
        @BindView(R.id.divider)
        View divider;

        public ProgramDetailHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
