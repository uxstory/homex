package com.samsung.android.clab.homex;

import java.util.List;

public class Successcondition {

    private String method;

    private String skelaton_a;
    private String skelaton_b;
    private String check_position;

    private List<CheckMethod> checkMethodList;


    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getSkelaton_a() {
        return skelaton_a;
    }

    public void setSkelaton_a(String skelaton_a) {
        this.skelaton_a = skelaton_a;
    }

    public String getSkelaton_b() {
        return skelaton_b;
    }

    public void setSkelaton_b(String skelaton_b) {
        this.skelaton_b = skelaton_b;
    }

    public String getCheck_position() {
        return check_position;
    }

    public void setCheck_position(String check_position) {
        this.check_position = check_position;
    }

    public List<CheckMethod> getCheckMethodList() {
        return checkMethodList;
    }

    public void setCheckMethodList(List<CheckMethod> checkMethodList) {
        this.checkMethodList = checkMethodList;
    }

    @Override
    public String toString()
    {
        return "method : " + method +"\n"
                + "skeleton_a" + skelaton_a + "\n"
                + "skeleton_b" + skelaton_b + "\n"
                + "check_position" + check_position;
    }
}
