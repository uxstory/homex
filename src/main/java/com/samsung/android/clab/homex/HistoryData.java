package com.samsung.android.clab.homex;

import java.util.ArrayList;

public class HistoryData {

    private String yearMonth;
    private ArrayList<HistoryDetailData> historyDetailData;

    public String getYearMonth() {
        return yearMonth;
    }

    public void setYearMonth(String yearMonth) {
        this.yearMonth = yearMonth;
    }

    public ArrayList<HistoryDetailData> getHistoryDetailData() {
        return historyDetailData;
    }

    public void setHistoryDetailData(ArrayList<HistoryDetailData> historyDetailData) {
        this.historyDetailData = historyDetailData;
    }
}
