package com.samsung.android.clab.homex;

public class Log {

    public static final boolean SHOW_ALL_LOG = true;

    public static final boolean SHOW_LOG_UPDATE_POSITION = true;

    public static final boolean SHOW_LOG_SEQU = true;

    public static final boolean SHOW_LOG_XML_PARSER = true;

    public static final boolean SHOW_LOG_COMPARE_VALUE = true;

    public static final boolean SHOW_LOG_COMPARE_DISTANCE = true;

    public static final boolean SHOW_LOG_MATCH_DISTANCE = true;

    public static final boolean SHOW_LOG_MATCH = true;

    public static final boolean SHOW_LOG_MATCH_DETAIL = true;

    public static void e(String tag, String msg) {
        if (SHOW_ALL_LOG == false)
            return;
        android.util.Log.e(tag, msg);
    }

    public static void d(String tag, String msg) {
        if (SHOW_ALL_LOG == false)
            return;
        if (tag.equalsIgnoreCase("updatePosition")) {
            if (SHOW_LOG_UPDATE_POSITION)
                android.util.Log.d(tag, msg);
        } else if (tag.equalsIgnoreCase("SEQU")) {
            if (SHOW_LOG_SEQU)
                android.util.Log.d(tag, msg);
        }

        else if (tag.equalsIgnoreCase("StartTagName") || tag.equalsIgnoreCase("EndTagName")
            || tag.equalsIgnoreCase("paramName") || tag.equalsIgnoreCase("paramValue")) {
            if (SHOW_LOG_XML_PARSER)
                android.util.Log.d(tag, msg);
        }

        else if (tag.equalsIgnoreCase("CompareValue")) {
            if (SHOW_LOG_COMPARE_VALUE)
                android.util.Log.d(tag, msg);
        } else if (tag.equalsIgnoreCase("CompareDistance")) {
            if (SHOW_LOG_COMPARE_DISTANCE)
                android.util.Log.d(tag, msg);
        } else if (tag.equalsIgnoreCase("MatchDistance")) {
            if (SHOW_LOG_MATCH_DISTANCE)
                android.util.Log.d(tag, msg);
        }

        else if (tag.equalsIgnoreCase("match")) {
            if (SHOW_LOG_MATCH)
                android.util.Log.d(tag, msg);
        } else if (tag.equalsIgnoreCase("matchdetail")) {
            if (SHOW_LOG_MATCH_DETAIL)
                android.util.Log.d(tag, msg);
        } else {
            android.util.Log.d(tag, msg);
        }
    }
}
