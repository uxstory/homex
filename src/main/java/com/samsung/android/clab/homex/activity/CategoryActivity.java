package com.samsung.android.clab.homex.activity;

import android.content.Intent;
import android.content.res.XmlResourceParser;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;

import com.samsung.android.clab.homex.Course;
import com.samsung.android.clab.homex.Program;
import com.samsung.android.clab.homex.R;
import com.samsung.android.clab.homex.Workout;
import com.samsung.android.clab.homex.adapter.ProgramAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CategoryActivity extends AppCompatActivity implements ProgramAdapter.AdapterListener {

    @BindView(R.id.list) RecyclerView list;
    @BindView(R.id.textViewTitle) TextView textViewTitle;
    @BindView(R.id.textViewDesc) TextView textViewDesc;

    private ProgramAdapter mAdapter;
    private ArrayList<Program> programs = new ArrayList<>();
    private ArrayList<Workout> workouts;
    private ArrayList<Integer> heartBeats;
    private Course course;
    private Workout workout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        ButterKnife.bind(this);

        list.setLayoutManager(new LinearLayoutManager(this));

        if(getIntent() != null) {
            programs = getIntent().getParcelableArrayListExtra("program");

            textViewTitle.setText(getIntent().getStringExtra("title"));
            textViewDesc.setText(getIntent().getStringExtra("description"));
        }

        mAdapter = new ProgramAdapter(programs, this);
        list.setAdapter(mAdapter);
    }

    @Override
    public void onItemClick(Program item) {
        int resId = getResources().getIdentifier(item.getCourse(), "xml", getPackageName());
        Course tmpCourse = readCourse(getResources().getXml(resId));
        Log.d("Log", "temp : " + tmpCourse);

        Intent intent = new Intent(this, CategoryDetailActivity.class);
        intent.putExtra("course", tmpCourse);
        startActivity(intent);
    }

    @OnClick(R.id.buttonBack)
    public void onClick() {
        finish();
    }

    public Course readCourse(XmlResourceParser parser)
    {
        Log.d("START", "read Course Start");
        try {
            int eventType = parser.getEventType();
            boolean isEnd = false;
            boolean isCos = false;
            boolean isRest = false;

            boolean isHeart = false;

            while (eventType != XmlResourceParser.END_DOCUMENT && isEnd == false) {
                String tagname;

                switch(eventType)
                {
                    case XmlResourceParser.START_TAG:
                        tagname = parser.getName();

                        if(tagname != null)
                        {
                            Log.d("tagName", tagname);
                        }

                        if(tagname.equalsIgnoreCase("course"))
                        {
                            course = new Course();
                            workouts = new ArrayList<>();
                            heartBeats = new ArrayList<>();

                            isCos = true;
                        }

                        else if(tagname.equalsIgnoreCase("rest"))
                        {
                            isRest = true;
                        }
                        else if(tagname.equalsIgnoreCase("heartbeat"))
                        {
                            isCos = false;
                            isHeart = true;
                        }

                        else if(tagname.equalsIgnoreCase("param"))
                        {
                            if(isCos)
                            {
                                String paramName = parser.getAttributeValue(0);
                                String paramValue;

                                if(paramName.equalsIgnoreCase("title"))
                                {
                                    paramValue = parser.getAttributeValue(null, "value");
                                    course.setTitle(paramValue);
                                }
                                else if(paramName.equalsIgnoreCase("desc"))
                                {
                                    paramValue = parser.getAttributeValue(null, "value");
                                    course.setDesc(paramValue);
                                }
                                else if(paramName.equalsIgnoreCase("level"))
                                {
                                    paramValue = parser.getAttributeValue(null, "value");
                                    course.setLevel(paramValue);
                                }
                                else if(paramName.equalsIgnoreCase("steps"))
                                {
                                    course.setStep(parser.getAttributeIntValue(null, "value", 0));
                                }
                                else if(paramName.equalsIgnoreCase("icon"))
                                {
                                    paramValue = parser.getAttributeValue(null, "value");
                                    course.setIcon(paramValue);
                                }
                                else if(paramName.equalsIgnoreCase("video"))
                                {
                                    paramValue = parser.getAttributeValue(null, "value");
                                    course.setVideo(paramValue);
                                }
                                else if(paramName.equalsIgnoreCase("set"))
                                {
                                    course.setSet(parser.getAttributeIntValue(null, "value", 0));
                                }

                            }
                            else if(isRest)
                            {
                                String paramName = parser.getAttributeValue(0);

                                if(paramName.equalsIgnoreCase("resttime"))
                                {
                                    course.setResttime(parser.getAttributeIntValue(null, "value", 0));
                                }
                                else if(paramName.equalsIgnoreCase("desc"))
                                {
                                    course.setRestdesc(parser.getAttributeValue(null, "value"));
                                }
                            }
                            else if(isHeart)
                            {
                                String paramName = parser.getAttributeValue(0);

                                if(paramName.equalsIgnoreCase("1"))
                                {
                                    heartBeats.add(parser.getAttributeIntValue(null, "value", 0));
                                }

                                else if(paramName.equalsIgnoreCase("2"))
                                {
                                    heartBeats.add(parser.getAttributeIntValue(null, "value", 0));
                                }

                                else if(paramName.equalsIgnoreCase("3"))
                                {
                                    heartBeats.add(parser.getAttributeIntValue(null, "value", 0));
                                }

                                else if(paramName.equalsIgnoreCase("4"))
                                {
                                    heartBeats.add(parser.getAttributeIntValue(null, "value", 0));
                                }

                                else if(paramName.equalsIgnoreCase("5"))
                                {
                                    heartBeats.add(parser.getAttributeIntValue(null, "value", 0));
                                }

                                else if(paramName.equalsIgnoreCase("6"))
                                {
                                    heartBeats.add(parser.getAttributeIntValue(null, "value", 0));
                                }

                                else if(paramName.equalsIgnoreCase("7"))
                                {
                                    heartBeats.add(parser.getAttributeIntValue(null, "value", 0));
                                }
                                else if(paramName.equalsIgnoreCase("8"))
                                {
                                    heartBeats.add(parser.getAttributeIntValue(null, "value", 0));
                                }
                                else if(paramName.equalsIgnoreCase("9"))
                                {
                                    heartBeats.add(parser.getAttributeIntValue(null, "value", 0));
                                }
                                else if(paramName.equalsIgnoreCase("10"))
                                {
                                    heartBeats.add(parser.getAttributeIntValue(null, "value", 0));
                                }
                                else
                                {

                                }
                            }
                            else
                            {
                                // workout param
                                String paramName = parser.getAttributeValue(0);

                                if(paramName.equalsIgnoreCase("title"))
                                {
                                    workout.setTitle(parser.getAttributeValue(null, "value"));
                                }
                                else if(paramName.equalsIgnoreCase("repeat"))
                                {
                                    workout.setRepeat(parser.getAttributeIntValue(null, "value", 0));
                                }
                                else if(paramName.equalsIgnoreCase("heart_avg"))
                                {
                                    workout.setHerart_avg(parser.getAttributeIntValue(null, "value", 0));
                                }
                                else if(paramName.equalsIgnoreCase("icon"))
                                {
                                    workout.setIcon(parser.getAttributeValue(null, "value"));
                                }
                                else if(paramName.equalsIgnoreCase("video"))
                                {
                                    workout.setVideo(parser.getAttributeValue(null, "value"));
                                }
                                else if(paramName.equalsIgnoreCase("tracker"))
                                {
                                    workout.setTracker(parser.getAttributeValue(null, "value"));
                                }
                            }

                        }
                        else if(tagname.equalsIgnoreCase("workout"))
                        {
                            isCos = false;
                            workout = new Workout();
                        }

                        break;

                    case XmlResourceParser.TEXT:
                        break;

                    case XmlResourceParser.END_TAG:
                        tagname = parser.getName();

                        if(tagname.equalsIgnoreCase("course"))
                        {
                            course.setWorkouts(workouts);
                            course.setHeartBeats(heartBeats);

                            isEnd = true;
                        }
                        else if(tagname.equalsIgnoreCase("rest"))
                        {
                            isRest = false;
                        }

                        else if(tagname.equalsIgnoreCase("param"))
                        {

                        }
                        else if(tagname.equalsIgnoreCase("heartbeat"))
                        {
                            isHeart = false;
                        }

                        else if(tagname.equalsIgnoreCase("workout"))
                        {

                            if(workout != null)
                            {
                                workouts.add(workout);
                            }
                        }
                        break;

                    default:
                        break;
                }
                eventType = parser.next();
            }

            return course;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }
}
