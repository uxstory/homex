package com.samsung.android.clab.homex.simulator;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.samsung.android.clab.homex.R;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.widget.AdapterView;
import android.widget.Toast;


public class StoredListActivity extends Activity {

    public static String PATH_TEST_XML_FILE_PATH = "/sdcard/HomexSimulator";

    private ArrayList<HashMap<String,String>> mList = new ArrayList<HashMap<String, String>>();


    private ListView mListview;
    SimpleAdapter mSimpleAdapter;
    private List<String> mFileNames = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.simulator_stored_list_activity);
        mListview = (ListView)findViewById(R.id.list_view);


        File root = new File(PATH_TEST_XML_FILE_PATH);
        File[] files = root.listFiles();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        if (files == null || files.length <= 0) {
            Toast.makeText(
                    this,
                    "no stored xml file",
                    Toast.LENGTH_LONG).show();
            finish();
            return;
        }
        for (int i = 0; i < files.length; i++)
        {
            HashMap<String,String> inputData = new HashMap<>();
            inputData.put("filename", files[i].getName());

            long fileLength = files[i].length();
            String description;
            if (fileLength >= 1024) {
                if (fileLength >= 1048576) {
                    description = format.format(files[i].lastModified())
                            + " size:" + Long.toString(fileLength / 1024 / 1024) + " MB";
                } else {
                    description = format.format(files[i].lastModified())
                            + " size:" + Long.toString(fileLength / 1024) + " KB";
                }
            } else {
                if (fileLength < 200) {
                    description = format.format(files[i].lastModified())
                            + " size:" + Long.toString(fileLength) + " B (no data)";
                } else {
                    description = format.format(files[i].lastModified())
                            + " size:" + Long.toString(fileLength) + " B";
                }
            }
            inputData.put("lastModified", description);
            mList.add(inputData);
            mFileNames.add(files[i].getName());
            Log.d("Files", "FileName:" + files[i].getName());
        }

        mSimpleAdapter = new SimpleAdapter(this,
                mList,android.R.layout.simple_list_item_2,
                new String[]{"filename","lastModified"}
                ,new int[]{android.R.id.text1,android.R.id.text2});

        mListview.setAdapter(mSimpleAdapter);
        mListview.setOnItemClickListener(mOnClickListener);

    }


    final private AdapterView.OnItemClickListener mOnClickListener
            = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> parent, View v, int position, long id) {

            Intent home = new Intent(getApplicationContext(), TesterActivity.class);
            home.putExtra("FileName", mFileNames.get(position));
            startActivity(home);
        }
    };

}
