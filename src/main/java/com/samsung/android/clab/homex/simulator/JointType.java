package com.samsung.android.clab.homex.simulator;

public enum JointType {
    JOINT_NONE,

    JOINT_HEAD,
    JOINT_NECK,
    JOINT_TORSO,
    JOINT_WAIST,

    JOINT_LEFT_COLLAR,
    JOINT_LEFT_SHOULDER,
    JOINT_LEFT_ELBOW,
    JOINT_LEFT_WRIST,
    JOINT_LEFT_HAND,
    JOINT_LEFT_FINGERTIP,

    JOINT_RIGHT_COLLAR,
    JOINT_RIGHT_SHOULDER,
    JOINT_RIGHT_ELBOW,
    JOINT_RIGHT_WRIST,
    JOINT_RIGHT_HAND,
    JOINT_RIGHT_FINGERTIP,

    JOINT_LEFT_HIP,
    JOINT_LEFT_KNEE,
    JOINT_LEFT_ANKLE,
    JOINT_LEFT_FOOT,

    JOINT_RIGHT_HIP,
    JOINT_RIGHT_KNEE,
    JOINT_RIGHT_ANKLE,
    JOINT_RIGHT_FOOT,

    JOINT_TYPE_MAX
}
