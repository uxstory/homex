package com.samsung.android.clab.homex;

import java.util.List;

public class Passcondition {

    private  boolean refered;

    private List<CheckMethod> checkMethodList;

    public boolean isRefered() {
        return refered;
    }

    public void setRefered(boolean refered) {
        this.refered = refered;
    }

    public List<CheckMethod> getCheckMethodList() {
        return checkMethodList;
    }

    public void setCheckMethodList(List<CheckMethod> checkMethodList) {
        this.checkMethodList = checkMethodList;
    }

    @Override
    public String toString() {
        return "refered : " + refered;
    }
}
