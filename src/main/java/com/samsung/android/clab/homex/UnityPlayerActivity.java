package com.samsung.android.clab.homex;

import android.Manifest;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.XmlResourceParser;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatButton;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.samsung.android.clab.homex.simulator.MotionTester;
import com.unity3d.player.UnityPlayer;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

/*-- START : Simulator --*/
/*-- END : Simulator --*/

public class UnityPlayerActivity extends Activity {

    //public static final String RawPath = "android.resource://com.samsung.android.clab.homex/raw/";
    public static final String RawPath = "file:///sdcard/Homex/";
    public static String PATH_XML_FILE_PATH = "/sdcard/Homex/";

    public static final Uri SQUAT = Uri.parse("android.resource://com.samsung.android.clab.homex/raw/squat");
    public static final Uri KNEE_UP = Uri.parse("android.resource://com.samsung.android.clab.homex/raw/knee_up");
    public static final Uri WIDE_SQUAT = Uri.parse("android.resource://com.samsung.android.clab.homex/raw/wide_squat");

    public static final int HEARTRATE_STATE_UNIDENTIFIED = 0;
    public static final int HEARTRATE_STATE_UNSTABLE = 1;
    public static final int HEARTRATE_STATE_STABLE = 2;

    public static final int LOAD_GOLF = 5;

    protected UnityPlayer mUnityPlayer; // don't change the name of this variable; referenced from native code

    public static Context mContext;

    //public String[] myBody;

    public String[] JointName = {"Head", "Neck", "Torso", "Waist",
            "LeftCollar", "LeftShoulder", "LeftElbow", " LeftWrist", "LeftHand", "LeftFingertip",
            "RightCollar", "RightShoulder", "RightElbow", "RightWrist", "RightHand", "RightFingertip",
            "LeftHip", "LeftKnee", "LeftAnkle", "LeftFoot",
            "RightHip", "RightKnee", "RightAnkle", "RightFoot"};
    public String PASS_MESSAGE = "그레잇";

    public Uri[] videoFile = {SQUAT, KNEE_UP, WIDE_SQUAT};
    private TTSSingleton mttsSingle;

    public String[] IntroVideoFile;
    public String[] WorkoutvideoFile;

    public String[] courseNames;
    public String[] trackerNames;
    public String[] trackerguides;

    public Drawable[] iconActives;
    public Drawable[] iconOuts;

    ConstraintLayout layerStartCount;
    ConstraintLayout layerPause;
    ConstraintLayout layerFail;
    ConstraintLayout layerVideoguide;

    RelativeLayout layerFinish;
    RelativeLayout layerGolfFinish;

    TextView viewfinishcourseTitle;
    TextView[] viewfinishTitles;
    TextView[] viewfinishScores;
    TextView[] viewfinishTotals;

    TextView viewPauseCount;

    TextView viewStartCount;
    TextView viewBpm;
    TextView viewCourse;
    TextView viewRepScore;
    TextView viewProgram;
    TextView viewCourseStep;
    TextView viewFinishCount;
    TextView viewFinishScore;

    ImageView viewpasscheck;
    ImageView viewDim;
    ImageView viewVideoBack;

    ListView listView;
    GuideAdapter guideAdapter;

    Timer dataTimer;

    VideoView mGuideVideoView;
    VideoView mIntroVideoView;

    AppCompatButton buttonPlay;

    private CountDownTimer countDownTimer;
    private CountDownTimer repCountDown;
    private CountDownTimer pauseCountDown;

    CountDownTimer finishCountDown;
    CountDownTimer videoCountDown;

    ConstraintLayout gameui;

    private int videoTime = 10000;
    private int videoTimer = 10;

    private int countTime = 3000;
    private int countTimer = 3;

    private long pauseTime = 30000;
    private int pausecountTimer = 30;

    private long finishTime = 30000;
    private int finishTimer = 31;

    // 선택 프로그램 번호
    private int selectCourseNum = 0;

    // 전체 단계
    private int totalStep = 0;

    // 현재 단계
    private int currentStep = 0;

    // 총 횟수
    private int totalRep = 0;

    // 현재 횟수
    private int currentRep = 0;

    // 개별 성공횟수
    private int[] passStepCount = new int[totalRep];

    // 전체 플레이 시간
    private int totalTime;

    private int currentTrackerNum = 0;

    // XML Parser
    XmlResourceParser[] xrp;

    XmlResourceParser trackerXml;
    XmlResourceParser[] trackerxmls;

    // workouot ui
    ImageView[] workoutIcons;
    TextView[] workoutNames;
    TextView[] workoutScores;



    // Vector Pos
    public Vector3[] UserPos;
    public Vector3[] EntryPos;
    public Vector3[] LastPos;

    public boolean isUser = false;

    public String titleName = "";
    public String videoName = "";

    public String entryDesc = "";

    public boolean entryRefered;
    public int sequenceTimeout = 5000;

    ListView listxml;

    List<Sequence> sequenceList;
    List<CheckMethod> entrypassconditions;
    List<CheckMethod> sequencepassconditions;
    List<CheckMethod> sequencesuccessconditions;
    List<Course> courses;
    List<Workout> workouts;

    ArrayList<Integer> heartbeats = new ArrayList<Integer>();

    private Course course;
    private Workout workout;

    private CheckMethod checkMethod;
    private Sequence sequence;
    private SuccessMethod successMethod;

    Timer UpdateMsgTimer;
    UpdateMsgTimeTask updateMsgTimeTask;

    Timer UpdatePosTimer;
    UpdatePosTimeTask updatePosTimeTask;

    Timer EntryTimer;
    EntryTimeTask entryTimeTask;

    Timer SequenceTimer;
    SequnceTimeTask sequnceTimeTask;

    int totalScore = 0;

    int workoutSize = 0;
    int[] workoutScore;


    int entrySize;
    int sequenceSize;

    int sequencepassSize;
    int sequencesuccessSize;

    int getCountTimer;

    // 엔트리 시퀀스 패스
    boolean[] EntryPass;
    // 운동 시퀀스 패스
    boolean[] SequencePass;

    // 운동 성공 시퀀스
    boolean[] SequenceSuccess;

    // 패스 끝 플레그
    boolean isEndpass;

    // 운동 성공 플래그
    boolean isSuccess = true;

    // 운동 성공 체크 플래그
    boolean isCheck = false;

    boolean isTutorial = false;

    boolean isEntry = false;
    boolean isPlay = false;
    boolean isSize = false;
    boolean isFullScreenPlay = false;

    int msgstep = 0;

    int stepNum = 0;

    float minValue = 0;
    float maxValue = 0;

    float minDistance = 0;
    float maxDistance = 0;

    ImageView msgIcon;

    TextView msgEntry;
    TextView msgFail;
    TextView msgGuide;

    TextView viewMaxValue;
    TextView viewStepNum;
    TextView viewmsgStep;

    TextView viewTimerCount;
    String faildesc;
    String golf_fail_desc;

    // Golf View
    RelativeLayout layoutGolf;
    LinearLayout layoutGolfMsg;
    VideoView golfGuideVideo;
    ImageView golfMap;
    TextView golfDistance, golfDistanceMax, golfDistanceAvg;
    TextView golfSpeedMax,golfSpeedAvg, golfFailMsg;
    ImageView golfDirection;
    ImageView golfDim;
    ImageView golfVideoback;
    TextView golfFinishCount;
    ImageView golfEntryImage;
    RelativeLayout layoutGolfFail;
    AnimationDrawable golfDrawable;
//    AnimationDrawableCallback golfBallDrawable;

    // 골프 운동 실패 시 문구
    private ArrayList<String> golfFailList = new ArrayList<>();
    // 골프 비거리 리스트
    private ArrayList<Integer> distanceList = new ArrayList<>();
    // 골프 스윙 스피드 리스트
    private ArrayList<Integer> speedList = new ArrayList<>();
    private float distanceSum;
    private int distanceMax;
    private int speedMax;
    private double speedSum;
    private int golfMapIndex = 0;
    private MediaPlayer golfSoundPlayer;

    // 골프 맵 리소스
    private int[] golfMapImage = {R.drawable.golf_01, R.drawable.golf_02, R.drawable.golf_03, R.drawable.golf_04, R.drawable.golf_01};
    /**
     * Bluetooth BPM*/
    float totalKal = 0;

    TextView viewKcal;
    ImageView slideBar;
    ImageView pointThumb;

    private static final UUID MY_UUID_INSECURE =  UUID.fromString("8ce255c0-200a-11e0-ac64-0800200c9a66");
    private static final int REQUEST_ENABLE_BT = 2;

    public static final String SEND_BPM = "send_bpm";
    public static final String BPM = "bpm";


    // 코스별 운동 산소 소모 계수 리스트
    /*
    근력운동: 스쿼트 사이드 레이즈 , 사이드 런지, 프론트 런지 로테이션, 와이드 스쿼트 사이드 니업
    자세교정: 와이드 스쿼트, 스쿼트 흉추신전, 스쿼트 로테이션, 런지 사이드 밴드
     */
    public float[][] courseCalorie = { {0.375f, 0.317f, 0.58f, 0.67f},
                                            {0.358f, 0.45f, 0.6f, 1.16f}};

    Time mGolfSwingStartTime;
    Time mGolfSwingEndTime;

    BluetoothAdapter mBluetoothAdapter;
    BluetoothConnectionService mBluetoothConnection;
    BluetoothDevice mBTDevice;

    String bpmValue = "80";                // 심박수
    String bpmValueRest = "72";            // 안정기 심박수
    boolean firstBpmVal = true;           // 처음으로 넘어오는 심박수를 체크하는 flag
    int lastBPMAdjustStep = 1;
    public Time lastBPMCheckedTime;


    Time startTime;
    Time endTime;

    GolfBallView mBallView;
    ImageView ballAnimation;

    /**
     * bpm 데이터 리시버
     */

    /*-- START : Simulator --*/
    private static final String KEY_SIMULATOR = "simulator";
    private static final String KEY_XML_STORING_MODE = "xml_storing_mode_enabled";
    private static final int STORING_MODE_OFF = 0;
    private static final int STORING_MODE_ON = 1;
    private static final int STORING_MODE_ON_WITH_DELAY = 2;

    private int mMotionRecordMode;
    private boolean mIsMotionRecordRunning;
    private List<String[]> mJointList = new ArrayList<String[]>();
    private List<String> mJointTime = new ArrayList<String>();
    private String filename = null;
    private View mView;
    private EditText mEditText;
    private static final int STORING_START_WITH_DELAY = 0;
    private final Handler mTimeoutHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case STORING_START_WITH_DELAY:
                    Toast.makeText(
                            getBaseContext(),
                            "start xml storing",
                            Toast.LENGTH_LONG).show();
                    mJointList.clear();
                    mJointTime.clear();
                    mIsMotionRecordRunning = true;
                    break;
                default:
                    break;
            }
        }
    };
    /*-- END : Simulator --*/

    private boolean isUpdatableHeartbeat() {
        return (layerStartCount.getVisibility() == View.VISIBLE || isFullScreenPlay );
    }

    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String bpm = intent.getStringExtra(BPM);

            Log.d("BPM", "hearRate Receiver bpm: " + bpm);

            String[] splitText = bpm.split(", ");
            String heartRate = splitText[0];
            int state = Integer.valueOf(splitText[1]);

            Log.d("BPM", "hearRate Receiver heartRate: " + heartRate + ", state: " + state);

            switch(state) {
                // 심박 값이 초기 측정이 되지 않은 상태
                case HEARTRATE_STATE_UNIDENTIFIED:
                    break;
                // 심박 값이 신뢰도가 없는 경우 (face detect 되지 않을 경우, fps가 낮은 경우)
                case HEARTRATE_STATE_UNSTABLE:
                    //최초 안정 심박수가 이전에 전달 되었다면 보정 심박수로 나타냄
                    if(!firstBpmVal) {
//                        bpmValue = adjustHeartBeat();
                    }
                    break;
                // 신뢰도 있는 심박수로 사용함
                case HEARTRATE_STATE_STABLE:
                    if(firstBpmVal) {
                        bpmValueRest = heartRate;
                        bpmValue = heartRate;
                        firstBpmVal = false;
                        lastBPMCheckedTime = new Time(System.currentTimeMillis());
                    } else {
                        if(isUpdatableHeartbeat()) {
                            bpmValue = heartRate;
                            lastBPMCheckedTime.setTime(System.currentTimeMillis());
                        }
                    }
                    break;
                default:
                    Log.e("BPM", "undefined state value: " + state);
                    break;
            }

            viewBpm.setText(bpmValue);
            moveBpmPosition(Integer.valueOf(bpmValue));
            Log.d("BPM", "bpmValue : " + bpmValue + " bpmValueRest : " + bpmValueRest);
        }
    };

    String adjustHeartBeat() {
        int adjustHeartRate = Integer.valueOf(bpmValue);
        int totalSet = course.getSet();
        //원래는 set 개념으로 심박수 증가를 보정해야 하지만,
        //전시회는 set 개념이 없어서 심박수에 set 당 심박수 증가분에 step 기준으로 값을 증가시킴
        int currentSet = 0;

        //하나의 step에서는 보정은 한 번만하고 신뢰성 있는 심박값이 전달된 지 10초가 지난 후에 보정함
        if(lastBPMAdjustStep != currentStep && ((System.currentTimeMillis() - lastBPMCheckedTime.getTime() )/1000) > 10) {
            adjustHeartRate += (int)((float)(heartbeats.get(currentSet).intValue()) * (((float)currentStep-(float)lastBPMAdjustStep)/(float)totalStep) );
            lastBPMAdjustStep = currentStep;

        } else {
            // 가장 최근에 신뢰도 있던 심박 값이 현재 스텝이면 보정없이 진행
//            Time lastTime = new Time(System.currentTimeMillis());
//            Log.d(BPM, "adjustHeartBeat currentTime: " + lastTime.toString() + ", lastBPMCheckedTime: " + lastBPMCheckedTime.toString());
        }
        Log.d("BPM", "adjustHeartBeat xml Beat: " + heartbeats.get(currentSet).intValue() + ", currentStep: " + currentStep + ", bpmAdjustStep: " + lastBPMAdjustStep + ", adjustValue: " + adjustHeartRate);

        return String.valueOf(adjustHeartRate);
    }

    public int GOLF_DITANCE_MAX_INDEX  = 10;
    /*
    골프 비거리 계산 테이블:
    {스윙속도, 비거리, 1mph 당 증가 비거리}
    */
    public float[][] mGolfDistance = {
            {80.0f,146.0f, 3.4f},  //80mh
            {85.0f,169.0f, 4.6f}, //85mh
            {90.0f,187.0f, 3.6f}, //90mh
            {95.0f,208.0f, 4.2f}, //95mh
            {100.0f,225.0f, 3.4f}, //100mh
            {105.0f,241.0f, 3.2f}, //105,mh
            {110.0f,256.0f, 3.0f}, //110mh
            {115.0f,269.0f, 2.6f}, //115mh
            {120.0f,281.0f, 2.4f}, //120mh
            {125.0f,292.0f, 2.2f}, //125mh
            {130.0f,302.0f, 2.0f}, //130mh
    };

    /* 스윙 속도로 비거리 계산
    speed 단위 mph, 버거리 단위 yard
     */
    private float getGolfDistance(float speed) {
        float distance = 0;

        if(speed < mGolfDistance[0][0]) {
            distance =  (mGolfDistance[0][1] * speed) / mGolfDistance[0][0];
            Log.d("GOLF", "getGolfDistance1 speed:" + speed + ", distance:" + distance );
        } else if( speed > mGolfDistance[GOLF_DITANCE_MAX_INDEX][0]  ) {
//            타이거 우즈가 최대 비거리 297 yard 임 최대 비거리 302 yard로 제한
//            distance = mGolfDistance[GOLF_DITANCE_MAX_INDEX][1] + ((speed - mGolfDistance[GOLF_DITANCE_MAX_INDEX][0]) * mGolfDistance[GOLF_DITANCE_MAX_INDEX][2]);
            distance = mGolfDistance[GOLF_DITANCE_MAX_INDEX][1];
            Log.d("GOLF", "getGolfDistance2 speed:" + speed + ", distance:" + distance );
        } else {
            int speedIndex = (int) ((speed - 80) / 5);
            distance = mGolfDistance[speedIndex][1] + ((speed - mGolfDistance[speedIndex][0]) * mGolfDistance[speedIndex][2]);
            Log.d("GOLF", "getGolfDistance3 speed:" + speed + ", speedIndex: " + speedIndex + ", distance:" + distance );
        }

        if(distance <= 0) {
            distance = 3.65f;
        }

        return distance;
    }

    /*
    이동거리: (사람팔길이 + 남성용 드라이버 채길이) * 2 * 파이) 가 원호, 원호의 2/5
     ((70cm + 111.76cm) * 2 * 3.14) * (2/5) = 456.58112cm  ==> 0.00283706354887 mile

     스윙 시간이 200ms 인 경우: 200/ (1000 * 60 * 60) = 0.000055555555 시간
     속도: 거리/ 시간 :  0.00283706354887 / 0.00005555555 =51 mph
     스윙 시간이 100ms 인 경우: 100 / (1000 * 60 * 60) = 0.000027777777  시간
     속도: 거리/ 시간 : 0.00283706354887 / 0.000027777777 = 102 mph
     스윙 속도가 50ms 인 경우:  50/ (1000 * 60 * 60 ) = 0.000013888888  시간
     속도: 거리/시간 :  0.00283706354887 / 0.000013888888 = 204 mph

     타이거 우거 스윙 속도 130mph, 비거리: 297 yard
     하수 아마추어 평균 비거리: 182 yard (166m) --> 스윙속도 88 mph
     중급 아마추어 평균 비거리: 199 yard(181m) --> 스윙속도 93mph
     상급 아마추어 평균 비거리: 225 yard (205m) --> 스윙속도 100 mph
     고수 아마추어 평균 비거리: 245 yard (224m) -->  스윙속도 106 mph

     아마추어 전체 평균 : 213 yard (194m) --> 스윙속도 96 mph  --> 스윙시간 106ms

     스윙 시간 86ms ~ 126ms  사이로 튜닝
     스윙 시간이 78ms 인 경우: 78 / (1000 * 60 * 60) = 0.0000216666 시간, 속도 130.9 mph
     스윙 시간이 86ms 인 경우: 86 / (1000 * 60 * 60) = 0.0000238888  시간, 속도 118mph.  비거리: 276.8 yard (253m)
     스윙 속도가 126ms 인 경우:  126/ (1000 * 60 * 60 ) = 0.000035  시간, 81 mph, 비거리: 149.4 yard (136m)s
     속도: 거리/시간 :  0.00283706354887 / 0.000035 = 81 mph
     비거리: 149.4 yard (136m)s
     스윙 시간이 166ms 인 경우: 166 / (1000 * 60 * 60) = 0.0000461111  시간, 속도 61mph.  비거리: 111yard
     스윙 시간이 200ms 인 경우: 200 / (1000 * 60 * 60) = 0.000055555  시간, 속도 51mph.  비거리: 93yard
     스윙 시간이 400ms 인 경우: 200 / (1000 * 60 * 60) = 0.000111111  시간, 속도 25mph.  비거리: 45yard
     스윙 시간이 600ms 인 경우: 200 / (1000 * 60 * 60) = 0.000166666 시간, 속도 17mph.  비거리: 31yard
     */
    private final float MIN_REAL_SWING_TIME = 300.0f;
    private final float MAX_REAL_SWING_TIME = 2400.0f;
    private final float MIN_DATA_SWING_TIME = 78.0f;
    private final float MAX_DATA_SWING_TIME = 600.0f;
    private float getGolfSwingSpeed(long time) {
        if(time <= 0) {
            time = 2400;
        }
        float adjustTime = (float)(time * ((MAX_DATA_SWING_TIME - MIN_DATA_SWING_TIME) / (MAX_REAL_SWING_TIME - MIN_REAL_SWING_TIME)));

        Log.d("GOLF", "getGolfSwingSpeed adjust1 gapTime: " + adjustTime);
        /* golf swing time 보정 필요 */
        if(adjustTime < 78) {
            adjustTime = 78;
        } else if(adjustTime > 3000) {
            adjustTime = 3000;
        }
        Log.d("GOLF", "getGolfSwingSpeed adjust2 gapTime: " + adjustTime);

        float speed = 102132 / (10* adjustTime);
        Log.d("GOLF", "getGolfSwingSpeed speed: " + speed);
        return speed;
    }

    /**
     * 나이 상관계수를 가져온다.
     *
     * @param age
     * @return
     */
    private float getKage(int age) {
        if(age <= 25) {
            return 1.0f;
        } else if(age <= 35) {
            return 0.95f;
        } else if(age <= 50) {
            return 0.85f;
        } else if(age <= 70) {
            return 0.75f;
        } else {
            return 0.6f;
        }
    }

    /**
     * 칼로리 계산식.
     * 운동별 산소 소모계수 * 몸무게 * 심박수(현재)/심박수(안정시) * (17.5/1000 * 나이상관계수) * 성별가중치
     * 성별 가중치 : 남성 1, 여성 0.85
     *
     * @return
     */
    private void calculateKcal(int heartBeat, int hearBeatRest, int step) {
        String gender = getPreferenceString(Constants.PREF_KEY_USER_GENDER, "F");
        int age = Integer.valueOf(getPreferenceString(Constants.PREF_KEY_USER_AGE, "31"));
        int weight = Integer.valueOf(getPreferenceString(Constants.PREF_KEY_USER_WEIGHT, "53"));

        // 성별, 체중, 연령 세팅값에서 가지고 오도록 설정
        float Kage = getKage(age);

        float genderValue;

        if(gender.equals("F")) {
            genderValue = 0.85f;
        }
        else {
            genderValue = 1.0f;
        }

        float kcal =  (float)(courseCalorie[selectCourseNum][step] * ((float)weight * ((float)heartBeat/(float)hearBeatRest)) * (17.5/1000 * Kage) * genderValue);
        totalKal += kcal;

        Log.d("BPM", "calculateKcal: " +  kcal);

        viewKcal.setText(String.format(Locale.getDefault(), "%.2f", totalKal));
    }

    /**
     * 해당 심박수 포지션으로 이동한다.
     *
     * @param bpm
     */
    private void moveBpmPosition(int bpm) {
        pointThumb.setTranslationX(3.4f * (bpm - 50.0f));
    }

    /**
     * 데이터를 가져오는 함수
     * @param key
     * @return
     */
    public String getPreferenceString(String key, String defValue){
        SharedPreferences pref = getSharedPreferences(Constants.PREFERENCE, MODE_PRIVATE);
        return pref.getString(key, defValue);
    }


    /**
     * This method is required for all devices running API23+
     * Android must programmatically check the permissions for bluetooth. Putting the proper permissions
     * in the manifest is not enough.
     *
     * NOTE: This will only execute on versions > LOLLIPOP because it is not needed otherwise.
     */
    private void checkBTPermissions() {
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP){
            int permissionCheck = this.checkSelfPermission("Manifest.permission.ACCESS_FINE_LOCATION");
            permissionCheck += this.checkSelfPermission("Manifest.permission.ACCESS_COARSE_LOCATION");
            if (permissionCheck != 0) {

                this.requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 1001); //Any number
            }
        }else{
            Log.d("Log", "checkBTPermissions: No need to check permissions. SDK version < LOLLIPOP.");
        }
    }

    private void startBT() {
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if(mBluetoothAdapter == null) {
            //Toast.makeText(getApplicationContext(), "블루투스를 지원하지 않는 기기입니다.", Toast.LENGTH_SHORT).show();
            return;
        }

        if(!mBluetoothAdapter.isEnabled()) {
            //Toast.makeText(getApplicationContext(), "블루투스가 꺼져 있습니다. 설정에서 확인해주세요.", Toast.LENGTH_SHORT).show();
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        } else {
            //Toast.makeText(getApplicationContext(), "블루투스를 사용 준비가 되었습니다.", Toast.LENGTH_SHORT).show();
            querypaired();
        }
    }

    private void querypaired() {
        Set<BluetoothDevice> paredDevices = mBluetoothAdapter.getBondedDevices();
        if(paredDevices.size() > 0) {
            final BluetoothDevice blueDev[] = new BluetoothDevice[paredDevices.size()];
            String item;
            int i = 0;

            for(BluetoothDevice devicel : paredDevices) {
                blueDev[i] = devicel;
                item = blueDev[i].getName() + " : " + blueDev[i].getAddress();
                Log.d("Log", "Device : " + item);
                i++;
            }

            //Toast.makeText(getApplicationContext(), "페어링된 디바이스 : " + blueDev[0].getName(), Toast.LENGTH_SHORT).show();

            mBTDevice = blueDev[0];
            mBluetoothConnection = new BluetoothConnectionService(UnityPlayerActivity.this);
            mBluetoothConnection.startClient(mBTDevice, MY_UUID_INSECURE);
        } else {
            //Toast.makeText(getApplicationContext(), "현재 페어링 된 디바이스가 없습니다.", Toast.LENGTH_SHORT).show();
        }
    }

    // Setup activity layout
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        super.onCreate(savedInstanceState);

        /*-- START : Simulator --*/
        mContext = this;
        SharedPreferences pref = getSharedPreferences(KEY_SIMULATOR, Context.MODE_PRIVATE);
        mMotionRecordMode = pref.getInt(KEY_XML_STORING_MODE, 0);
        mIsMotionRecordRunning = false;
        /*-- END : Simulator --*/

        setContentView(R.layout.activity_track);

        listxml = findViewById(R.id.listxml);

        msgIcon = findViewById(R.id.icon_entry);
        msgEntry = findViewById(R.id.msg_entry);

        msgGuide = findViewById(R.id.msg_guide);

        msgFail = findViewById(R.id.msg_fail);

        gameui = (ConstraintLayout) findViewById(R.id.constraintLayout);

        layerVideoguide = (ConstraintLayout)findViewById(R.id.layer_videoguide);

        layerStartCount = (ConstraintLayout) findViewById(R.id.layer_starttimer);
        viewStartCount = (TextView) layerStartCount.findViewById(R.id.val_timer);

        viewPauseCount = (TextView) findViewById(R.id.val_pausetimer);
        layerPause = (ConstraintLayout)findViewById(R.id.layer_pause);

        layerFinish = (RelativeLayout)findViewById(R.id.layoutTrackFinish);
        viewFinishCount = findViewById(R.id.finishCount);

        layerFail = (ConstraintLayout)findViewById(R.id.layer_fail);

        viewfinishcourseTitle = findViewById(R.id.finish_coursetitle);

        viewfinishTitles = new TextView[5];
        viewfinishTotals = new TextView[5];
        viewfinishScores = new TextView[5];

        viewfinishTitles[0] = findViewById(R.id.finish_title_01);
        viewfinishTitles[1] = findViewById(R.id.finish_title_02);
        viewfinishTitles[2] = findViewById(R.id.finish_title_03);
        viewfinishTitles[3] = findViewById(R.id.finish_title_04);
        viewfinishTitles[4] = findViewById(R.id.finish_title_05);

        viewfinishTotals[0] = findViewById(R.id.finish_total_01);
        viewfinishTotals[1] = findViewById(R.id.finish_total_02);
        viewfinishTotals[2] = findViewById(R.id.finish_total_03);
        viewfinishTotals[3] = findViewById(R.id.finish_total_04);
        viewfinishTotals[4] = findViewById(R.id.finish_total_05);

        viewfinishScores[0] = findViewById(R.id.finish_score_01);
        viewfinishScores[1] = findViewById(R.id.finish_score_02);
        viewfinishScores[2] = findViewById(R.id.finish_score_03);
        viewfinishScores[3] = findViewById(R.id.finish_score_04);
        viewfinishScores[4] = findViewById(R.id.finish_score_05);


        viewDim = findViewById(R.id.view_dim);
        viewVideoBack = findViewById(R.id.view_videoback);

        viewBpm = findViewById(R.id.val_bpm);
        viewProgram = findViewById(R.id.val_program);
        viewCourse = findViewById(R.id.val_course);
        viewKcal = findViewById(R.id.val_kcal);
        viewFinishScore = findViewById(R.id.finish_score);

        viewRepScore = findViewById(R.id.val_repscore);
        viewCourseStep = findViewById(R.id.val_coursestep);

        viewMaxValue = findViewById(R.id.msg_maxvalue);
        viewStepNum = findViewById(R.id.msg_stpeNum);
        viewmsgStep = findViewById(R.id.msg_msgStep);
        viewpasscheck = findViewById(R.id.view_passcheck);

        viewTimerCount = findViewById(R.id.view_timercount);

        listView = findViewById(R.id.list_view);


        mGuideVideoView = findViewById(R.id.videoView);
        mIntroVideoView = findViewById(R.id.introVideoView);
        mIntroVideoView.setVisibility(View.VISIBLE);

        // Golf Layout view find
        layoutGolf = findViewById(R.id.layoutGolf);                 // 골프 운동 layout, 기본은 gone처리 => 골프 운동으로 진입 시 visible로 변경
        golfGuideVideo = findViewById(R.id.golfGuideVideo);         // 골프 가이드 비디오 영상
        golfMap = findViewById(R.id.imageViewGolfMap);              // 골프 지도 이미지 적용, golf_01, golf_02, golf_03, golf_04
        golfDistance = findViewById(R.id.textViewDistance);         // 비거리 적용 ex) 100.98 m
        golfDirection = findViewById(R.id.imageViewDirection);      // 방향 이미지 적용, icon_direction_1, icon_direction_2, icon_direction_3
        golfEntryImage = findViewById(R.id.golfEntryImage);         // 엔트리 이미지 적용
        golfDistanceMax = findViewById(R.id.golfDistanceMax);       // 최대 비거리
        golfDistanceAvg = findViewById(R.id.golfDistanceAvg);       // 평균 비거리
        golfSpeedMax = findViewById(R.id.speedMax);                 // 최대 스윙 스피드
        golfSpeedAvg = findViewById(R.id.speedAvg);                 // 평균 스윙 스피드
        layoutGolfMsg = findViewById(R.id.golfResultMsg);           // 골프 결과 자세 교정 메시지
        golfDim = findViewById(R.id.golfDim);
        golfVideoback = findViewById(R.id.golfVideoback);
        layerGolfFinish = findViewById(R.id.layoutGolfFinish);
        golfFinishCount = findViewById(R.id.golfFinishCount);
        layoutGolfFail = findViewById(R.id.golfFailLayer);
        golfFailMsg = findViewById(R.id.golfFailMsg);
        mBallView = findViewById(R.id.golfBallView);
        ballAnimation = findViewById(R.id.ballAnimation);

        mGuideVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                Log.d("Log", "Video play complete.");
                mGuideVideoView.seekTo(0);   // looping
                mGuideVideoView.start();
                Log.d("Log", "Video play start.");
            }
        });

        mIntroVideoView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if(mIntroVideoView.isPlaying()) {

                    mIntroVideoView.stopPlayback();

                    mIntroVideoView.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            mIntroVideoView.setVisibility(View.GONE);

                            Log.d("workout Video ", WorkoutvideoFile[currentStep]);

                            mGuideVideoView.setVideoPath(WorkoutvideoFile[currentStep]);
                            mGuideVideoView.requestFocus();

                            mGuideVideoView.start();

                            videoSizeUp(mGuideVideoView);

                            Video_CountDown();

//                            mGuideVideoView.postDelayed(new Runnable() {
//                                @Override
//                                public void run() {
//                                    mGuideVideoView.pause();
//                                }
//                            }, 100);

                            UI_Init();

                            //pause용 플래그 (true -- 일시정지 사용 , false -- 사용안함)
                            isPlay = false;

                            // start 시작 영상 10초 후로 변경
//                            countTimer = 3;
//                            StartTimer(countTime);


                        }

                    }, 100);
                }
                return false;
            }
        });

        mIntroVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {

                mIntroVideoView.setVisibility(View.GONE);

                Log.d("workout Video ", WorkoutvideoFile[currentStep]);

                mGuideVideoView.setVideoPath(WorkoutvideoFile[currentStep]);
                mGuideVideoView.requestFocus();

                mGuideVideoView.start();

                videoSizeUp(mGuideVideoView);

                Video_CountDown();
/*
                mGuideVideoView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mGuideVideoView.pause();
                    }
                }, 100);
*/
                UI_Init();

                //pause용 플래그 (true -- 일시정지 사용 , false -- 사용안함)
                isPlay = false;

                // start 시작 영상 10초 후로 변경
//                            countTimer = 3;
//                            StartTimer(countTime);


            }
        });

        buttonPlay = findViewById(R.id.buttonPlay);
        buttonPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("Load", "LOAD Video Test");

                if(isSize == false) {

                    isSize = true;
                    videoSizeUp(mGuideVideoView);

                } else
                {
                    isSize = false;
                    videoSizeDown(mGuideVideoView);
                }


                //change_sequence();

                //Tracker_Reset();


                /*
                if (mGuideVideoView.isPlaying()) {
                    mGuideVideoView.stopPlayback();
                    buttonPlay.setText("Play");
                } else {
                    buttonPlay.setText("Stop");
                    playVideo(mGuideVideoView, WorkoutvideoFile[0]);
                }*/
            }
        });

        slideBar = findViewById(R.id.slideBar);
        pointThumb = findViewById(R.id.thumb);

        slideBar.setBackgroundResource(R.drawable.hr_bar_01);

        mUnityPlayer = new UnityPlayer(this);

        //setContentView(mUnityPlayer);
        // mUnityPlayer.requestFocus();

        FrameLayout layout = (FrameLayout) findViewById(R.id.frameLayout1);

        layout.addView(mUnityPlayer.getView());

        mUnityPlayer.requestFocus();

        /// 운동 variable
        UserPos = new Vector3[JointName.length];
        for (int i = 0; i < UserPos.length; i++)
        {
            UserPos[i] = new Vector3();
        }
        // guide 실행 모드 true false
        // Set_Mode("true");


        // Init
        Init();

        checkBTPermissions();
        //StartTimer();

        // BLUETOOTH INIT
        IntentFilter filter = new IntentFilter();
        filter.addAction(SEND_BPM);
        registerReceiver(mBroadcastReceiver, filter);

        startBT();

    }

    public void Init(){

        Log.d("Game Init", "Load Init");

        courses = new ArrayList<Course>();

        xrp = new XmlResourceParser[6];

        xrp[0] = getResources().getXml(R.xml.course_muscle_leg_demo);
        xrp[1] = getResources().getXml(R.xml.course_balance_stretching_demo);

        xrp[2] = getResources().getXml(R.xml.course_diet_fat_burn_full);
        xrp[3] = getResources().getXml(R.xml.course_diet_fat_burn_demo);

        xrp[4] = getResources().getXml(R.xml.course_balance_stretching_full);
        xrp[5] = getResources().getXml(R.xml.course_golf_swing_demo);

        trackerxmls = new XmlResourceParser[workoutSize];

        for (int i = 0; i < xrp.length; i++)
        {
            readCourse(xrp[i]);
        }

        // Intro video Init
        IntroVideoFile = new String[courses.size()];

        for (int i = 0; i < courses.size(); i++) {

            IntroVideoFile[i] = RawPath + courses.get(i).getVideo()+".mp4";

            Log.d("IntroVideoFile video", IntroVideoFile[i]);
        }

        EntryPos = new Vector3[JointName.length];
        LastPos = new Vector3[JointName.length];

        // Update Pos Timer Init
        UpdatePosTimer = new Timer();
        updatePosTimeTask = new UpdatePosTimeTask();
        UpdatePosTimer.schedule(updatePosTimeTask, 0, 50);

        mttsSingle = TTSSingleton.getInstance(getApplicationContext());
    }

    public void Load_Course(final int selectnum) {

        selectCourseNum = selectnum;

        Log.d("Load_Course", "num : " + selectCourseNum);

        workoutSize = courses.get(selectCourseNum).workoutList.size();

        Log.d("Load_Course", "workoutSize : " + workoutSize);

        trackerNames = new String[workoutSize];

        // workouts Video Init
        WorkoutvideoFile = new String[workoutSize];

        if(selectnum == LOAD_GOLF) {
            loadGolf();
        } else {
            loadFitness(selectnum);
        }


//        UI_Init();
//        StartTimer();
    }

    public void Tracker_Init() {

        entrypassconditions = new ArrayList<CheckMethod>();
        sequenceList = new ArrayList<Sequence>();

        //int trackerID = getResources().getIdentifier(trackerNames[currentStep], "xml", getPackageName());

        //Log.d("Tracker_Init", "trackerNames_" + currentStep + " : " + trackerNames[currentStep] + ", ID = "+ trackerID);

        //trackerXml = getResources().getXml(trackerID);

        readEntry(trackerNames[currentStep]);
        readSequence(trackerNames[currentStep]);

        if(guideAdapter != null)
        {
            guideAdapter.notifyDataSetChanged();
        }

        guideAdapter = new GuideAdapter();

        if(selectCourseNum != LOAD_GOLF) {
            listView.setAdapter(guideAdapter);
            // guide Init
            msgEntry.setText(entryDesc);
        }

        // Entry Init
        entrySize = entrypassconditions.size();
        sequenceSize = sequenceList.size();

        trackerguides = new String[sequenceSize];

        Log.d("Courses", "trackerguides length = " + trackerguides.length);

        for (int i = 0; i < sequenceSize; i++)
        {
            trackerguides[i] = sequenceList.get(i).getDescription();

            Log.d("Courses", "tracker guide_" + i + " desc : " + trackerguides[i]);
        }


        Init_Msg();
        //sequencepassSize = sequenceSize;
        //sequencesuccessSize = sequenceSize;

        EntryPass = new boolean[entrySize];

        // Rep Init
        totalStep = courses.get(selectCourseNum).getStep();
        totalRep = courses.get(selectCourseNum).workoutList.get(currentStep).getRepeat();

        //Log.d("Courses", "workouts Size = " + workouts.size());
        Log.d("Courses", "totalStep = " + totalStep);
        Log.d("Courses", "totalRep = " + totalRep);

        if(selectCourseNum != LOAD_GOLF) {
            // Course Init
            viewCourse.setText(courses.get(selectCourseNum).workoutList.get(currentStep).getTitle());
            viewProgram.setText(courses.get(selectCourseNum).getTitle());
            viewRepScore.setText(RepToString(totalRep, currentRep));
            viewCourseStep.setText(StepToString(totalStep, (currentStep + 1)));

            Update_Workout();
        }

        /*
        for (int i = 0; i < courses.size(); i++) {
            String val = "";

            if (courses.get(i).getRestdesc() != null) {
                val = courses.get(i).getRestdesc();
            }
            Log.d("tracker name", val);
        }*/

    }

    public void Tracker_Reset()
    {
        Tracker_Init();

        /*
        stepNum += 1;

        if(stepNum == sequenceSize)
        {
            stepNum = 0;

            currentStep += 1;

            if(currentStep == 4)
            {
                currentStep = 0;
            }

            Tracker_Init();
        }

        guideAdapter.index = stepNum;

        guideAdapter.notifyDataSetChanged();

        Log.d("TRACKER RESET", "stepNum = " + stepNum + ", sequenceSize = " + sequenceSize);

        if(stepNum == 3  && sequenceSize <= 5)
        {
            listView.setSelection(3);
        }
        else if(stepNum == 3 && sequenceSize == 6)
        {
            listView.setSelection(3);
        }
        else if(stepNum == 0)
        {
            listView.setSelection(0);
        }
        */


        //Tracker_Init();
    }

    private void loadGolf() {
        // 스윙 타임 임의 초기화
        mGolfSwingStartTime = new Time(System.currentTimeMillis());
        mGolfSwingEndTime = new Time(System.currentTimeMillis());

        for (int i = 0; i < workoutSize; i++) {
            WorkoutvideoFile[i] = RawPath + courses.get(selectCourseNum).workoutList.get(i).getVideo() + ".mp4";
            Log.d("workout video", WorkoutvideoFile[i]);
            trackerNames[i] = courses.get(selectCourseNum).workoutList.get(i).getTracker();
            Log.d("workout tracker", "tracker Name =" + trackerNames[i]);
        }

        Log.d("workout Video ", WorkoutvideoFile[currentStep]);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Tracker_Init();

                golfGuideVideo.setVideoPath(WorkoutvideoFile[currentStep]);
                golfGuideVideo.requestFocus();

                golfGuideVideo.start();

                videoSizeUp(golfGuideVideo);
                Golf_UI_Init();

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        videoSizeDown(golfGuideVideo);

                        stepNum = 0;
                        msgstep = 0;

                        // Entry Start
                        start_entry();
                    }
                }, 10000);
            }
        });
    }

    private void loadFitness(int selectnum) {
        workoutScore = new int[workoutSize];

        workoutIcons = new ImageView[4];
        workoutIcons[0] = findViewById(R.id.msg_icon_01);
        workoutIcons[1] = findViewById(R.id.msg_icon_02);
        workoutIcons[2] = findViewById(R.id.msg_icon_03);
        workoutIcons[3] = findViewById(R.id.msg_icon_04);

        workoutNames = new TextView[4];
        workoutNames[0] = findViewById(R.id.msg_course_01);
        workoutNames[1] = findViewById(R.id.msg_course_02);
        workoutNames[2] = findViewById(R.id.msg_course_03);
        workoutNames[3] = findViewById(R.id.msg_course_04);

        workoutScores = new TextView[4];
        workoutScores[0] = findViewById(R.id.msg_count_01);
        workoutScores[1] = findViewById(R.id.msg_count_02);
        workoutScores[2] = findViewById(R.id.msg_count_03);
        workoutScores[3] = findViewById(R.id.msg_count_04);


        iconActives = new Drawable[workoutSize];
        iconOuts = new Drawable[workoutSize];

        for (int i = 0; i < workoutSize; i++) {

            WorkoutvideoFile[i] = RawPath + courses.get(selectCourseNum).workoutList.get(i).getVideo() + ".mp4";

            Log.d("workout video", WorkoutvideoFile[i]);

            trackerNames[i] = courses.get(selectCourseNum).workoutList.get(i).getTracker();

            Log.d("workout tracker", "tracker Name =" + trackerNames[i]);


        }

        /*
        for(int i =0 ; i< workoutSize; i++)
        {
            Log.d("workout tracker", "trackerNames_" + i + " : " + trackerNames[i] + ", ID = "+ getApplicationContext().getResources().getIdentifier(trackerNames[i], "xml", getApplicationContext().getPackageName()));

        }*/

        // trackerXml = getResources().getXml(trackerID);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                // Finish Init
                viewfinishcourseTitle.setText(courses.get(selectnum).getTitle());

                for (int i = 0; i < workoutSize; i++)
                {
                    viewfinishTitles[i].setText(courses.get(selectnum).workoutList.get(i).getTitle());
                    viewfinishTotals[i].setText("/" + courses.get(selectnum).workoutList.get(i).getRepeat());
                    viewfinishScores[i].setText("" + workoutScore[i]);

                    // set workout icon
                    int iconNum  = 0;

                    iconNum = getResources().getIdentifier(courses.get(selectCourseNum).workoutList.get(i).getActionicon_active(), "drawable", getPackageName());

                    iconActives[i] = getResources().getDrawable(iconNum);

                    iconNum = getResources().getIdentifier(courses.get(selectCourseNum).workoutList.get(i).getActionicon_out(), "drawable", getPackageName());

                    iconOuts[i] = getResources().getDrawable(iconNum);

                    workoutIcons[i].setImageDrawable(iconOuts[i]);
                    workoutIcons[i].setVisibility(View.VISIBLE);

                    workoutNames[i].setText(courses.get(selectnum).workoutList.get(i).getTitle());
                    workoutNames[i].setVisibility(View.VISIBLE);

                    workoutScores[i].setText(workoutScore[i] + " / " + courses.get(selectnum).workoutList.get(i).getRepeat());
                    workoutScores[i].setVisibility(View.VISIBLE);
                }

                Tracker_Init();

                playVideo(mIntroVideoView, IntroVideoFile[selectCourseNum]);
                //startBT();

            }
        });
    }

    private int calculateScore() {
        /* 점수는 시간 30%, 정확도 70% 조합으로 표시 */
        int timeScore = 30;
        int successScore = 0;
        endTime = new Time(System.currentTimeMillis());
        // 근력강화 운동 3번 반복시 13s, 8s, (15s), 18s, 15s  == 54s
        // 자세교정 운동 2번 반복시 7s,14s, 16s, 27s == 64s
        long timeGap = (endTime.getTime() - startTime.getTime())/1000;
        long bestTime = courses.get(selectCourseNum).getBestTime();

        for(int i = 0; i < workoutSize; i++) {
            //각 운동 간 가이드 비디오 플레이 시간 + Timer 시간 3초
            bestTime += (int)(courses.get(selectCourseNum).workoutList.get(i).getGuideTime()) + 3;
            Log.d("Score", "calculateScore " + i + ", th video added: " + bestTime);
        }

        if(bestTime >= timeGap) {
            // time penalty 0
        } else {
            int penaltyCount = (int)((timeGap - bestTime) / 10);
            // 10s 마다 -10%
            timeScore -= (penaltyCount * 3);
            if(timeScore <= 0 ) {
                timeScore = 0;
            }
            Log.d("Score", "calculateScore timeGap: " + timeGap + ", bestTime: " + bestTime + ", penaltyCount: " + penaltyCount);
        }

        int totalSuccessScore  = 0;
        for(int i=0; i < workoutSize; i++) {
            totalSuccessScore += workoutScore[i];
        }
        Log.d("Score", "calculateScore total success: " + totalSuccessScore);
        // 총 운동갯수 백분위 점수의 70%
        successScore = (int)((((float)(totalSuccessScore) / 12.0f/*총 운동횟수*/) * 100.0f) * 0.7);
        Log.d("Score", "time: " + timeScore + ", success Score: " + successScore);
        return successScore + timeScore;
    }

    public void Load_Finish()
    {
        int score = calculateScore();
        //  score view에 setText 로 score 표시

        viewFinishScore.setText("" + score);

        for(int i =0; i < workoutSize; i++) {

            viewfinishScores[i].setText("" + workoutScore[i]);
        }

        layerFinish.setVisibility(View.VISIBLE);
        layerFinish.bringToFront();
        Finish_Countdown();
    }

    private void finishGolf() {
        calculateMaxAvgDistance();
        calculateMaxAvgSpeed();

//        golfFailList.add("다운스윙:허리가 가슴보다 먼저 나가야 합니다.");
//        golfFailList.add("피니쉬:피니쉬에 몸을 충분히 틀어주세요.");
//        golfFailList.add("다운스윙:허리가 가슴보다 먼저 나가야 합니다.");

        layoutGolfMsg.removeAllViews();
        if(golfFailList.size() == 0 ) {
            // Pass
            golfFailList.add(getString(R.string.golf_pass_desc));
            for(int i = 0 ; i < golfFailList.size() ; i++) {
                layoutGolfMsg.addView(new GolfFinishMessage(this, golfFailList.get(i)));
            }
        } else {
            for(int i = 0 ; i < golfFailList.size() ; i++) {
                layoutGolfMsg.addView(new GolfFinishMessage(this, golfFailList.get(i)));
            }
        }

        layerGolfFinish.setVisibility(View.VISIBLE);
        layerGolfFinish.bringToFront();
        Finish_Countdown();

        golfMapIndex = 0;

        golfSoundPlayer.release();
    }

    public void Finish_Countdown()
    {
        Log.d("Countdown", "Countdown START");

        finishCountDown = new CountDownTimer(finishTime, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

                Log.d("finishTimer", "" + finishTimer);

                finishTimer--;

                if(selectCourseNum != LOAD_GOLF) {
                    viewFinishCount.setText("" + finishTimer);
                } else {
                    golfFinishCount.setText("" + finishTimer);
                }
            }

            @Override
            public void onFinish() {

                Log.d("Countdown", "Countdown Finish");

                // 처음으로

                // 가이드시작
                //UnityPlayer.UnitySendMessage("GM", "RestartAppForAOS", "0");

                // 메뉴 시작
//                UnityPlayer.UnitySendMessage("GM", "RestartAppForAOS", "1");

                if(selectCourseNum != LOAD_GOLF) {
                    layerFinish.setVisibility(View.GONE);
                    gameui.setVisibility(View.GONE);
                    mIntroVideoView.setVisibility(View.GONE);
                    listxml.setVisibility(View.GONE);
                } else {
                    layerGolfFinish.setVisibility(View.GONE);
                    layoutGolf.setVisibility(View.GONE);
                    ballAnimation.setVisibility(View.GONE);
                    mBallView.setVisibility(View.GONE);
                }
            }

        }.start();
    }

    public String StepToString(int _totalStep, int _currentStep) {
        return "현재 " + _totalStep + "단계 중 " + _currentStep + "단계";
    }

    public String RepToString(int _totalRep, int _currentRep) {
        return _currentRep + " / " + _totalRep + " 회 반복";
    }


    private void playVideo(VideoView view, String videoFile) {

        Log.d("Log", "Video file : " + videoFile);

        // 인트로 비디오 플레이
        try {

            //view.setVideoURI(videoFile);
            view.setVideoPath(videoFile);
            view.requestFocus();

            view.start();
        } catch (Exception e) {
            Log.d("Log", "Video Failed : " + e.toString());
        }
    }

    private void playGuideVideo(VideoView view, String videoFile)
    {
        // 가이드 비디오 플레이 테스트

        try {
            //view.setVideoURI(videoFile);
            view.setVideoPath(videoFile);
            view.requestFocus();

            view.start();

            /*
            view.postDelayed(new Runnable() {
                @Override
                public void run() {
                    view.pause();
                }
            }, 100);*/

        } catch (Exception e) {
            Log.d("Log", "Video Failed : " + e.toString());
        }
    }


    public void Video_CountDown()
    {
        int playTime =  courses.get(selectCourseNum).workoutList.get(currentStep).getGuideTime() * 1000;
        Log.d("Countdown", "Video Countdown START: " + playTime);
        isFullScreenPlay = true;

        videoCountDown = new CountDownTimer(playTime, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

                Log.d("videoTimer", "" + videoTimer);

                videoTimer--;

                viewTimerCount.setText("" + videoTimer);
            }

            @Override
            public void onFinish() {

                Log.d("Countdown", "Video Countdown Finish");

                // 영상 10초 끝 운동 시작
                videoSizeDown(mGuideVideoView);

//                layerStartCount.setVisibility(View.VISIBLE);
//                layerStartCount.bringToFront();

                msgEntry.setVisibility(View.VISIBLE);
                msgIcon.setVisibility(View.VISIBLE);

                stepNum = 0;
                msgstep = 0;

                isEndpass = false;
                isSuccess = true;
                isCheck = true;
                isFullScreenPlay = false;

//                운동 간 Timer를 별도로 할 경우 길게는 3~8초까지 카운트가 달라져서 통일되지 못해서 더 혼란스러움 count는 3초로 통일
//                countTimer = (courses.get(selectCourseNum).workoutList.get(currentStep).getGuideTime()) / 2 ;
//                countTime = (courses.get(selectCourseNum).workoutList.get(currentStep).getGuideTime() * 1000) / 2;
                countTimer = 3;
                StartTimer(countTime);

            }

        }.start();
    }

    void videoSizeUp(final View view)
    {
        PropertyValuesHolder[] holders = new PropertyValuesHolder[4];
        holders[0] = PropertyValuesHolder.ofInt("dx", 32, 200);
        holders[1] = PropertyValuesHolder.ofInt("dy", 32, 0);
        holders[2] = PropertyValuesHolder.ofInt("dw", 824, 2560);
        holders[3] = PropertyValuesHolder.ofInt("dh", 464, 1440);


        new Handler().postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                layerVideoguide.setVisibility(View.VISIBLE);
                viewVideoBack.setVisibility(View.VISIBLE);
                viewDim.setVisibility(View.GONE);
                golfDim.setVisibility(View.GONE);
                golfVideoback.setVisibility(View.VISIBLE);
            }
        }, 1000);

        ValueAnimator animator = ValueAnimator.ofPropertyValuesHolder(holders);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int dx = (int) valueAnimator.getAnimatedValue("dx");
                int dy = (int) valueAnimator.getAnimatedValue("dy");
                int dw = (int) valueAnimator.getAnimatedValue("dw");
                int dh = (int) valueAnimator.getAnimatedValue("dh");

                if(selectCourseNum != LOAD_GOLF) {
                    ConstraintLayout.LayoutParams layoutParams =(ConstraintLayout.LayoutParams)view.getLayoutParams();

                    layoutParams.width = dw;
                    layoutParams.height = dh;

                    layoutParams.leftMargin = dx;
                    layoutParams.bottomMargin = dy;

                    view.setLayoutParams(layoutParams);
                } else {
                    RelativeLayout.LayoutParams layoutParams =(RelativeLayout.LayoutParams)view.getLayoutParams();

                    layoutParams.width = dw;
                    layoutParams.height = dh;

                    layoutParams.leftMargin = dx;
                    layoutParams.bottomMargin = dy;

                    view.setLayoutParams(layoutParams);
                }
            }
        });

        animator.setDuration(1000);
        animator.start();

    }

    void videoSizeDown(final View view)
    {
        layerVideoguide.setVisibility(View.GONE);
        viewDim.setVisibility(View.VISIBLE);
        golfDim.setVisibility(View.VISIBLE);
        viewVideoBack.setVisibility(View.GONE);
        golfVideoback.setVisibility(View.GONE);

        PropertyValuesHolder[] holders = new PropertyValuesHolder[4];
        holders[0] = PropertyValuesHolder.ofInt("dx", 200, 32);
        holders[1] = PropertyValuesHolder.ofInt("dy", 0, 32);
        holders[2] = PropertyValuesHolder.ofInt("dw", 2560, 824);
        holders[3] = PropertyValuesHolder.ofInt("dh", 1440, 464);

        ValueAnimator animator = ValueAnimator.ofPropertyValuesHolder(holders);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int dx = (int) valueAnimator.getAnimatedValue("dx");
                int dy = (int) valueAnimator.getAnimatedValue("dy");
                int dw = (int) valueAnimator.getAnimatedValue("dw");
                int dh = (int) valueAnimator.getAnimatedValue("dh");

                if(selectCourseNum != LOAD_GOLF) {
                    ConstraintLayout.LayoutParams layoutParams =(ConstraintLayout.LayoutParams)view.getLayoutParams();

                    layoutParams.width = dw;
                    layoutParams.height = dh;

                    layoutParams.leftMargin = dx;
                    layoutParams.bottomMargin = dy;

                    view.setLayoutParams(layoutParams);
                } else {
                    RelativeLayout.LayoutParams layoutParams =(RelativeLayout.LayoutParams)view.getLayoutParams();

                    layoutParams.width = dw;
                    layoutParams.height = dh;

                    layoutParams.leftMargin = dx;
                    layoutParams.bottomMargin = dy;

                    view.setLayoutParams(layoutParams);
                }

            }
        });
        animator.setDuration(1000);
        animator.start();
    }


    void start_entry() {

        // Entry Start
        EntryTimer = new Timer();
        entryTimeTask = new EntryTimeTask();

        EntryTimer.schedule(entryTimeTask, 0, 100);

        isEntry = true;

        Reset_Msg();

        /*
        UpdateMsgTimer = new Timer();
        updateMsgTimeTask = new UpdateMsgTimeTask();
        UpdateMsgTimer.schedule(updateMsgTimeTask, 0, 1000);
        */

        if(selectCourseNum != LOAD_GOLF) {
            msgIcon.setVisibility(View.VISIBLE);
            msgEntry.setVisibility(View.VISIBLE);
        }
    }

    void start_sequence()
    {
        Log.d("SequenceTimer", "START");

        SequenceTimer = new Timer();

        sequnceTimeTask = new SequnceTimeTask();

        SequenceTimer.schedule(sequnceTimeTask, 0, 50);

        maxValue = 0;
        minValue = 10;

        maxDistance = 0;
        minDistance = 1000;

        On_Msg();

        /*
        if(isTutorial)
        {
            // 5초 타이머 진행
            counter_sequence();
        }
        else
        {
            // 자동 진행
        }*/
    }

//    void startGolfBallAnimation() {
//        golfBallDrawable = new AnimationDrawableCallback((AnimationDrawable) ballAnimation.getBackground()) {
//            /**
//             * Called when the animation finishes.
//             */
//            @Override
//            public void onAnimationFinish() {
//                mBallView.setUserPos(EntryPos);
//                mBallView.setVisibility(View.VISIBLE);
//                ballAnimation.setVisibility(View.GONE);
//            }
//
//            /**
//             * Called when the animation starts.
//             */
//            @Override
//            public void onAnimationStart() {
//                mBallView.setVisibility(View.GONE);
//                ballAnimation.setVisibility(View.VISIBLE);
//            }
//        };
//
//        ballAnimation.setBackground(golfBallDrawable);
//        golfBallDrawable.start();
//    }

    void startGolfBallAnimation() {
        Log.d("GOLF", "startGolfBallAnimation");
        mBallView.setVisibility(View.GONE);
        ballAnimation.setVisibility(View.VISIBLE);

        golfDrawable = (AnimationDrawable) ballAnimation.getBackground();
        golfDrawable.start();
    }

    void stopGolfBallAnimation() {
        Log.d("GOLF", "stopGolfBallAnimation");
        mBallView.setUserPos(EntryPos);
        mBallView.setVisibility(View.VISIBLE);
        ballAnimation.setVisibility(View.GONE);

        if(golfDrawable.isRunning()) {
            golfDrawable.stop();
        }
    }

    void playGolfSound() {
        golfSoundPlayer = MediaPlayer.create(this, R.raw.golf_swing);
        golfSoundPlayer.start();
    }

    void updateGolfSwingData() {
        long golfSwingTime = mGolfSwingEndTime.getTime() - mGolfSwingStartTime.getTime();
        Log.d("GOLF", "===============" + currentRep + "========================");
        Log.d("GOLF", "updateGolfSwingData gapTime: " + golfSwingTime);

        float swingSpeed = getGolfSwingSpeed(golfSwingTime);
        float flyDistance = getGolfDistance(swingSpeed);
        speedList.add((int)(swingSpeed));
        distanceList.add((int)flyDistance);
        golfDistance.setText((int)(flyDistance) + " yard");
    }

    private void calculateMaxAvgDistance() {
        distanceMax = distanceList.get(0);
        for(int i=0; i < distanceList.size() ; i++) {
            if(distanceMax < distanceList.get(i)) {
                distanceMax = distanceList.get(i);
            }

            distanceSum += distanceList.get(i);
        }
        golfDistanceMax.setText(distanceMax + " yard");
        golfDistanceAvg.setText((int)(distanceSum / distanceList.size()) + " yard");
    }

    private void calculateMaxAvgSpeed() {
        speedMax = speedList.get(0);
        for(int i=0; i < speedList.size() ; i++) {
            if(speedMax < speedList.get(i)) {
                speedMax = speedList.get(i);
            }

            speedSum += speedList.get(i);
        }

        double avg = (speedSum / speedList.size());
        String strAvg = String.format(Locale.getDefault(), "%.2f", avg);
        //String strSpeedMax = String.format(Locale.getDefault(), "%.2f", speedMax);

        golfSpeedMax.setText(speedMax + " mph");
        golfSpeedAvg.setText(strAvg + " mph");
    }


    void change_sequence(boolean isSkip)
    {
        /*
        if(stepNum  == 1)
        {

        }
        else
        {
            stepNum += 1;
            msgstep += 1;
        }*/

        if(isSkip == true)
        {
            isSuccess = false;
        }

        stepNum += 1;
        msgstep += 1;

        Log.d("change_sequence", "sequenceSize = " + sequenceSize + ", stepNum = " + stepNum);

        if(stepNum == sequenceSize)
        {
            stepNum = 0;
            msgstep = 0;

            // 운동 성공
            // 시퀀스 타이머 종료
            if(repCountDown != null)
            {
                repCountDown.cancel();
            }

            isEndpass = true;

            if(selectCourseNum != LOAD_GOLF) {
                if(TextUtils.isEmpty(bpmValue) || TextUtils.isEmpty(bpmValueRest))
                {
                    bpmValueRest = "72";
                    bpmValue = "80";
                    calculateKcal(Integer.valueOf(bpmValue), Integer.valueOf(bpmValueRest), currentStep);
                }
                else {
                    calculateKcal(Integer.valueOf(bpmValue), Integer.valueOf(bpmValueRest), currentStep);
                }

                if(isSuccess == true)
                {
                    Load_pass();

                    // 성공 카운트 증가
                    workoutScore[currentStep] += 1;
                    //Log.d("workoutScore", "workoutScore = "  + workoutScore[currentStep] );
                }
            }
            else {
                int r = new Random().nextInt(3) + 1;
                String fileName = "icon_direction_" + r; // icon_direction_1, icon_direction_2, icon_direction_3
                int resId = getResources().getIdentifier(fileName, "drawable", getPackageName());
                golfDirection.setImageResource(resId);

                Log.d("change_sequence", "golfMapIndex = " + golfMapIndex);
                golfMapIndex++;
                golfMap.setBackgroundResource(golfMapImage[golfMapIndex]);

                updateGolfSwingData();
                playGolfSound();
            }

            // 운동 횟수 증가
            currentRep += 1;

            if(currentRep == totalRep)
            {
                // 운동 체크 시퀀스 끝
                sequnceTimeTask.cancel();

                currentRep = 0;
                currentStep += 1;

                Log.d("change_sequence", "currentStep = " + currentStep + ", totalStep = " + totalStep);


                // 전체 반복횟수  채움
                if(currentStep == totalStep) {

                    if(selectCourseNum != LOAD_GOLF) {
                        // 마지막 운동 이었을때
                        if(mGuideVideoView.isPlaying()) {

                            mGuideVideoView.stopPlayback();
                        }
                    } else {
                        // 마지막 운동 이었을때
                        if(golfGuideVideo.isPlaying()) {

                            golfGuideVideo.stopPlayback();
                        }
                    }

                    new Handler().postDelayed(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            if(selectCourseNum != LOAD_GOLF) {
                                Load_Finish();
                            } else {
                                finishGolf();
                            }
                        }
                    }, 3000);
                }
                else {


                    new Handler().postDelayed(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            if(selectCourseNum != LOAD_GOLF) {
                                Update_Workout();

                                if(mGuideVideoView.isPlaying())
                                {
                                    mGuideVideoView.stopPlayback();
                                }
                            } else {
                                if(golfGuideVideo.isPlaying())
                                {
                                    golfGuideVideo.stopPlayback();
                                }
                            }

                            //  다음 운동 로드
                            Reset_Msg();
							
                            Tracker_Reset();

                            if(selectCourseNum != LOAD_GOLF) {
                                playGuideVideo(mGuideVideoView, WorkoutvideoFile[currentStep]);
                                videoSizeUp(mGuideVideoView);
                                Video_CountDown();

                                update_rep();
                                update_step();
                            } else {
                                playGuideVideo(golfGuideVideo, WorkoutvideoFile[currentStep]);

                                videoSizeUp(golfGuideVideo);
                            }
                        }
                    }, 1000);

                }

            }
            else {
                // 다음 횟수로
                sequnceTimeTask.cancel();

                stepNum = 0;
                msgstep = 0;

                isEndpass = false;
                isSuccess = true;

                isCheck = true;

                //Reset_Msg();

                //start_entry();
                new Handler().postDelayed(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        // 운동 가이드 변경
                        Update_Msg();
                        // 다음 운동 시작
                        start_sequence();

                        // 운동 카운트 시작
                        //counter_sequence();

                    }

                }, 1000);

            }

//            update_rep();
//            update_step();

        }
        else
        {
            // 다음 자세로
            //sequnceTimeTask.cancel();

            //start_entry();
            isCheck = true;

            if(selectCourseNum != LOAD_GOLF) {
                update_rep();
                update_step();
            }

            Update_Msg();

        }
    }


    void counter_sequence()
    {
        Log.d("Countdown", "Countdown START");

        viewTimerCount.setVisibility(View.VISIBLE);

        countTimer = sequenceTimeout/1000+1;


        repCountDown = new CountDownTimer(sequenceTimeout, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

                Log.d("CountdownTimer", "" + countTimer);

                countTimer--;

                viewTimerCount.setText("" + countTimer);

                Log.d("CountdownTimer", "isEndpass = " + isEndpass);

                /*
                if(isEndpass == true)
                {
                    // 운동 성공했음
                    repCountDown.cancel();
                }*/
            }

            @Override
            public void onFinish() {

                Log.d("Countdown", "Countdown Finish");

                viewTimerCount.setVisibility(View.GONE);

                if(isEndpass == false)
                {
                    // 시간이 다되었는데 성공이 아닐경우
                    // 운동 체크 시퀀스 종료
                    sequnceTimeTask.cancel();

                    stepNum = 0;
                    msgstep = 0;

                    currentRep += 1;

                    if(currentRep == totalRep)
                    {
                        currentRep = 0;
                        currentStep += 1;

                        // 전체 반복횟수  채움
                        //  다음 운동 자세 로드
                        if(currentStep == totalStep) {
                            // 마지막 운동 이었을때
                            if(mGuideVideoView.isPlaying())
                            {
                                mGuideVideoView.stopPlayback();
                            }

                            new Handler().postDelayed(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    if(selectCourseNum != LOAD_GOLF) {
                                        Load_Finish();
                                    } else {
                                        finishGolf();
                                    }
                                }
                            }, 3000);
                        }
                        else {
                            //  다음 운동 로드
                            Tracker_Reset();

                            isEndpass = false;
                            isSuccess = true;

                            new Handler().postDelayed(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    /*
                                    start_entry();

                                    if(mGuideVideoView.isPlaying())
                                    {
                                        mGuideVideoView.stopPlayback();
                                    }
                                    Log.d("Video", "WorkoutvideoFile_0" + currentStep + " = " + WorkoutvideoFile[currentStep]);

                                    playGuideVideo(mGuideVideoView, WorkoutvideoFile[currentStep]);

                                    */

                                    if(mGuideVideoView.isPlaying())
                                    {
                                        mGuideVideoView.stopPlayback();
                                    }

                                    Log.d("Video", "WorkoutvideoFile_0" + currentStep + " = " + WorkoutvideoFile[currentStep]);

                                    playGuideVideo(mGuideVideoView, WorkoutvideoFile[currentStep]);

                                    videoSizeUp(mGuideVideoView);
                                    Video_CountDown();

                                    update_rep();
                                    update_step();
                                }
                            }, 2000);

                        }
                    }
                    else
                    {
                        // 다음 횟수로

                        sequnceTimeTask.cancel();
                        start_entry();

                        stepNum = 0;
                        msgstep = 0;

                        isEndpass = false;
                        isSuccess = true;

                        new Handler().postDelayed(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                start_sequence();

                                Update_Msg();
                            }
                        }, 1000);


                        update_rep();
                        update_step();
                    }
                }

            } // onfinish

        }.start();
    }

    public void update_rep()
    {
        if (currentStep < totalStep) {
            viewRepScore.setText(RepToString(totalRep, currentRep));

            workoutScores[currentStep].setText((currentRep+1) + " / " + totalRep);
        }
    }

    public void update_step()
    {
        if (currentStep < totalStep) {
            viewCourseStep.setText(StepToString(totalStep, currentStep + 1));
        }
    }

    class SequnceTimeTask extends TimerTask {
        @Override
        public void run() {

            runOnUiThread(new Runnable() {
                public void run() {

                    String str = "";

                    if (isUser == true) {
                        // 매 시퀀스당 조건 갯수

                        int Size = sequenceList.get(stepNum).passList.size();

                        int indexNum = 0;

                        if(stepNum == 0) {

                        }
                        else {

                            indexNum = stepNum - 1;

                            if (sequenceList.get(indexNum).getSuccessMethod() != null && isCheck == true)
                            {
                                Log.d("Match" , "---------------Check Value start--------------");

                                if(LastPos != null && LastPos[16] !=  null)
                                {
                                    //Log.d("Match" , "LastPos : " + LastPos[sequenceList.get(indexNum).getSuccessMethod().getSkeleton_a()].y);
                                    Log.d("Match" , "LastPos : " + LastPos[16].y);
                                }

                                if (sequenceList.get(indexNum).getSuccessMethod().getMethod().equalsIgnoreCase("minimum_value")) {

                                    Log.d("Match" , "---------------Check minimum_value start--------------");

                                    // 최소값 찾기
                                    if (sequenceList.get(indexNum).getSuccessMethod().getCheck_position().equalsIgnoreCase("x")) {
                                        if (UserPos[sequenceList.get(indexNum).getSuccessMethod().getSkeleton_a()].x < minValue) {
                                            minValue = UserPos[sequenceList.get(indexNum).getSuccessMethod().getSkeleton_a()].x;

                                            LastPos = UserPos.clone();
                                        }
                                    } else if (sequenceList.get(indexNum).getSuccessMethod().getCheck_position().equalsIgnoreCase("y")) {

                                        if (UserPos[sequenceList.get(indexNum).getSuccessMethod().getSkeleton_a()].y < minValue) {
                                            minValue = UserPos[sequenceList.get(indexNum).getSuccessMethod().getSkeleton_a()].y;
                                            LastPos = UserPos.clone();
                                        }
                                    } else if (sequenceList.get(indexNum).getSuccessMethod().getCheck_position().equalsIgnoreCase("z")) {
                                        if (UserPos[sequenceList.get(indexNum).getSuccessMethod().getSkeleton_a()].z < minValue) {
                                            minValue = UserPos[sequenceList.get(indexNum).getSuccessMethod().getSkeleton_a()].z;

                                            LastPos = UserPos.clone();
                                        }
                                    } else {

                                    }
                                    viewMaxValue.setText("min : " + minValue);

                                } else if (sequenceList.get(indexNum).getSuccessMethod().getMethod().equalsIgnoreCase("maximum_value")) {
                                    // 최소값이지만 아래로 갈수록 값이 증가 한다.
                                    Log.d("Match" , "---------------Check maximum_value start--------------");

                                    if (sequenceList.get(indexNum).getSuccessMethod().getCheck_position().equalsIgnoreCase("x")) {
                                        if (UserPos[sequenceList.get(indexNum).getSuccessMethod().getSkeleton_a()].x > maxValue) {
                                            maxValue = UserPos[sequenceList.get(indexNum).getSuccessMethod().getSkeleton_a()].x;
                                            LastPos = UserPos.clone();
                                        }
                                    } else if (sequenceList.get(indexNum).getSuccessMethod().getCheck_position().equalsIgnoreCase("y")) {

                                        if (UserPos[sequenceList.get(indexNum).getSuccessMethod().getSkeleton_a()].y > maxValue) {

                                            Log.d("Match" , "Prev : " +sequenceList.get(indexNum).getSuccessMethod().getSkeleton_a() + ", MaxValue : " + maxValue);

                                            maxValue = UserPos[sequenceList.get(indexNum).getSuccessMethod().getSkeleton_a()].y;
                                            LastPos = UserPos.clone();
                                            Log.d("Match" , "---------------Check maximum_value --------------");
                                            Log.d("Match" , "" +sequenceList.get(indexNum).getSuccessMethod().getSkeleton_a() + ", MaxValue : " + maxValue);


                                        }
                                    } else if (sequenceList.get(indexNum).getSuccessMethod().getCheck_position().equalsIgnoreCase("z")) {
                                        if (UserPos[sequenceList.get(indexNum).getSuccessMethod().getSkeleton_a()].z > maxValue) {
                                            maxValue = UserPos[sequenceList.get(indexNum).getSuccessMethod().getSkeleton_a()].z;

                                            LastPos = UserPos.clone();
                                        }
                                    } else {

                                    }
                                    if(LastPos != null && LastPos[16] !=  null)
                                    {
                                        //Log.d("Match" , "LastPos : " + LastPos[sequenceList.get(indexNum).getSuccessMethod().getSkeleton_a()].y);
                                        Log.d("Match" , "LastPos after : " + LastPos[16].y);
                                    }
                                    viewMaxValue.setText("max : " + maxValue);


                                } else if (sequenceList.get(indexNum).getSuccessMethod().getMethod().equalsIgnoreCase("minimum_distance")) {

                                    Log.d("Match" , "---------------Check minimum_distance start--------------");
                                    Log.d("Match", "min_distance : " + minDistance);

                                    // 두 관절의 선택 축의 높이 차가 최소 일떄의 관절좌표
                                    float distance = 0;

                                    if (sequenceList.get(indexNum).getSuccessMethod().getCheck_position().equalsIgnoreCase("x")) {
                                        float skeleton_a = UserPos[sequenceList.get(indexNum).getSuccessMethod().getSkeleton_a()].x;
                                        float skeleton_b = UserPos[sequenceList.get(indexNum).getSuccessMethod().getSkeleton_b()].x;

                                        distance = Math.abs(skeleton_a - skeleton_b);

                                        if (distance < minDistance) {
                                            minDistance = distance;
                                            LastPos = UserPos.clone();
                                        }

                                    } else if (sequenceList.get(indexNum).getSuccessMethod().getCheck_position().equalsIgnoreCase("y")) {
                                        float skeleton_a = UserPos[sequenceList.get(indexNum).getSuccessMethod().getSkeleton_a()].y;
                                        float skeleton_b = UserPos[sequenceList.get(indexNum).getSuccessMethod().getSkeleton_b()].y;

                                        distance = Math.abs(skeleton_a - skeleton_b);

                                        if (distance < minDistance) {
                                            minDistance = distance;
                                            LastPos = UserPos.clone();
                                        }

                                    } else if (sequenceList.get(indexNum).getSuccessMethod().getCheck_position().equalsIgnoreCase("z")) {
                                        float skeleton_a = UserPos[sequenceList.get(indexNum).getSuccessMethod().getSkeleton_a()].z;
                                        float skeleton_b = UserPos[sequenceList.get(indexNum).getSuccessMethod().getSkeleton_b()].z;

                                        distance = Math.abs(skeleton_a - skeleton_b);

                                        if (distance < minDistance) {
                                            minDistance = distance;
                                            LastPos = UserPos.clone();
                                        }
                                    }
                                    else {

                                    }

                                    viewMaxValue.setText("max : " + minDistance);


                                } else if (sequenceList.get(indexNum).getSuccessMethod().getMethod().equalsIgnoreCase("maximum_distance")) {

                                    Log.d("Match" , "---------------Check maximum_distance start--------------");


                                    // 두 관절의 선택 축의 높이 차가 최대 일떄의 관절좌표
                                    float distance = 0;

                                    if (sequenceList.get(indexNum).getSuccessMethod().getCheck_position().equalsIgnoreCase("x")) {
                                        float skeleton_a = UserPos[sequenceList.get(indexNum).getSuccessMethod().getSkeleton_a()].x;
                                        float skeleton_b = UserPos[sequenceList.get(indexNum).getSuccessMethod().getSkeleton_b()].x;

                                        distance = Math.abs(skeleton_a - skeleton_b);

                                        if (distance > maxDistance) {
                                            maxDistance = distance;
                                            LastPos = UserPos.clone();
                                        }

                                    } else if (sequenceList.get(indexNum).getSuccessMethod().getCheck_position().equalsIgnoreCase("y")) {
                                        float skeleton_a = UserPos[sequenceList.get(indexNum).getSuccessMethod().getSkeleton_a()].y;
                                        float skeleton_b = UserPos[sequenceList.get(indexNum).getSuccessMethod().getSkeleton_b()].y;

                                        distance = Math.abs(skeleton_a - skeleton_b);

                                        if (distance > maxDistance) {
                                            maxDistance = distance;
                                            LastPos = UserPos.clone();
                                        }

                                    } else if (sequenceList.get(indexNum).getSuccessMethod().getCheck_position().equalsIgnoreCase("z")) {
                                        float skeleton_a = UserPos[sequenceList.get(indexNum).getSuccessMethod().getSkeleton_a()].z;
                                        float skeleton_b = UserPos[sequenceList.get(indexNum).getSuccessMethod().getSkeleton_b()].z;

                                        distance = Math.abs(skeleton_a - skeleton_b);

                                        if (distance > maxDistance) {
                                            maxDistance = distance;
                                            LastPos = UserPos.clone();
                                        }
                                    }
                                    else {

                                    }

                                    viewMaxValue.setText("max : " + maxDistance);
                                } else {

                                }


                            } else {
                                // sequenceList.get(indexNum).getSuccessMethod() == null

                            }

                            //Log.d("SEQU", "StepNum = " + stepNum + ", pass Size = " + Size);
                        }

                        SequencePass = new boolean[Size];

                        for (int i = 0; i < Size; i++)
                        {
                            String methodName = sequenceList.get(stepNum).passList.get(i).getName();

                            Log.d("Match", "sequence index = " + sequenceList.get(stepNum).getIndex() + ", desc = " + sequenceList.get(stepNum).getDescription());

                            if (methodName.equalsIgnoreCase("compare_value")) {

                                if (SequencePass[i] == false) {

                                    Log.d("Match", "methodName = " + methodName + ", sequenceNum = " + stepNum + ", methodNum = " + i);

                                    SequencePass[i] = compare_value(sequenceList.get(stepNum).passList.get(i), UserPos);
                                }
                            } else if (methodName.equalsIgnoreCase("compare_distance_by_percent")) {

                                if (SequencePass[i] == false) {
                                    Log.d("Match", methodName + ", sequenceNum = " + stepNum + ", method Num = " + i);

                                    if(sequenceList.get(stepNum).passList.get(i).comparison_entry_sequence == true)
                                    {
                                        SequencePass[i] = compare_distance_by_percent(sequenceList.get(stepNum).passList.get(i), UserPos, EntryPos);
                                    }
                                    else
                                    {
                                        SequencePass[i] = compare_distance_by_percent(sequenceList.get(stepNum).passList.get(i), UserPos, UserPos);
                                    }

                                }

                            } else if (methodName.equalsIgnoreCase("compare_angle")) {
                                if (SequencePass[i] == false) {
                                    Log.d("Match", methodName + ", sequenceNum = " + stepNum + ", method Num = " + i);

                                    SequencePass[i] = compare_angle(sequenceList.get(stepNum).passList.get(i), UserPos);
                                }
                            } else {

                            }

                            Log.d("SEQU", "Pass bool " + i + " = " + SequencePass[i]);
                        }

                        if (allTrue(SequencePass))
                        {
                            //isCheck = false;

                            if(stepNum == 0) {

                            }
                            else
                            {
                                if(selectCourseNum == LOAD_GOLF) {

                                    if(stepNum == 1){
                                        mGolfSwingStartTime.setTime(System.currentTimeMillis());
                                        Log.d("GOLF", "Start Time : " + mGolfSwingStartTime.toString());
                                    }
                                    else if(stepNum == 2){
                                        mGolfSwingEndTime.setTime(System.currentTimeMillis());

                                        Log.d("GOLF", "END Time : " + mGolfSwingEndTime.toString());
                                        Log.d("GOLF", "Time : " + (mGolfSwingEndTime.getTime() - mGolfSwingStartTime.getTime()));

                                        startGolfBallAnimation();
                                    }
                                }

                                if (sequenceList.get(indexNum).successList != null)
                                {
                                    Log.d("Match" , "ALL PASS AND START SEQUENCE");
                                    Log.d("Match" , "---------------success start--------------");

                                    for(int i = 0; i < LastPos.length; i++)
                                    {
                                        Log.d("Match", "LastPos Num = " + i + " name : " + JointName[i] + " = " + LastPos[i].toString());
                                        Log.d("Match", "EntryPos Num = " + i + " name : " + JointName[i] + " = " + EntryPos[i].toString());


                                    }

                                    int list_Size = sequenceList.get(indexNum).successList.size();

                                    //Log.d("SEQU", "successList Size=" + sequenceList.get(stepNum).successList.size());

                                    if (sequenceList.get(indexNum).getSuccessMethod().getMethod().equalsIgnoreCase("minimum_value")) {

                                        if(list_Size > 0)
                                        {
                                            SequenceSuccess = new boolean[sequenceList.get(indexNum).successList.size()];

                                            for(int j = 0; j < list_Size; j++)
                                            {
                                                String metname = sequenceList.get(indexNum).successList.get(j).getName();

                                                if (metname.equalsIgnoreCase("compare_value")) {
                                                    SequenceSuccess[j] = compare_value(sequenceList.get(indexNum).successList.get(j), LastPos);
                                                    Log.d("matchdetail", "minimum_value CompareValue(seq_index" + indexNum + "-" + j + ") Desc : " + sequenceList.get(indexNum).successList.get(j).getFail_description());
                                                    Log.d("matchdetail", "minimum_value CompareValue(seq_index" + indexNum + "-" + j + ") result : " + SequenceSuccess[j]);
                                                /*
                                                if (SequenceSuccess[j] == false) {

                                                }
                                                */
                                                } else if (metname.equalsIgnoreCase("compare_distance_by_percent")) {

                                                    if(sequenceList.get(indexNum).successList.get(j).comparison_entry_sequence == true)
                                                    {
                                                        SequenceSuccess[j] = compare_distance_by_percent(sequenceList.get(indexNum).successList.get(j), LastPos, EntryPos);
                                                        Log.d("matchdetail", "minimum_value CompareDistance1(seq_index" + indexNum + "-" + j + ") Desc : " + sequenceList.get(indexNum).successList.get(j).getFail_description());
                                                        Log.d("matchdetail", "minimum_value CompareDistance1(seq_index" + indexNum + "-" + j + ") result : " + SequenceSuccess[j]);
                                                    }
                                                    else{
                                                        SequenceSuccess[j] = compare_distance_by_percent(sequenceList.get(indexNum).successList.get(j), LastPos, LastPos);
                                                        Log.d("matchdetail", "minimum_value CompareDistance2(seq_index" + indexNum + "-" + j + ") Desc : " + sequenceList.get(indexNum).successList.get(j).getFail_description());
                                                        Log.d("matchdetail", "minimum_value CompareDistance2(seq_index" + indexNum + "-" + j + ") result : " + SequenceSuccess[j]);
                                                    }

                                                } else if (metname.equalsIgnoreCase("compare_angle")) {

                                                    SequenceSuccess[j] = compare_angle(sequenceList.get(indexNum).successList.get(j), LastPos);
                                                }
                                                else {

                                                }

                                            }

                                            if (allTrue(SequenceSuccess) == false)
                                            {
                                                // 하나라도 틀리면 Fail 메시지 부름 (가장 먼저 fail 발생한 항목)
                                                for (int i=0; i<SequenceSuccess.length; i++) {
                                                    Log.d("Match", "SequenceSuccess(" + i + ") : " + SequenceSuccess[i]);
                                                    Log.d("Match", "SequenceSuccess(" + i + ") : " + sequenceList.get(indexNum).successList.get(i).getFail_description());
                                                    if (SequenceSuccess[i] == false) {
                                                        faildesc = sequenceList.get(indexNum).successList.get(i).getFail_description();
                                                        if(selectCourseNum == LOAD_GOLF) {
                                                            golf_fail_desc = sequenceList.get(indexNum).successList.get(i).getFail_golf_description();
                                                        }
                                                        break;
                                                    }
                                                }

                                                Log.d("Failed Desc", faildesc);
                                                if(selectCourseNum == LOAD_GOLF) {
                                                    updateGolfResultMsg(golf_fail_desc);
                                                }
                                                Load_Fail(faildesc);

                                                isSuccess = false;
                                            }
                                            else
                                            {
                                                // 모두 성공이면 없음
                                                //Load_pass();

                                                if(selectCourseNum == LOAD_GOLF) {
                                                    new Handler().postDelayed(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            stopGolfBallAnimation();
                                                        }
                                                    }, 1000);
                                                }
                                            }
                                        }

                                    } else if (sequenceList.get(indexNum).getSuccessMethod().getMethod().equalsIgnoreCase("maximum_value")) {

                                        if(list_Size > 0)
                                        {
                                            SequenceSuccess = new boolean[sequenceList.get(indexNum).successList.size()];

                                            for(int j = 0; j < list_Size; j++)
                                            {
                                                String metname = sequenceList.get(indexNum).successList.get(j).getName();

                                                if (metname.equalsIgnoreCase("compare_value")) {

                                                    SequenceSuccess[j] = compare_value(sequenceList.get(indexNum).successList.get(j), LastPos);
                                                    Log.d("matchdetail", "maximum_value CompareValue(seq_index" + indexNum + "-" + j + ") Desc : " + sequenceList.get(indexNum).successList.get(j).getFail_description());
                                                    Log.d("matchdetail", "maximum_value CompareValue(seq_index" + indexNum + "-" + j + ") result : " + SequenceSuccess[j]);
                                                } else if (metname.equalsIgnoreCase("compare_distance_by_percent")) {

                                                    if(sequenceList.get(indexNum).successList.get(j).comparison_entry_sequence == true)
                                                    {
                                                        SequenceSuccess[j] = compare_distance_by_percent(sequenceList.get(indexNum).successList.get(j), LastPos, EntryPos);
                                                        Log.d("matchdetail", "maximum_value CompareDistance1(seq_index" + indexNum + "-" + j + ") Desc : " + sequenceList.get(indexNum).successList.get(j).getFail_description());
                                                        Log.d("matchdetail", "maximum_value CompareDistance1(seq_index" + indexNum + "-" + j + ") result : " + SequenceSuccess[j]);
                                                    }
                                                    else{
                                                        SequenceSuccess[j] = compare_distance_by_percent(sequenceList.get(indexNum).successList.get(j), LastPos, LastPos);
                                                        Log.d("matchdetail", "maximum_value CompareDistance2(seq_index" + indexNum + "-" + j + ") Desc : " + sequenceList.get(indexNum).successList.get(j).getFail_description());
                                                        Log.d("matchdetail", "maximum_value CompareDistance2(seq_index" + indexNum + "-" + j + ") result : " + SequenceSuccess[j]);
                                                    }

                                                } else if (metname.equalsIgnoreCase("compare_angle")) {

                                                    SequenceSuccess[j] = compare_angle(sequenceList.get(indexNum).successList.get(j), LastPos);
                                                }
                                                else {

                                                }
                                            }

                                            if (allTrue(SequenceSuccess) == false)
                                            {
										     	// 하나라도 틀리면 Fail 메시지 부름 (가장 먼저 fail 발생한 항목)
                                                for (int i=0; i<SequenceSuccess.length; i++) {
                                                    Log.d("Match", "SequenceSuccess(" + i + ") : " + SequenceSuccess[i]);
                                                    Log.d("Match", "SequenceSuccess(" + i + ") : " + sequenceList.get(indexNum).successList.get(i).getFail_description());
                                                    if (SequenceSuccess[i] == false) {
                                                        faildesc = sequenceList.get(indexNum).successList.get(i).getFail_description();
                                                        if(selectCourseNum == LOAD_GOLF) {
                                                            golf_fail_desc = sequenceList.get(indexNum).successList.get(i).getFail_golf_description();
                                                        }
                                                        break;
                                                    }
                                                }

                                                Log.d("Failed Desc", faildesc);
                                                if(selectCourseNum == LOAD_GOLF) {
                                                    updateGolfResultMsg(golf_fail_desc);
                                                }
                                                Load_Fail(faildesc);

                                                isSuccess = false;
                                            }
                                            else
                                            {
                                                // 모두 성공이면 없음
                                                //Load_pass();

                                                if(selectCourseNum == LOAD_GOLF) {
                                                    new Handler().postDelayed(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            stopGolfBallAnimation();
                                                        }
                                                    }, 1000);
                                                }
                                            }

                                        }
                                    } else if (sequenceList.get(indexNum).getSuccessMethod().getMethod().equalsIgnoreCase("maximum_distance") ||
                                            sequenceList.get(indexNum).getSuccessMethod().getMethod().equalsIgnoreCase("minimum_distance")) {
                                        if(list_Size > 0)
                                        {
                                            SequenceSuccess = new boolean[sequenceList.get(indexNum).successList.size()];

                                            for(int j = 0; j < list_Size; j++)
                                            {
                                                String metname = sequenceList.get(indexNum).successList.get(j).getName();

                                                if (metname.equalsIgnoreCase("compare_value")) {

                                                    SequenceSuccess[j] = compare_value(sequenceList.get(indexNum).successList.get(j), LastPos);
                                                    Log.d("matchdetail", "maximum_distance CompareValue(seq_index" + indexNum + "-" + j + ") Desc : " + sequenceList.get(indexNum).successList.get(j).getFail_description());
                                                    Log.d("matchdetail", "maximum_distance CompareValue(seq_index" + indexNum + "-" + j + ") result : " + SequenceSuccess[j]);

                                                } else if (metname.equalsIgnoreCase("compare_distance_by_percent")) {

                                                    if(sequenceList.get(indexNum).successList.get(j).comparison_entry_sequence == true)
                                                    {
                                                        SequenceSuccess[j] = compare_distance_by_percent(sequenceList.get(indexNum).successList.get(j), LastPos, EntryPos);
                                                        Log.d("matchdetail", "S CompareDistance1(seq_index" + indexNum + "-" + j + ") Desc : " + sequenceList.get(indexNum).successList.get(j).getFail_description());
                                                        Log.d("matchdetail", "S CompareDistance1(seq_index" + indexNum + "-" + j + ") result : " + SequenceSuccess[j]);
                                                    }
                                                    else{
                                                        SequenceSuccess[j] = compare_distance_by_percent(sequenceList.get(indexNum).successList.get(j), LastPos, LastPos);
                                                        Log.d("matchdetail", "S CompareDistance2(seq_index" + indexNum + "-" + j + ") Desc : " + sequenceList.get(indexNum).successList.get(j).getFail_description());
                                                        Log.d("matchdetail", "S CompareDistance2(seq_index" + indexNum + "-" + j + ") result : " + SequenceSuccess[j]);
                                                    }

                                                } else if (metname.equalsIgnoreCase("compare_angle")) {

                                                    SequenceSuccess[j] = compare_angle(sequenceList.get(indexNum).successList.get(j), LastPos);
                                                }
                                                else {

                                                }
                                            }

                                            if (allTrue(SequenceSuccess) == false)
                                            {
                                                // 하나라도 틀리면 Fail 메시지 부름 (가장 먼저 fail 발생한 항목)
                                                for (int i=0; i<SequenceSuccess.length; i++) {
                                                    Log.d("Match", "SequenceSuccess(" + i + ") : " + SequenceSuccess[i]);
                                                    Log.d("Match", "SequenceSuccess(" + i + ") : " + sequenceList.get(indexNum).successList.get(i).getFail_description());
                                                    if (SequenceSuccess[i] == false) {
                                                        faildesc = sequenceList.get(indexNum).successList.get(i).getFail_description();
                                                        if(selectCourseNum == LOAD_GOLF) {
                                                            golf_fail_desc = sequenceList.get(indexNum).successList.get(i).getFail_golf_description();
                                                        }
                                                        break;
                                                    }
                                                }

                                                Log.d("Failed Desc", faildesc);
                                                if(selectCourseNum == LOAD_GOLF) {
                                                    updateGolfResultMsg(golf_fail_desc);
                                                }
                                                Load_Fail(faildesc);

                                                isSuccess = false;
                                            }
                                            else
                                            {
                                                // 모두 성공이면 없음
                                                //Load_pass();

                                                if(selectCourseNum == LOAD_GOLF) {
                                                    new Handler().postDelayed(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            stopGolfBallAnimation();
                                                        }
                                                    }, 1000);
                                                }
                                            }

                                        }


                                    }
                                }
                            }


                            // 다음 시퀀스 진행
                            change_sequence(false);
                        }
                        else {
                            // SequencePass가 모두 만족 안함
                        }
                    }
                }
            });
        }
    }


    class EntryTimeTask extends TimerTask {
        @Override
        public void run() {
            // 실시간 엔트리 체크
            //getCountTimer += 10;

            Log.d("Match" , "---------------entry start--------------");
            runOnUiThread(new Runnable() {
                public void run() {

                    String str = "";

                    /*
                    if(isUser == true && isEntry)
                    {
                        //compare_angle(sequenceList.get(1).successList.get(0), UserPos);

                        Log.d("Check Entry", "Method Name : " + sequenceList.get(0).passList.get(0).name);

                        compare_distance_by_percent(sequenceList.get(0).passList.get(0), UserPos);

                    }*/


                    if (isUser == true && isEntry == true)
                    {
                        /*
                        for(int i  = 0; i < entrySize; i++) {
                            EntryPass[i]= false;
                        }*/
                        // 엔트리 패스 초기화

                        Arrays.fill(EntryPass, false);

                        for (int i = 0; i < entrySize; i++) {
                            String methodName = entrypassconditions.get(i).getName();

                            if (methodName.equalsIgnoreCase("compare_value")) {
                                EntryPass[i] = compare_value(entrypassconditions.get(i), UserPos);
                            } else if (methodName.equalsIgnoreCase("compare_distance_by_percent")) {
                                EntryPass[i] = compare_distance_by_percent(entrypassconditions.get(i), UserPos, EntryPos);
                            } else if (methodName.equalsIgnoreCase("compare_angle")) {
                                EntryPass[i] = compare_angle(entrypassconditions.get(i), UserPos);
                            }

                            str += i + " : " + EntryPass[i] + ", ";
                        }

                        if (allTrue(EntryPass))
                        {
                            Log.d("Match" , "---------------entry end-------------");
                            isEntry = false;

                            Log.d("Entry", "All PASS");

                            // 모든 엔트리가 트루면

                            for (int i = 0; i < EntryPos.length; i++) {
                                EntryPos[i] = new Vector3(UserPos[i].x, UserPos[i].y, UserPos[i].z);
                                //Log.d("Match", "Pos Num = " + i + " name : " + JointName[i] + " = " + EntryPos[i].toString());
                            }

                            if(selectCourseNum != LOAD_GOLF) {
                                msgEntry.setVisibility(View.GONE);
                                msgIcon.setVisibility(View.GONE);
                            } else {
                                mBallView.setUserPos(EntryPos);
                                mBallView.setVisibility(View.VISIBLE);
                            }

                            start_sequence();

                            entryTimeTask.cancel();

                            // 운동 카운트 시작
                            // counter_sequence();

                        }
                        else {

                        }
                    }

                    // region Entry
                }
            });
        }
    }


    class UpdatePosTimeTask extends TimerTask {

        @Override
        public void run() {

            runOnUiThread(new Runnable() {
                public void run() {

                    if (isUser == true)
                    {
                        Call_ProjPosition();
                    }
                }
            });
        }
    }

    class UpdateMsgTimeTask extends  TimerTask
    {
        @Override
        public void run()
        {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {


                }
            });
        }
    }

    public void Load_Step() {
        Log.d("Size", "" + sequenceList.size());

    }

    private static boolean allTrue(boolean[] values) {
        for (boolean value : values) {
            if (!value)
                return false;
        }
        return true;
    }

    public void readCourse(XmlResourceParser parser) {

        Log.d("START", "read Course Start");
        try {
            int eventType = parser.getEventType();
            boolean isEnd = false;
            boolean isCos = false;
            boolean isRest = false;

            boolean isHeart = false;

            while (eventType != XmlResourceParser.END_DOCUMENT && isEnd == false) {
                String tagname;

                switch (eventType) {
                    case XmlResourceParser.START_TAG:
                        tagname = parser.getName();

                        if (tagname != null) {
                            Log.d("StartTagName", tagname);
                        }

                        if (tagname.equalsIgnoreCase("course")) {
                            course = new Course();

                            workouts = new ArrayList<Workout>();
                            isCos = true;
                        }
                        else if (tagname.equalsIgnoreCase("rest")) {
                            isRest = true;
                        } else if (tagname.equalsIgnoreCase("heartbeat")) {
                            isHeart = true;
                        } else if (tagname.equalsIgnoreCase("param")) {

                            if (isCos) {
                                String paramName = parser.getAttributeValue(0);
                                String paramValue;

                                if (paramName.equalsIgnoreCase("title")) {
                                    paramValue = parser.getAttributeValue(null, "value");
                                    course.setTitle(paramValue);
                                } else if (paramName.equalsIgnoreCase("desc")) {
                                    paramValue = parser.getAttributeValue(null, "value");
                                    course.setDesc(paramValue);
                                } else if (paramName.equalsIgnoreCase("level")) {
                                    paramValue = parser.getAttributeValue(null, "value");
                                    course.setLevel(paramValue);
                                } else if (paramName.equalsIgnoreCase("steps")) {
                                    course.setStep(parser.getAttributeIntValue(null, "value", 0));
                                } else if (paramName.equalsIgnoreCase("icon")) {
                                    paramValue = parser.getAttributeValue(null, "value");
                                    course.setIcon(paramValue);
                                } else if (paramName.equalsIgnoreCase("video")) {
                                    paramValue = parser.getAttributeValue(null, "value");
                                    course.setVideo(paramValue);
                                } else if (paramName.equalsIgnoreCase("set")) {
                                    course.setSet(parser.getAttributeIntValue(null, "value", 0));
                                } else if(paramName.equalsIgnoreCase("besttime")) {
                                    course.setBestTime(parser.getAttributeIntValue(null, "value", 0));
                                }


                                else if (isHeart) {
                                    paramName = parser.getAttributeValue(0);

                                    if (paramName.equalsIgnoreCase("1")) {
                                        heartbeats.add(parser.getAttributeIntValue(null, "value", 0));
                                    } else if (paramName.equalsIgnoreCase("2")) {
                                        heartbeats.add(parser.getAttributeIntValue(null, "value", 0));
                                    } else if (paramName.equalsIgnoreCase("3")) {
                                        heartbeats.add(parser.getAttributeIntValue(null, "value", 0));
                                    } else if (paramName.equalsIgnoreCase("4")) {
                                        heartbeats.add(parser.getAttributeIntValue(null, "value", 0));
                                    } else if (paramName.equalsIgnoreCase("5")) {
                                        heartbeats.add(parser.getAttributeIntValue(null, "value", 0));
                                    } else if (paramName.equalsIgnoreCase("6")) {
                                        heartbeats.add(parser.getAttributeIntValue(null, "value", 0));
                                    } else if (paramName.equalsIgnoreCase("7")) {
                                        heartbeats.add(parser.getAttributeIntValue(null, "value", 0));
                                    } else if (paramName.equalsIgnoreCase("8")) {
                                        heartbeats.add(parser.getAttributeIntValue(null, "value", 0));
                                    } else if (paramName.equalsIgnoreCase("9")) {
                                        heartbeats.add(parser.getAttributeIntValue(null, "value", 0));
                                    } else if (paramName.equalsIgnoreCase("10")) {
                                        heartbeats.add(parser.getAttributeIntValue(null, "value", 0));
                                    } else {

                                    }
                                }

                            } else if (isRest) {
                                String paramName = parser.getAttributeValue(0);

                                if (paramName.equalsIgnoreCase("resttime")) {
                                    course.setResttime(parser.getAttributeIntValue(null, "value", 0));
                                } else if (paramName.equalsIgnoreCase("desc")) {
                                    course.setRestdesc(parser.getAttributeValue(null, "value"));
                                }

                            } else {
                                // workout param
                                String paramName = parser.getAttributeValue(0);

                                if (paramName.equalsIgnoreCase("title")) {
                                    workout.setTitle(parser.getAttributeValue(null, "value"));
                                } else if (paramName.equalsIgnoreCase("repeat")) {
                                    workout.setRepeat(parser.getAttributeIntValue(null, "value", 0));
                                } else if (paramName.equalsIgnoreCase("heart_avg")) {
                                    workout.setHerart_avg(parser.getAttributeIntValue(null, "value", 0));
                                } else if (paramName.equalsIgnoreCase("icon")) {
                                    workout.setIcon(parser.getAttributeValue(null, "value"));
                                } else if (paramName.equalsIgnoreCase("video")) {
                                    workout.setVideo(parser.getAttributeValue(null, "value"));
                                } else if (paramName.equalsIgnoreCase("tracker")) {
                                    workout.setTracker(parser.getAttributeValue(null, "value"));
                                } else if (paramName.equalsIgnoreCase("actionicon_active")) {
                                    workout.setActionicon_active(parser.getAttributeValue(null, "value"));
                                } else if (paramName.equalsIgnoreCase("actionicon_out")) {
                                    workout.setActionicon_out(parser.getAttributeValue(null, "value"));
                                } else if (paramName.equalsIgnoreCase("guidetime")) {
                                    workout.setGuideTime(parser.getAttributeIntValue(null, "value", 10));
                                }

                            }


                        } else if (tagname.equalsIgnoreCase("workout")) {
                            isCos = false;
                            workout = new Workout();
                        }

                        break;

                    case XmlResourceParser.TEXT:
                        break;

                    case XmlResourceParser.END_TAG:
                        tagname = parser.getName();

                        if (tagname.equalsIgnoreCase("course")) {

                            course.setWorkoutList(workouts);

                            if (course != null) {

                                courses.add(course);
                            }
                            isEnd = false;

                        } else if (tagname.equalsIgnoreCase("rest")) {
                            isRest = false;

                        } else if (tagname.equalsIgnoreCase("param")) {


                        } else if (tagname.equalsIgnoreCase("heartbeat")) {
                            isHeart = false;

                        } else if (tagname.equalsIgnoreCase("workout")) {
                            if (workout != null) {
                                workouts.add(workout);
                            }
                        }
                        break;

                    default:
                        break;
                }
                eventType = parser.next();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void readInfo(XmlResourceParser parser) {
        Log.d("START", "ReadInfo Start");

        try {
            int eventType = parser.getEventType();
            boolean isEnd = false;


            while (eventType != XmlResourceParser.END_DOCUMENT && isEnd == false) {
                String tagname;

                switch (eventType) {
                    case XmlResourceParser.START_TAG:
                        tagname = parser.getName();

                        if (tagname != null) {
                            Log.d("StartTagName", tagname);
                        }

                        if (tagname.equalsIgnoreCase("Info")) {

                        } else if (tagname.equalsIgnoreCase("param")) {
                            String paramName = parser.getAttributeValue(0);
                            String paramValue;

                            Log.d("paramName", paramName);

                            if (paramName.equalsIgnoreCase("title")) {
                                paramValue = parser.getAttributeValue(null, "value");
                                Log.d("paramValue", paramValue);

                                titleName = paramValue;
                            } else if (paramName.equalsIgnoreCase("video")) {
                                paramValue = parser.getAttributeValue(null, "value");
                                videoName = paramValue;
                            }
                        }
                        break;

                    case XmlResourceParser.TEXT:
                        break;

                    case XmlResourceParser.END_TAG:

                        tagname = parser.getName();

                        if (tagname.equalsIgnoreCase("info")) {
                            Log.d("title", titleName);
                            Log.d("video", videoName);

                            isEnd = true;
                            Log.d("END While", "END Info");

                        }

                        break;

                    default:
                        break;
                }
                eventType = parser.next();

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void readEntry(String fileName) {

        Log.d("START", "readEntry Start");

        XmlPullParserFactory factory = null;
        XmlPullParser parser = null;


        File file = new File(PATH_XML_FILE_PATH + fileName + ".xml");

        try {

            factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            parser = factory.newPullParser();

            FileInputStream fis = new FileInputStream(file);
            parser.setInput(new InputStreamReader(fis));

            int eventType = parser.getEventType();
            boolean isEnd = false;


            while (eventType != XmlPullParser.END_DOCUMENT && isEnd == false) {
                String tagname;

                switch (eventType) {
                    case XmlPullParser.START_TAG:
                        tagname = parser.getName();

                        if (tagname != null) {
                            Log.d("Start Tag Name", tagname);
                        }

                        if (tagname.equalsIgnoreCase("entry_sequence")) {
                            String val = parser.getAttributeValue(0);
                            Log.d("entry_sequence desc", val);
                            entryDesc = val;
                        } else if (tagname.equalsIgnoreCase("passcondition")) {
                            // boolean val = parser.getAttributeBooleanValue(0, false);

                            // Log.d("pass value", ""+  val);
                            // entryRefered = val;
                        } else if (tagname.equalsIgnoreCase("checkmethod")) {
                            checkMethod = new CheckMethod();

                            checkMethod.setName(parser.getAttributeValue(null, "name"));
                            checkMethod.setCheck_position(parser.getAttributeValue(null, "check_position"));

                            checkMethod.setSkeleton_a(parser.getAttributeValue(null, "skeleton_a"));
                            checkMethod.setComparison_skeleton_a(parser.getAttributeValue(null, "comparison_skeleton_a"));

                            checkMethod.setComparison_formula(parser.getAttributeValue(null, "comparison_formula"));
                            checkMethod.setComparison_threshold(Float.parseFloat(parser.getAttributeValue(null, "comparison_threshold")));
                        }
                        break;

                    case XmlPullParser.TEXT:
                        break;

                    case XmlPullParser.END_TAG:

                        tagname = parser.getName();
                        if (tagname != null) {
                            Log.d("EndTagName", tagname);
                        }

                        if (tagname.equalsIgnoreCase("entry_sequence")) {
                            isEnd = true;
                            Log.d("END While", "END Entry");

                            // int count = entrypassconditions.size();

                            // Log.d("method Size", ""+ count);

                        } else if (tagname.equalsIgnoreCase("checkmethod")) {
                            if (checkMethod != null) {
                                entrypassconditions.add(checkMethod);
                            }
                        }
                        break;

                    default:
                        break;
                }
                eventType = parser.next();

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void readSequence(String fileName) {
        Log.d("START", "read Sequence Start");
        XmlPullParserFactory factory = null;
        XmlPullParser parser = null;


        File file = new File(PATH_XML_FILE_PATH + fileName + ".xml");

        try {

            factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            parser = factory.newPullParser();

            FileInputStream fis = new FileInputStream(file);
            parser.setInput(new InputStreamReader(fis));


            int eventType = parser.getEventType();
            boolean isSeq = false;
            boolean isEnd = false;
            boolean isPass = false;
            boolean isSucss = false;


            while (eventType != XmlPullParser.END_DOCUMENT && isEnd == false) {
                String tagname;

                switch (eventType) {
                    case XmlPullParser.START_TAG:
                        tagname = parser.getName();

                        if (tagname != null) {
                            Log.d("Start Tag Name`1", tagname);
                        }

                        if (tagname.equalsIgnoreCase("sequences")) {
                            int timeout = Integer.parseInt(parser.getAttributeValue(null, "timeout"));

                            Log.d("TimeOut", "" + timeout);

                            sequenceTimeout = timeout;

                            isSeq = true;

                        } else if (tagname.equalsIgnoreCase("sequence")) {
                            sequence = new Sequence();

                            sequence.setIndex(Integer.parseInt(parser.getAttributeValue(null, "index")));
                            sequence.setDescription(parser.getAttributeValue(null, "description"));

                        } else if(tagname.equalsIgnoreCase("actionimg")) {
                            sequence.setActionimg(parser.getAttributeValue(null, "name"));

                        } else if (tagname.equalsIgnoreCase("passcondition")) {
                            if (isSeq == true) {
                                //boolean val = parser.getAttributeValue(0, false);
                                //Log.d("sequence pass value", "" + val);

                                //sequence.setRefered(val);

                                sequencepassconditions = new ArrayList<CheckMethod>();
                                isPass = true;
                            }

                        } else if (tagname.equalsIgnoreCase("checkmethod")) {

                            if (isPass == true) {
                                String methodName = parser.getAttributeValue(null, "name");

                                Log.d("Methodname", methodName);

                                if (methodName.equalsIgnoreCase("compare_value")) {
                                    checkMethod = new CheckMethod();

                                    checkMethod.setName(parser.getAttributeValue(null, "name"));

                                    checkMethod.setCheck_position(parser.getAttributeValue(null, "check_position"));

                                    checkMethod.setSkeleton_a(parser.getAttributeValue(null, "skeleton_a"));

                                    checkMethod.setComparison_skeleton_a(parser.getAttributeValue(null, "comparison_skeleton_a"));

                                    checkMethod.setComparison_formula(parser.getAttributeValue(null, "comparison_formula"));

                                    checkMethod.setComparison_threshold(Float.parseFloat(parser.getAttributeValue(null, "comparison_threshold")));

                                } else if (methodName.equalsIgnoreCase("compare_distance_by_percent")) {
                                    checkMethod = new CheckMethod();

                                    checkMethod.setName(parser.getAttributeValue(null, "name"));

                                    checkMethod.setCheck_position(parser.getAttributeValue(null, "check_position"));

                                    checkMethod.setSkeleton_a(parser.getAttributeValue(null, "skeleton_a"));

                                    checkMethod.setSkeleton_b(parser.getAttributeValue(null, "skeleton_b"));

                                    checkMethod.setComparison_entry_sequence(parser.getAttributeValue(null, "comparison_entry_sequence"));

                                    checkMethod.setComparison_skeleton_a(parser.getAttributeValue(null, "comparison_skeleton_a"));

                                    checkMethod.setComparison_skeleton_b(parser.getAttributeValue(null, "comparison_skeleton_b"));

                                    checkMethod.setComparison_formula(parser.getAttributeValue(null, "comparison_formula"));

                                    Log.d("read", "comparison_formula = " + checkMethod.comparison_formula);

                                    if (checkMethod.comparison_formula.equalsIgnoreCase("equal")) {
                                        checkMethod.setComparison_threshold(Float.parseFloat(parser.getAttributeValue(null, "comparison_threshold")));
                                    } else {
                                        checkMethod.setComparison_value(Float.parseFloat(parser.getAttributeValue(null, "comparison_value")));
                                    }


                                } else if (methodName.equalsIgnoreCase("compare_angle")) {
                                    checkMethod = new CheckMethod();

                                    checkMethod.setName(parser.getAttributeValue(null, "name"));

                                    checkMethod.setCheck_position(parser.getAttributeValue(null, "check_position"));

                                    checkMethod.setSkeleton_a(parser.getAttributeValue(null, "skeleton_a"));

                                    checkMethod.setSkeleton_b(parser.getAttributeValue(null, "skeleton_b"));

                                    checkMethod.setComparison_skeleton_a(parser.getAttributeValue(null, "comparison_skeleton_a"));;

                                    checkMethod.setComparison_skeleton_b(parser.getAttributeValue(null, "comparison_skeleton_b"));

                                    checkMethod.setComparison_formula(parser.getAttributeValue(null, "comparison_formula"));

                                    checkMethod.setFail_description(parser.getAttributeValue(null, "fail_description"));

                                    if(selectCourseNum == LOAD_GOLF) {
                                        checkMethod.setFail_golf_description(parser.getAttributeValue(null, "golf_fail_description"));
                                    }

                                    checkMethod.setComparison_threshold(Float.parseFloat(parser.getAttributeValue(null, "comparison_threshold")));

                                }
                            } else if (isSucss == true) {
                                String methodName = parser.getAttributeValue(null, "name");

                                //Log.d("Methodname", methodName);

                                if (methodName.equalsIgnoreCase("compare_value")) {
                                    checkMethod = new CheckMethod();

                                    checkMethod.setName(parser.getAttributeValue(null, "name"));

                                    checkMethod.setCheck_position(parser.getAttributeValue(null, "check_position"));

                                    checkMethod.setSkeleton_a(parser.getAttributeValue(null, "skeleton_a"));

                                    checkMethod.setComparison_skeleton_a(parser.getAttributeValue(null, "comparison_skeleton_a"));

                                    checkMethod.setComparison_formula(parser.getAttributeValue(null, "comparison_formula"));

                                    checkMethod.setFail_description(parser.getAttributeValue(null, "fail_description"));

                                    if(selectCourseNum == LOAD_GOLF) {
                                        checkMethod.setFail_golf_description(parser.getAttributeValue(null, "golf_fail_description"));
                                    }

                                    checkMethod.setComparison_threshold(Float.parseFloat(parser.getAttributeValue(null, "comparison_threshold")));

                                } else if (methodName.equalsIgnoreCase("compare_angle")) {
                                    checkMethod = new CheckMethod();

                                    checkMethod.setName(parser.getAttributeValue(null, "name"));

                                    checkMethod.setCheck_position(parser.getAttributeValue(null, "check_position"));

                                    checkMethod.setSkeleton_a(parser.getAttributeValue(null, "skeleton_a"));

                                    checkMethod.setSkeleton_b(parser.getAttributeValue(null, "skeleton_b"));

                                    checkMethod.setComparison_skeleton_a(parser.getAttributeValue(null, "comparison_skeleton_a"));

                                    checkMethod.setComparison_skeleton_b(parser.getAttributeValue(null, "comparison_skeleton_b"));

                                    checkMethod.setComparison_formula(parser.getAttributeValue(null, "comparison_formula"));

                                    checkMethod.setFail_description(parser.getAttributeValue(null, "fail_description"));

                                    if(selectCourseNum == LOAD_GOLF) {
                                        checkMethod.setFail_golf_description(parser.getAttributeValue(null, "golf_fail_description"));
                                    }

                                    checkMethod.setComparison_threshold(Float.parseFloat(parser.getAttributeValue(null, "comparison_threshold")));

                                } else if (methodName.equalsIgnoreCase("compare_distance_by_percent")) {
                                    checkMethod = new CheckMethod();

                                    checkMethod.setName(parser.getAttributeValue(null, "name"));

                                    checkMethod.setCheck_position(parser.getAttributeValue(null, "check_position"));

                                    checkMethod.setSkeleton_a(parser.getAttributeValue(null, "skeleton_a"));

                                    checkMethod.setSkeleton_b(parser.getAttributeValue(null, "skeleton_b"));

                                    checkMethod.setComparison_skeleton_a(parser.getAttributeValue(null, "comparison_skeleton_a"));

                                    checkMethod.setComparison_skeleton_b(parser.getAttributeValue(null, "comparison_skeleton_b"));

                                    checkMethod.setComparison_formula(parser.getAttributeValue(null, "comparison_formula"));

                                    if (checkMethod.comparison_formula.equalsIgnoreCase("equal")) {
                                        checkMethod.setComparison_threshold(Float.parseFloat(parser.getAttributeValue(null, "comparison_threshold")));
                                    } else {
                                        checkMethod.setComparison_value(Float.parseFloat(parser.getAttributeValue(null, "comparison_value")));
                                    }

                                    checkMethod.setFail_description(parser.getAttributeValue(null, "fail_description"));
                                    if(selectCourseNum == LOAD_GOLF) {
                                        checkMethod.setFail_golf_description(parser.getAttributeValue(null, "golf_fail_description"));
                                    }
                                    checkMethod.setComparison_entry_sequence(parser.getAttributeValue(null, "comparison_entry_sequence"));

                                } else {

                                }
                            }

                        } else if (tagname.equalsIgnoreCase("successcondition")) {
                            isSucss = true;

                            sequencesuccessconditions = new ArrayList<CheckMethod>();
                            successMethod = new SuccessMethod();

                            String methodName = parser.getAttributeValue(null, "method");

                            Log.d("success", methodName);

                            successMethod.setMethod(methodName);
                            successMethod.setSkeleton_a(parser.getAttributeValue(null, "skeleton_a"));
                            successMethod.setSkeleton_b(parser.getAttributeValue(null, "skeleton_b"));
                            successMethod.setCheck_position(parser.getAttributeValue(null, "check_position"));
                        }
                        break;

                    case XmlPullParser.TEXT:
                        break;

                    case XmlPullParser.END_TAG:

                        tagname = parser.getName();

                        if (tagname != null) {
                            Log.d("EndTagName", tagname);
                        }

                        if (tagname.equalsIgnoreCase("sequences")) {
                            isEnd = true;

                            Log.d("END While", "END sequences");
                        } else if (tagname.equalsIgnoreCase("sequence")) {
                            if (sequence != null) {
                                sequenceList.add(sequence);
                            }
                        } else if (tagname.equalsIgnoreCase("passcondition")) {
                            if (sequencepassconditions != null) {
                                sequence.setPassList(sequencepassconditions);
                            }

                            isPass = false;

                        } else if (tagname.equalsIgnoreCase("successcondition")) {
                            if (successMethod != null) {
                                sequence.setSuccessMethod(successMethod);
                            }
                            if (sequencesuccessconditions != null) {
                                sequence.setSuccessList(sequencesuccessconditions);
                            }
                        } else if (tagname.equalsIgnoreCase("checkmethod")) {


                            if (checkMethod != null) {
                                //Log.d("END CHECK Method", "" + checkMethod.skeleton_a);

                                if (isSeq == true) {
                                    if (isPass == true) {
                                        sequencepassconditions.add(checkMethod);
                                    } else if (isSucss == true) {
                                        sequencesuccessconditions.add(checkMethod);
                                    }
                                }
                            }
                        }
                        break;

                    default:
                        break;
                }
                eventType = parser.next();

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }



    public void StartTimer(final int _counttime){
        runOnUiThread(new Runnable() {
            public void run() {
                layerStartCount.setVisibility(View.VISIBLE);
                layerStartCount.bringToFront();

                countDownTimer = new CountDownTimer(_counttime, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                        viewStartCount.setText(String.valueOf(countTimer));

                        countTimer--;
                    }

                    @Override
                    public void onFinish() {

                        layerStartCount.setVisibility(View.INVISIBLE);

                        countTimer = 3;
                        isCheck = true;

                        // Entry Start
                        start_entry();

                        updateStepnum();

                        //mGuideVideoView.setVideoURI(WorkoutvideoFile[0]);
                        //mGuideVideoView.start();
                    }

                }.start();
            }
        });
    }

    public void Update_Workout() {

        for(int i =0; i < workoutSize; i++)
        {
            if(i == currentStep) {

                workoutIcons[i].setImageDrawable(iconActives[i]);

                workoutNames[i].setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.coloractive));

                workoutScores[i].setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.coloractive));


            } else
            {
                workoutIcons[i].setImageDrawable(iconOuts[i]);

                workoutNames[i].setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colornormal));

                workoutScores[i].setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colornormal));
            }

        }

    }

    public void updateStepnum() {
        viewStepNum.setText("" + stepNum);
        viewmsgStep.setText("" + msgstep);
    }

    public void Init_Msg() {

        if(selectCourseNum != LOAD_GOLF) {
            for(int i =0; i < trackerguides.length;i++)
            {
                guideAdapter.addItem(ContextCompat.getDrawable(this,R.drawable.icon_chk_i), trackerguides[i]);
            }

            //msgGuide.setText(trackerguides[0]);
        } else {
            golfEntryImage.setBackgroundResource(R.drawable.icon_golf_swing_01);
        }
    }

    public void Reset_Msg() {
        if(selectCourseNum != LOAD_GOLF) {
            listView.setVisibility(View.GONE);
            listView.setSelection(0);

            layerFail.setVisibility(View.GONE);
            msgFail.setVisibility(View.GONE);
        } else {
            //golfEntryImage.setBackgroundResource(R.drawable.icon_golf_swing_01);
        }

        /*
        msgIcon.setVisibility(View.GONE);
        msgGuide.setVisibility(View.GONE);

        msgGuide.setText(trackerguides[0]);*/
    }

    public void On_Msg() {
        if(selectCourseNum != LOAD_GOLF) {
            msgEntry.setVisibility(View.GONE);
            //msgIcon.setVisibility(View.GONE);
            listView.setVisibility(View.VISIBLE);
            //msgGuide.setVisibility(View.VISIBLE);
        } else {
            //golfEntryImage.setBackgroundResource(R.drawable.icon_golf_swing_01);
        }
    }

    public void Update_Msg() {

        // 가이드 문구 업데이트

        // Log.d("TRACKER RESET", "stepNum = " + stepNum + ", sequenceSize = " + sequenceSize);
        // msgGuide.setText(trackerguides[stepNum]);

        guideAdapter.index = stepNum;
        guideAdapter.notifyDataSetChanged();

        Log.d("Update Msg", "stepNum = " + stepNum + ", sequenceSize = " + sequenceSize);

        if(selectCourseNum != LOAD_GOLF) {
            if(stepNum == 0)
            {
                listView.setSelection(0);
            }
            else if(stepNum > 1)
            {
                listView.setSelection(stepNum -1);
            }
        } else {
            String imageName = sequenceList.get(stepNum).getActionimg();
            if (imageName != null) {
                int imgnum = getResources().getIdentifier(sequenceList.get(stepNum).getActionimg(), "drawable", getPackageName());
                golfEntryImage.setBackgroundResource(imgnum);
            }
        }

        /*
        if(stepNum == 2  && sequenceSize <= 5 )
        {
            listView.setSelection(1);
        }
        else if(stepNum == 4)
        {
            listView.setSelection(3);
        }
        else if(stepNum == 2 && sequenceSize == 6)
        {
            listView.setSelection(1);
        }
        else if(stepNum == 0)
        {
            listView.setSelection(0);
        }*/
    }

    private void updateGolfResultMsg(String msg) {
        Log.d("GOLF", "updateGolfResultMsg called: " + msg);
        if(selectCourseNum != LOAD_GOLF || msg == null) {
            return;
        }

        for(int i = 1; i < golfFailList.size(); i++) {
            if(golfFailList.get(i).equals(msg)) {
                Log.d("GOLF", "updateGolfResultMsg has already same message " + i + "th: " + golfFailList.get(i) );
                return;
            }
        }

        if(golfFailList.size() < 4) {
            golfFailList.add(msg);
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                stopGolfBallAnimation();
            }
        }, 1000);
    }

    public void Load_Fail(String msg)
    {
        if(layerFail.getVisibility() == View.VISIBLE)
        {
            return;
        }

        if(mttsSingle != null && msg.length() > 0) {
            mttsSingle.speakGuide(msg);
        }

        msgFail.setText(msg);

        layerFail.setVisibility(View.VISIBLE);
        msgFail.setVisibility(View.VISIBLE);

        countDownTimer = new CountDownTimer(5000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {

                layerFail.setVisibility(View.GONE);
                msgFail.setVisibility(View.GONE);

                layoutGolfFail.setVisibility(View.GONE);
                golfFailMsg.setVisibility(View.GONE);

            }

        }.start();

        if(selectCourseNum == LOAD_GOLF) {
            layoutGolfFail.setVisibility(View.VISIBLE);
            golfFailMsg.setVisibility(View.VISIBLE);

            golfFailMsg.setText(msg);

//            if(golfFailList.size() < 5) {
//                golfFailList.add(msg);
//            }
        }
    }

    public void Load_pass()
    {
        layerFail.setVisibility(View.GONE);
        msgFail.setVisibility(View.GONE);

        if(mttsSingle != null) {
            mttsSingle.speakGuide(PASS_MESSAGE);
        }

        viewpasscheck.setVisibility(View.VISIBLE);

        countDownTimer = new CountDownTimer(2000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                viewpasscheck.setVisibility(View.GONE);
            }

        }.start();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mUnityPlayer.start();
    }

    @Override
    protected void onStop() {
        //dataTimer.cancel();
        super.onStop();
        mUnityPlayer.stop();
    }

    // Quit Unity
    @Override
    protected void onDestroy() {
        mUnityPlayer.quit();
        unregisterReceiver(mBroadcastReceiver);
        mttsSingle.shutdownTTS();
        super.onDestroy();
    }

    // Pause Unity
    @Override
    protected void onPause() {
        super.onPause();
        mUnityPlayer.pause();

        /*-- START : Simulator --*/
        if ((mMotionRecordMode != STORING_MODE_OFF) && mIsMotionRecordRunning == true) {
            mMotionRecordMode = STORING_MODE_OFF;
            mIsMotionRecordRunning = false;
            showFileNamePopup();
        }
        /*-- END : Simulator --*/
    }

    // Resume Unity
    @Override
    protected void onResume() {
        super.onResume();
        mUnityPlayer.resume();
    }


    // Low Memory Unity
    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mUnityPlayer.lowMemory();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        // To support deep linking, we need to make sure that the client can get access to
        // the last sent intent. The clients access this through a JNI api that allows them
        // to get the intent set on launch. To update that after launch we have to manually
        // replace the intent with the one caught here.
        setIntent(intent);
    }

    // Trim Memory Unity
    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        if (level == TRIM_MEMORY_RUNNING_CRITICAL) {
            mUnityPlayer.lowMemory();
        }
    }

    // This ensures the layout will be correct.
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mUnityPlayer.configurationChanged(newConfig);
    }

    // Notify Unity of the focus change.
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        mUnityPlayer.windowFocusChanged(hasFocus);
    }

    // For some reason the multiple keyevent type is not supported by the ndk.
    // Force event injection by overriding dispatchKeyEvent().
    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_MULTIPLE)
            return mUnityPlayer.injectEvent(event);
        return super.dispatchKeyEvent(event);
    }

    // Pass any events not handled by (unfocused) views straight to UnityPlayer
    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        return mUnityPlayer.injectEvent(event);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        /*-- START : Simulator --*/
        //start immediately
        //moved to onTouchListner
        /*if (mIsMotionRecordMode == true) {
            if (mIsMotionRecordRunning == false) {
                if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
                    Toast.makeText(
                            this,
                            "start xml storing",
                            Toast.LENGTH_LONG).show();
                    mJointList.clear();
                    mJointTime.clear();
                    mIsMotionRecordRunning = true;
                } else if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
                    Toast.makeText(
                            this,
                            "start xml storing after 5 sec",
                            Toast.LENGTH_LONG).show();
                    mTimeoutHandler.sendMessageDelayed(mTimeoutHandler.obtainMessage(STORING_START_WITH_DELAY),
                            5000);
                }
            } else {
                if (keyCode == KeyEvent.KEYCODE_VOLUME_UP || keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
                    mIsMotionRecordMode = false;
                    mIsMotionRecordRunning = false;
                    showFileNamePopup();
                }
            }
            return mUnityPlayer.injectEvent(event);
        }*/
        /*-- END : Simulator --*/

        if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
            UnityPlayer.UnitySendMessage("GM", "RestartAppForAOS", "0");


        } else if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
            UnityPlayer.UnitySendMessage("GM", "RestartAppForAOS", "1");
        }

        return mUnityPlayer.injectEvent(event);

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        Log.d("TOUCH", "Touch Event : " + event);

        return mUnityPlayer.injectEvent(event);
    }

    /*API12*/
    public boolean onGenericMotionEvent(MotionEvent event) {
        return mUnityPlayer.injectEvent(event);
    }

    public void Set_Mode(String mode) {
        UnityPlayer.UnitySendMessage("GM", "Set_Mode", mode);
    }

    public void Call_Test() {
        Log.d("Call", "Call_Test");
    }

    public void Call_RealPosition() {
        //Log.d("Call", "Call_RealPosition");

        UnityPlayer.UnitySendMessage("GM", "Read_RealPos", "");
    }

    public void Call_ProjPosition() {

        // Log.d("Call", "Call_ProjPosition");

        UnityPlayer.UnitySendMessage("GM", "Read_ProjPos", "");
    }

    public void updatePosition(String[] type) {
        // Set Position
//        boolean isValable = false;
        if (isUser) {
            for (int i = 0; i < type.length; i++) {

                //Log.d("body", i + " : " + JointName[i] + " : " + type[i].toString());

                //Log.d("SIZE", "" + UserPos.length);

//                Log.d("updatePosition", "UserPos[" + JointName[i] + "] confidence = "  + Confidence[i]);
//                if(Float.parseFloat(Confidence[i]) >= 0.75) {
                    UserPos[i] = new Vector3(type[i]);
//                    isValable = true;
//                }
                //Log.d("UserPos", "UserPos[" + i + "] = " + UserPos[i]);
            }
        }

        /*-- START : Simulator --*/
        if (/*isValable == true &&*/ type != null && type.length >= JointName.length) {
            if ((mMotionRecordMode != STORING_MODE_OFF) && mIsMotionRecordRunning) {
                mJointList.add(type);
                final long currentTime = SystemClock.uptimeMillis();
                mJointTime.add(Long.toString(currentTime));
            }
        }
        /*-- END : Simulator --*/

    }

    public void pause_CountDown()
    {
        pauseCountDown = new CountDownTimer(pauseTime, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

                // pause 카운트
                viewPauseCount.setText("" + pausecountTimer);

                pausecountTimer -= 1;
            }

            @Override
            public void onFinish() {


                Log.d("pause_CountDown", "pause_CountDown OnFinish");


                // 게임 종료 메인으로
                //UnityPlayer.UnitySendMessage("GM", "Load_Reset", "");
            }



        }.start();

    }

    public void user_in() {
        isUser = true;

        if(isPlay)
        {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            layerPause.setVisibility(View.GONE);

                            if(pauseCountDown != null)
                            {
                                pauseCountDown.cancel();
                            }
                        }

                    });
                }

            }).start();
        }
    }

    public void user_out()
    {
        isUser = false;

        if(isPlay)
        {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            pausecountTimer = 30;
                            layerPause.setVisibility(View.VISIBLE);
                            layerPause.bringToFront();
                            pause_CountDown();

                        }

                    });
                }

            }).start();
        }
    }

    private void Golf_UI_Init() {
        gameui.setVisibility(View.GONE);
        layoutGolf.setVisibility(View.VISIBLE);

        layoutGolf.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(isEntry)
                {
                    isEntry = false;

                    for (int i = 0; i < EntryPos.length; i++) {
                        EntryPos[i] = new Vector3(UserPos[i].x, UserPos[i].y, UserPos[i].z);
                        //Log.d("Match", "Pos Num = " + i + " name : " + JointName[i] + " = " + EntryPos[i].toString());
                    }

                    start_sequence();

                    entryTimeTask.cancel();
                }
                else
                {
                    change_sequence(true);
                }

                return false;
            }
        });
    }

    public void UI_Init() {

        startTime = new Time(System.currentTimeMillis());

        new Thread(new Runnable() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        gameui.setVisibility(View.VISIBLE);
                        //gameui.bringToFront();

                        gameui.setOnTouchListener(new View.OnTouchListener() {
                            @Override
                            public boolean onTouch(View v, MotionEvent event) {

                                if (mMotionRecordMode != STORING_MODE_OFF) {
                                    if (mIsMotionRecordRunning == false) {
                                        if (mMotionRecordMode == STORING_MODE_ON) {
                                            Toast.makeText(
                                                    mContext,
                                                    "start xml storing",
                                                    Toast.LENGTH_LONG).show();
                                            mJointList.clear();
                                            mJointTime.clear();
                                            mIsMotionRecordRunning = true;
                                        } else {
                                            Toast.makeText(mContext,
                                                    "start xml storing after 5 sec",
                                                    Toast.LENGTH_LONG).show();
                                            mTimeoutHandler.sendMessageDelayed(mTimeoutHandler.obtainMessage(STORING_START_WITH_DELAY),
                                                    5000);
                                        }
                                    } else {
                                        mMotionRecordMode = STORING_MODE_OFF;
                                        mIsMotionRecordRunning = false;
                                        showFileNamePopup();
                                    }
                                    return false;
                                }

                                Log.d("TOUCH", "Touch event GameUI");

                                /*// 가이드 강제 진행
                                if(isUser)
                                {
                                    change_sequence(true);
                                }
*/
                                if(isEntry)
                                {
                                    isEntry = false;

                                    for (int i = 0; i < EntryPos.length; i++) {
                                        EntryPos[i] = new Vector3(UserPos[i].x, UserPos[i].y, UserPos[i].z);
                                        //Log.d("Match", "Pos Num = " + i + " name : " + JointName[i] + " = " + EntryPos[i].toString());
                                    }

                                    start_sequence();

                                    entryTimeTask.cancel();

                                    msgEntry.setVisibility(View.GONE);
                                    msgIcon.setVisibility(View.GONE);
                                }
                                else
                                {
                                    change_sequence(true);
                                }

                                return false;
                            }
                        });

                        Log.d("Init", "UI_Init");

                    }
                });
            }

        }).start();


        // StartTimer();
    }

    /**
     * 1. compare_value
     * <p>
     * 설명 : 지정된 두개 관절의 값을 비교한다.
     * <p>
     * Attributes
     * <p>
     * 1) name : "compare_value"
     * <p>
     * 2) check_position : "x(너비) / y(높이) / z(깊이)"
     * <p>
     * 3) skeleton_a : source skeleton. 현재 시점의 비교할 관절 번호, nuitrack에서 정의 된 Joint enum 상수 값에 따른다.
     * <p>
     * 4) comparison_skeleton_a : target skeleton. 비교할 관절의 번호, nuitrack에서 정의 된 Joint enum 상수 값에 따른다.
     * <p>
     * 5) comparison_formula : 비교할 방식, "smaller / bigger / equal"
     * <p>
     * 6) comparison_threshold : 임계점, 값을 비교할 때 인정 허용 범위. +-'값'
     * <p>
     * 7) fail_description : 해당 'checkmethod'가 'successcondition'에서 사용되었을 때, 화면에 표시할 정보
     */


    public boolean compare_value(CheckMethod body, Vector3[] bodyPos) {

        boolean isPass = false;

        float threshold = 0;
        float skeletonA = 0;
        float skeletonComparisonA = 0;

        float compare_threshold = body.comparison_threshold;


        if (body.check_position.equalsIgnoreCase("x")) {
            skeletonA = bodyPos[body.skeleton_a].x;
            skeletonComparisonA = bodyPos[body.comparison_skeleton_a].x;

            Log.d("Match", body.skeleton_a + " skeleton_a.X  = " + bodyPos[body.skeleton_a].x);
            Log.d("Match", body.comparison_skeleton_a + " comparison_skeleton_a.X  = " + bodyPos[body.comparison_skeleton_a].x);

        } else if (body.check_position.equalsIgnoreCase("y")) {
            skeletonA = bodyPos[body.skeleton_a].y;
            skeletonComparisonA = bodyPos[body.comparison_skeleton_a].y;

            Log.d("Match", body.skeleton_a + " skeleton_a.Y  = " + bodyPos[body.skeleton_a].y);
            Log.d("Match", body.comparison_skeleton_a + " comparison_skeleton_a.Y  = " + bodyPos[body.comparison_skeleton_a].y);

        } else if (body.check_position.equalsIgnoreCase("z")) {
            skeletonA = bodyPos[body.skeleton_a].z;
            skeletonComparisonA = bodyPos[body.comparison_skeleton_a].z;

            Log.d("Match", body.skeleton_a + " skeleton_a.Z  = " + bodyPos[body.skeleton_a].z);
            Log.d("Match", body.comparison_skeleton_a + " comparison_skeleton_a.Z  = " + bodyPos[body.comparison_skeleton_a].z);
        }
        threshold = Math.abs(skeletonA - skeletonComparisonA);

        if (body.comparison_formula.equalsIgnoreCase("smaller")) {

            if (skeletonA < skeletonComparisonA && threshold > compare_threshold) {
                isPass = true;
            } else {
                isPass = false;
            }

        } else if (body.comparison_formula.equalsIgnoreCase("bigger")) {
            if (skeletonA > skeletonComparisonA && threshold > compare_threshold) {
                isPass = true;
            } else {
                isPass = false;
            }
        } else if (body.comparison_formula.equalsIgnoreCase("equal")) {
            if (threshold <= compare_threshold) {
                isPass = true;
            } else {
                isPass = false;
            }

        } else {
            isPass = false;
        }

        Log.d("CompareValue", "fomula = " + body.comparison_formula);

        //Log.d("CompareValue", "skeleton_a = " + body.skeleton_a);
        //Log.d("CompareValue", "comparison_skeleton_a = " + body.comparison_skeleton_a);
        Log.d("Match", "threshold = " + threshold);
        Log.d("Match", "comparison_threshold = " + compare_threshold);

        //Log.d("Match", "Equal Value = " + Math.abs((compare_threshold - threshold)));

        Log.d("Match" , "isPASS = " + isPass);


        return isPass;
    }


    /**
     * 2. compare_angle
     * <p>
     * 설명 : 2개 관절 사이의 각도를, 또 다른 2개 관절의 사이의 각도와 일치하는지 비교한다.
     * <p>
     * Attributes
     * <p>
     * 1) name : "compare_angle"
     * <p>
     * 2) check_position : 회전축. xy(너비*높이) / yz(높이*깊이) / xz(너비*깊이)
     * <p>
     * 3) skeleton_a, skeleton_b : source skeleton. 각도를 측정할 두개의 관절, nuitrack에서 정의 된 Joint enum 상수 값에 따른다.
     * <p>
     * 4) comparison_skeleton_a, comparison_skeleton_b : target skeleton. 각도를 측정할 두개의 관절, nuitrack에서 정의 된 Joint enum 상수 값에 따른다.
     * <p>
     * 5) comparison_formula : 비교할 방식, "smaller / bigger / equal"
     * <p>
     * 6) comparison_threshold : 임계점, 값이 동등한지 비교할 때 인정 허용 범위. +-'값'.
     * <p>
     * 7) fail_description : 해당 'checkmethod'가 'successcondition'에서 사용되었을 때, 화면에 표시할 정보
     **/

    public float get_angle(float P1X , float P1Y, float P2X, float P2Y)
    {

        //Log.d("get_angle", "P1 (" + P1X +  ", "+ P1Y + ")");
        //Log.d("get_angle", "P2 (" + P2X +  ", "+ P2Y + ")");

        float dx = P2X - P1X;

        float dy = P2Y - P1Y;

        double angleRad = Math.atan2(dy, dx);

        double angleDeg = (angleRad * 180) / Math.PI;


        if (angleDeg < 0) {
            angleDeg = 180 + angleDeg;
        }

        return  (float)angleDeg;
    }

    public boolean compare_angle(CheckMethod body, Vector3[] bodyPos) {

        boolean isPass = false;

        float angle_a = 0;
        float angle_b = 0;

        float threshold = 0;

        float compare_threshold = body.comparison_threshold;


        // check_position : 회전축. xy(너비*높이) / yz(높이*깊이) / xz(너비*깊이)

        if (body.check_position.equalsIgnoreCase("xy")) {

            angle_a = get_angle(bodyPos[body.skeleton_a].x, bodyPos[body.skeleton_a].y, bodyPos[body.skeleton_b].x, bodyPos[body.skeleton_b].y);
            angle_b = get_angle(bodyPos[body.comparison_skeleton_a].x, bodyPos[body.comparison_skeleton_a].y, bodyPos[body.comparison_skeleton_b].x, bodyPos[body.comparison_skeleton_b].y);

        }
        else if (body.check_position.equalsIgnoreCase("yz")) {
            angle_a = get_angle(bodyPos[body.skeleton_a].y, bodyPos[body.skeleton_a].z, bodyPos[body.skeleton_b].y, bodyPos[body.skeleton_b].z);
            angle_b = get_angle(bodyPos[body.comparison_skeleton_a].y, bodyPos[body.comparison_skeleton_a].z, bodyPos[body.comparison_skeleton_b].y, bodyPos[body.comparison_skeleton_b].z);


        }
        else if (body.check_position.equalsIgnoreCase("xz")) {
            angle_a = get_angle(bodyPos[body.skeleton_a].x, bodyPos[body.skeleton_a].z, bodyPos[body.skeleton_b].x, bodyPos[body.skeleton_b].z);
            angle_b = get_angle(bodyPos[body.comparison_skeleton_a].x, bodyPos[body.comparison_skeleton_a].z, bodyPos[body.comparison_skeleton_b].x, bodyPos[body.comparison_skeleton_b].z);

        }

        threshold = Math.abs(angle_a - angle_b);



        //viewMaxValue.setText(" Angle_A = " + (int)angle_a + ", Angle_B = " + (int)angle_b);

        if (body.comparison_formula.equalsIgnoreCase("smaller")) {
            if (angle_a < angle_b) {
                isPass = true;
            } else {
                isPass = false;
            }
        } else if (body.comparison_formula.equalsIgnoreCase("bigger")) {
            if (angle_a > angle_b) {
                isPass = true;
            } else {
                isPass = false;
            }
        } else if (body.comparison_formula.equalsIgnoreCase("equal")) {
            if (threshold <= compare_threshold) {
                isPass = true;
            } else {
                isPass = false;
            }
        } else {
            isPass = false;
        }


        Log.d("Match", "check_position = " + body.check_position);
        Log.d("Match", body.skeleton_a + " skeleton_a.x  = " + bodyPos[body.skeleton_a].x + ", " + " skeleton_a.x  = " + bodyPos[body.skeleton_a].y);
        Log.d("Match", body.skeleton_b + " skeleton_b.x  = " + bodyPos[body.skeleton_b].x + ", " + " skeleton_b.x  = " + bodyPos[body.skeleton_b].y);
        Log.d("Match", body.comparison_skeleton_a + " comparison_skeleton_a.x  = " + bodyPos[body.comparison_skeleton_a].x + ", " + " comparison_skeleton_a.y  = " + bodyPos[body.comparison_skeleton_a].y);
        Log.d("Match", body.comparison_skeleton_b + " comparison_skeleton_b.x  = " + bodyPos[body.comparison_skeleton_b].x + ", " + " comparison_skeleton_b.x  = " + bodyPos[body.comparison_skeleton_b].y);

        Log.d("Match", "Angle_A = " + angle_a + ", Angle_B = " + angle_b);
        Log.d("Match", "threshold = " + threshold);



        Log.d("Match angle" , "isPASS = " + isPass);

        return isPass;

    }

    /**
     * 3. compare_distance_by_percent
     * <p>
     * Attributes
     * <p>
     * 설명 : 지정된 관절 사이의 길이를 percent 기준으로 비교한다. 현재 시점의 관절일 수 있고, entry_sequence 통과 시점의 좌표일 수 있다.
     * <p>
     * 1) name : compare_distance_by_percent
     * <p>
     * 2) check_position : x(너비) / y(높이) / z(깊이)
     * <p>
     * 3) skeleton_a, skeleton_b : source skeleton. 현재 시점의 비교할 관절 번호, nuitrack에서 정의 된 Joint enum 상수 값에 따른다.
     * <p>
     * 4) comparison_entry_sequence : 'entry_sequence'을 종료했을 때의 좌표를 참조할 지 여부. true일 경우 아래 5)의 관절 좌표는 entry_sequence의 종료했을 때 관절 좌표를 사용한다.  "true/false"
     * <p>
     * 5) comparison_skeleton_a, comparison_skeleton_b : target skeleton. 비교할 관절의 번호, nuitrack에서 정의 된 Joint enum 상수 값에 따른다.
     * <p>
     * 6) comparison_formula : 비교할 방식, smaller / bigger / equal
     * <p>
     * 7) comparison_value : 비교할 값, 0 ~ 200(%) (comparison_formula가 smaller 또는 bigger일 경우)
     * <p>
     * 8) comparison_threshold : 임계점, 값이 동등한지 비교할 때 인정 허용 범위. +-'값' percent 기준 (comparison_formula가 equal일 경우)
     * <p>
     * 9) fail_description : 해당 'checkmethod'가 'successcondition'에서 사용되었을 때, 화면에 표시할 정보
     */

    public boolean compare_distance_by_percent(CheckMethod body, Vector3[] bodyPos, Vector3[] entryPos) {

        boolean isPass = false;

        float threshold = 0;
        float value = 0;

        float distance = 0;
        float compare_distance = 0;

        float compare_threshold = 0;
        float compare_value = 0;

        if (body.comparison_formula.equalsIgnoreCase("equal")) {
            compare_threshold = body.comparison_threshold;
        } else {
            compare_value = body.comparison_value;
        }


        if (body.check_position.equalsIgnoreCase("x")) {
            distance = Math.abs(bodyPos[body.skeleton_a].x - bodyPos[body.skeleton_b].x);

            compare_distance = Math.abs(entryPos[body.comparison_skeleton_a].x - entryPos[body.comparison_skeleton_b].x);

            threshold = distance * 100 / compare_distance;


            Log.d("Match", body.skeleton_a + " skeleton_a = " + bodyPos[body.skeleton_a].x);
            Log.d("Match", body.skeleton_b + " skeleton_b = " + bodyPos[body.skeleton_b].x);
            Log.d("Match", body.comparison_skeleton_a + " comparison_skeleton_a = " + entryPos[body.comparison_skeleton_a].x);
            Log.d("Match", body.comparison_skeleton_b + " comparison_skeleton_b = " + entryPos[body.comparison_skeleton_b].x);

        } else if (body.check_position.equalsIgnoreCase("y")) {
            distance = Math.abs(bodyPos[body.skeleton_a].y - bodyPos[body.skeleton_b].y);

            compare_distance = Math.abs(entryPos[body.comparison_skeleton_a].y - entryPos[body.comparison_skeleton_b].y);

            threshold = distance * 100 / compare_distance;

            Log.d("Match", body.skeleton_a + " skeleton_a = " + bodyPos[body.skeleton_a].y);
            Log.d("Match", body.skeleton_b + " skeleton_b = " + bodyPos[body.skeleton_b].y);
            Log.d("Match", body.comparison_skeleton_a + " comparison_skeleton_a = " + entryPos[body.comparison_skeleton_a].y);
            Log.d("Match", body.comparison_skeleton_b + " comparison_skeleton_b = " + entryPos[body.comparison_skeleton_b].y);

        } else if (body.check_position.equalsIgnoreCase("z")) {
            distance = Math.abs(bodyPos[body.skeleton_a].z - bodyPos[body.skeleton_b].z);

            compare_distance = Math.abs(entryPos[body.comparison_skeleton_a].z - entryPos[body.comparison_skeleton_b].z);

            threshold = distance * 100 / compare_distance;


            Log.d("Match", body.skeleton_a + " skeleton_a = " + bodyPos[body.skeleton_a].z);
            Log.d("Match", body.skeleton_b + " skeleton_b = " + bodyPos[body.skeleton_b].z);
            Log.d("Match", body.comparison_skeleton_a + " comparison_skeleton_a = " + entryPos[body.comparison_skeleton_a].z);
            Log.d("Match", body.comparison_skeleton_b + " comparison_skeleton_b = " + entryPos[body.comparison_skeleton_b].z);

        }


        Log.d("Match", "distance = " + distance);
        Log.d("Match", "compare_distance = " + compare_distance);
        Log.d("Match", "compare_threshold =" + compare_threshold);
        Log.d("Match", "compare_value =" + compare_value);
        Log.d("Match", "threshold = " + threshold);
        Log.d("Match","fomula = " + body.comparison_formula);


        //viewMaxValue.setText("" + threshold);

        if (body.comparison_entry_sequence == true) {
            // entry position 과 비교
            if (body.comparison_formula.equalsIgnoreCase("smaller")) {
                if (threshold < compare_value) {
                    isPass = true;
                } else {
                    isPass = false;
                }

            } else if (body.comparison_formula.equalsIgnoreCase("bigger")) {
                if (threshold > compare_value) {
                    isPass = true;
                } else {
                    isPass = false;
                }
            } else if (body.comparison_formula.equalsIgnoreCase("equal")) {
                Log.d("CompareDistance", "Equal Value " + Math.abs((100 - threshold)));

                if (Math.abs((100 - threshold)) <= compare_threshold) {


                    isPass = true;
                } else {
                    isPass = false;
                }

            } else {
                isPass = false;
            }

        } else {
            // 라스트 포지션과 비교
            if (body.comparison_formula.equalsIgnoreCase("smaller")) {
                if (threshold < compare_value) {
                    isPass = true;
                } else {
                    isPass = false;
                }

            } else if (body.comparison_formula.equalsIgnoreCase("bigger")) {
                if (threshold > compare_value) {
                    isPass = true;
                } else {
                    isPass = false;
                }
            } else if (body.comparison_formula.equalsIgnoreCase("equal")) {
                Log.d("CompareDistance", "Equal Value " + Math.abs((100 - threshold)));

                if (Math.abs((100 - threshold)) <= compare_threshold) {


                    isPass = true;
                } else {
                    isPass = false;
                }

            } else {
                isPass = false;
            }

        }

        Log.d("MatchDistance" , "isPASS = " + isPass);

        return isPass;
    }

    /*-- START : Simulator --*/
    void showFileNamePopup() {
        mView = getLayoutInflater().inflate(R.layout.simulator_file_name_popup, null);
        mEditText = (EditText) mView.findViewById(R.id.file_name_edit);
        Date dt = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyMMdd_HHmmss");
        mEditText.setText(dateFormat.format(dt).toString());
        AlertDialog.Builder mAlertDialog = new AlertDialog.Builder(this);
        mAlertDialog.setTitle("set file name");
        mAlertDialog.setView(mView);
        mAlertDialog.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                filename = mEditText.getText().toString();
            }
        });
        mAlertDialog.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                filename = null;
            }
        });
        mAlertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface arg0) {

                MotionTester motionTester = new MotionTester();
                if (filename == null) {
                    Toast.makeText(mContext, "storing cancled", Toast.LENGTH_LONG).show();
                    mJointList.clear();
                    mJointTime.clear();
                    SharedPreferences pref = getSharedPreferences(KEY_SIMULATOR, Context.MODE_PRIVATE);
                    mMotionRecordMode = pref.getInt(KEY_XML_STORING_MODE, 0);
                } else {
                    motionTester.openXmlForStoring(filename);
                    Toast.makeText(mContext, "storing as [" + filename + ".xml]", Toast.LENGTH_LONG).show();
                    for (int i=0; i<mJointList.size(); i++) {
                        motionTester.writeJointDataToXml(mJointList.get(i), mJointTime.get(i));
                    }
                    motionTester.closeXmlForStoring();
                    Toast.makeText(mContext, "successfully stored", Toast.LENGTH_LONG).show();
                    SharedPreferences pref = getSharedPreferences(KEY_SIMULATOR, Context.MODE_PRIVATE);
                    mMotionRecordMode = pref.getInt(KEY_XML_STORING_MODE, 0);
                }


            }
        });
        mAlertDialog.show();
    }
    /*-- END : Simulator --*/
}





