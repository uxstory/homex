package com.samsung.android.clab.homex;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.ImageView;

public class GolfBallView extends ImageView {

    Vector3[] UserPos;
    Context mContext;
    Bitmap ball;

    int mWidth, mHeight;
    int screenW, screenH;
    float ballX, ballY;

    public GolfBallView(Context context) {
        super(context);
        this.mContext = context;

        ball = BitmapFactory.decodeResource(getResources(), R.drawable.golf_ball);
    }

    public GolfBallView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;

        ball = BitmapFactory.decodeResource(getResources(), R.drawable.golf_ball);
    }

    public void setUserPos(Vector3[] userPos) {
        this.UserPos = userPos;
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        screenW = w;
        screenH = h;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        // in pixels
        mWidth = getWidth();
        mHeight = getHeight();

        if(UserPos != null && UserPos.length > 0)
        {
            ballX = UserPos[2].x*2960;
            ballY = UserPos[18].y*1440;

            Log.d("GOLF", "ballX : " + ballX + " ballY : " + ballY);

            canvas.drawBitmap(ball, ballX, ballY, null);
        }
    }
}
