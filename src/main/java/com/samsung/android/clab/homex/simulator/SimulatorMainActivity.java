package com.samsung.android.clab.homex.simulator;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.samsung.android.clab.homex.R;
import com.samsung.android.clab.homex.UnityPlayerActivity;

public class SimulatorMainActivity extends AppCompatActivity {

    private static final String KEY_SIMULATOR = "simulator";
    private static final String KEY_XML_STORING_MODE = "xml_storing_mode_enabled";
    private static final int STORING_MODE_OFF = 0;
    private static final int STORING_MODE_ON = 1;
    private static final int STORING_MODE_ON_WITH_DELAY = 2;

    private Button mStartModeButton;
    private Button mStartModeDelayButton;
    private Button mStopModeButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.simulator_activity_main);
        SharedPreferences pref = getSharedPreferences(KEY_SIMULATOR, Context.MODE_PRIVATE);

        mStartModeButton = (Button) findViewById(R.id.start_mode);
        mStartModeDelayButton = (Button) findViewById(R.id.start_mode_delay);
        mStopModeButton = (Button) findViewById(R.id.stop_mode);
        int mode = pref.getInt(KEY_XML_STORING_MODE, 0);
        if (mode == STORING_MODE_OFF) {
            mStartModeButton.setEnabled(true);
            mStartModeDelayButton.setEnabled(true);
            mStopModeButton.setEnabled(false);
        } else if (mode == STORING_MODE_ON) {
            mStartModeButton.setEnabled(false);
            mStartModeDelayButton.setEnabled(true);
            mStopModeButton.setEnabled(true);
        } else {
            mStartModeButton.setEnabled(true);
            mStartModeDelayButton.setEnabled(false);
            mStopModeButton.setEnabled(true);
        }
    }

    public void startStoringMode(View view)
    {
        /*Intent home = new Intent(getApplicationContext(), UnityPlayerActivity.class);
        home.putExtra("MotionRecord", true);
        startActivity(home);

        finish();*/

        SharedPreferences pref = getSharedPreferences(KEY_SIMULATOR, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt(KEY_XML_STORING_MODE, STORING_MODE_ON);
        editor.commit();

        mStartModeButton.setEnabled(false);
        mStartModeDelayButton.setEnabled(true);
        mStopModeButton.setEnabled(true);
        Toast.makeText(this, R.string.started_xml_storing_mode, Toast.LENGTH_LONG).show();
    }


    public void startStoringModeWithDealy(View view)
    {
        SharedPreferences pref = getSharedPreferences(KEY_SIMULATOR, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt(KEY_XML_STORING_MODE, STORING_MODE_ON_WITH_DELAY);
        editor.commit();

        mStartModeButton.setEnabled(true);
        mStartModeDelayButton.setEnabled(false);
        mStopModeButton.setEnabled(true);
        Toast.makeText(this, R.string.started_xml_storing_mode, Toast.LENGTH_LONG).show();
    }

    public void stopStoringMode(View view)
    {
        SharedPreferences pref = getSharedPreferences(KEY_SIMULATOR, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt(KEY_XML_STORING_MODE, STORING_MODE_OFF);
        editor.commit();

        mStartModeButton.setEnabled(true);
        mStartModeDelayButton.setEnabled(true);
        mStopModeButton.setEnabled(false);
        Toast.makeText(this, R.string.stopped_xml_storing_mode, Toast.LENGTH_LONG).show();
    }

    public void startTester(View view)
    {
        int permissioninfo = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permissioninfo == PackageManager.PERMISSION_GRANTED) {
            Intent home = new Intent(getApplicationContext(), StoredListActivity.class);
            //set fixed name (not used)
            //Intent home = new Intent(getApplicationContext(), TesterActivity.class);
            startActivity(home);
        } else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                Toast.makeText(getApplicationContext(), "need WRITE_EXTERNAL_STORAGE permission", Toast.LENGTH_SHORT).show();
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 100);
            }
        }
    }
}
