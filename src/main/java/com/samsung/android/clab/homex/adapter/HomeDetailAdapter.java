package com.samsung.android.clab.homex.adapter;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import com.samsung.android.clab.homex.fragment.HomeDetailFragment;
import com.samsung.android.clab.homex.fragment.HomeDetailFragment.Listener;

import java.util.ArrayList;

public class HomeDetailAdapter extends FragmentStatePagerAdapter {

    private Listener mListener;
    private ArrayList<Integer> pageIndex = new ArrayList<>();

    public HomeDetailAdapter(FragmentManager fm, Listener listener) {
        super(fm);
        this.mListener = listener;

        for(int i = 0 ; i < 5 ; i++) {
            pageIndex.add(i);
        }
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        Object obj = super.instantiateItem(container, position);
        return obj;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        if (object != null) {
            return ((Fragment) object).getView() == view;
        } else {
            return false;
        }
    }

    @Override
    public int getItemPosition(Object object) {
        // Causes adapter to reload all Fragments when
        // notifyDataSetChanged is called
        return POSITION_NONE;
    }

    @Override
    public Fragment getItem(int position) {
        Integer index = pageIndex.get(position);
        HomeDetailFragment fragment = HomeDetailFragment.newInstance(index);
        fragment.setListener(mListener);
        return fragment;
    }

    @Override
    public int getCount() {
        return pageIndex.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return "";
    }

    public void removePage(int position) {
        pageIndex.remove(position);
        notifyDataSetChanged();
    }
}
