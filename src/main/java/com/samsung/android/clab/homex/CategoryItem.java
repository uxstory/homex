package com.samsung.android.clab.homex;

public class CategoryItem {
    public int resId;
    public String title;
    public String level;

    public CategoryItem(int resId, String title, String level) {
        this.resId = resId;
        this.title = title;
        this.level = level;
    }

    public int getResId() {
        return resId;
    }

    public void setResId(int resId) {
        this.resId = resId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }
}
