package com.samsung.android.clab.homex.activity;

import android.content.Context;
import android.content.Intent;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.samsung.android.clab.homex.UnityPlayerActivity;

import java.util.HashMap;
import java.util.Iterator;

public class SplashActivity extends AppCompatActivity {
    public static final String ORBBEC_PRODUCT = "ORBBEC Depth Sensor";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        UsbManager manager = (UsbManager) getSystemService(Context.USB_SERVICE);
        HashMap<String, UsbDevice> deviceList = manager.getDeviceList();
        Iterator<UsbDevice> deviceIterator = deviceList.values().iterator();

        while(deviceIterator.hasNext()){
            UsbDevice device = deviceIterator.next();
            if(ORBBEC_PRODUCT.equals(device.getProductName()) ) {
                startActivity(new Intent(getApplicationContext(), UnityPlayerActivity.class));
                finish();
                return;
            }
        }

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(SplashActivity.this, MainActivity.class));
                finish();
            }
        }, 500);
    }
}
