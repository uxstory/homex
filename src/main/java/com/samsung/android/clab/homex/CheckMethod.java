package com.samsung.android.clab.homex;


import android.util.Log;

import java.lang.Math;

public class CheckMethod {

    public String name;

    public String check_position;
    public String comparison_formula;

    public int skeleton_a;
    public int comparison_skeleton_a;

    public int skeleton_b;
    public int comparison_skeleton_b;

    public String fail_description;

    public String fail_golf_description = " ";

    public String getFail_golf_description() {
        return fail_golf_description;
    }

    public void setFail_golf_description(String fail_golf_description) {
        this.fail_golf_description = fail_golf_description;
    }

    public boolean comparison_entry_sequence;
    public float comparison_value;

    public float comparison_threshold;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getSkeleton_b() {
        return skeleton_b;
    }

    public void setSkeleton_b(String skeleton_b) {
        if(skeleton_b == null)
        {
            this.skeleton_b = 0;
        }
        else
        {
            this.skeleton_b = Integer.parseInt(skeleton_b);
        }

    }

    public float getComparison_skeleton_b() {
        return comparison_skeleton_b;
    }

    public void setComparison_skeleton_b(String comparison_skeleton_b) {
        if(comparison_skeleton_b == null)
        {
            this.comparison_skeleton_b = 0;
        }
        else
        {
            this.comparison_skeleton_b = Integer.parseInt(comparison_skeleton_b);
        }
    }

    public String getFail_description() {
        return fail_description;
    }

    public void setFail_description(String fail_description) {
        this.fail_description = fail_description;
    }

    public boolean isComparison_entry_sequence() {
        return comparison_entry_sequence;
    }

    public void setComparison_entry_sequence(String comparison_entry_sequence) {

        if(comparison_entry_sequence == null)
        {
            this.comparison_entry_sequence = false;
        }
        else
        {
            this.comparison_entry_sequence = Boolean.parseBoolean(comparison_entry_sequence);
        }
    }

    public float getComparison_value() {
        return comparison_value;
    }

    public void setComparison_value(float comparison_value) {
        this.comparison_value = comparison_value;
    }

    public String getCheck_position() {
        return check_position;
    }

    public void setCheck_position(String check_position) {
        this.check_position = check_position;
    }

    public float getSkeleton_a() {
        return skeleton_a;
    }

    public void setSkeleton_a(String skeleton_a) {
        if(skeleton_a == null)
        {
            this.skeleton_a = 0;
        }
        else
        {
            this.skeleton_a = Integer.parseInt(skeleton_a);
        }

    }

    public int getComparison_skeleton_a() {
        return comparison_skeleton_a;
    }

    public void setComparison_skeleton_a(String comparison_skeleton_a) {

        if(comparison_skeleton_a == null)
        {
            this.comparison_skeleton_a = 0;
        }
        else
        {
            this.comparison_skeleton_a = Integer.parseInt(comparison_skeleton_a);
        }
    }

    public String getComparison_formula() {
        return comparison_formula;
    }

    public void setComparison_formula(String comparison_formula) {
        this.comparison_formula = comparison_formula;
    }

    public float getComparison_threshold() {
        return comparison_threshold;
    }

    public void setComparison_threshold(float comparison_threshold) {
        this.comparison_threshold = comparison_threshold;
    }


    @Override
    public String toString()
    {
        return "name : "  + name +"\n"

                + "fail_description : " + fail_description +"\n"

                + "check_position : " + check_position + "\n"


                + "skeleton_a : " + skeleton_a+ "\n"
                + "skeleton_b : " + skeleton_b+ "\n"
                + "comparison_skeleton_a : " + comparison_skeleton_a + "\n"
                + "comparison_skeleton_b : " + comparison_skeleton_b + "\n"

                +"comparison_entry_sequence : " + comparison_entry_sequence +"\n"
                + "comparison_formula : " + comparison_formula +"\n"
                + "comparison_threshold : " + comparison_threshold +"\n"
                + "comparison_value : " + comparison_value;

    }

}