package com.samsung.android.clab.homex.dialog;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatDialog;
import android.view.Window;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.samsung.android.clab.homex.Constants;
import com.samsung.android.clab.homex.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.content.Context.MODE_PRIVATE;

public class ShowResultTimeDialog extends AppCompatDialog {

    @BindView(R.id.showTime) RadioGroup radioGroupShowTime;
    @BindView(R.id.fifteenSec) RadioButton fifteenSec;
    @BindView(R.id.thirtySec) RadioButton thirtySec;
    @BindView(R.id.oneMin) RadioButton oneMin;

    private Context mContext;
    private OnShowTimeListener mListener;

    private String showTime;

    public interface OnShowTimeListener {
        void onShowTime(String sec);
    }

    private RadioGroup.OnCheckedChangeListener showTimeOnCheckedChangeListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            if(checkedId == R.id.fifteenSec) {
                showTime = "15";
            } else if(checkedId == R.id.thirtySec) {
                showTime = "30";
            } else if(checkedId == R.id.oneMin) {
                showTime = "60";
            }

            setPreference(Constants.PREF_KEY_RESULT_SHOW_TIME, showTime);
        }
    };

    public ShowResultTimeDialog(Context context, OnShowTimeListener listener) {
        super(context);

        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.dialog_show_result_time);
        ButterKnife.bind(this);

        this.mContext = context;
        this.mListener = listener;

        radioGroupShowTime.setOnCheckedChangeListener(showTimeOnCheckedChangeListener);

        showTime = getPreferenceString(Constants.PREF_KEY_RESULT_SHOW_TIME);
        if(showTime.equals("15")) {
            fifteenSec.setChecked(true);
        } else if(showTime.equals("30")) {
            thirtySec.setChecked(true);
        } else if(showTime.equals("60")) {
            oneMin.setChecked(true);
        } else {
            fifteenSec.setChecked(true);
        }
    }

    @OnClick({R.id.buttonConfirm})
    public void onClick() {
        mListener.onShowTime(showTime);
        dismiss();
    }

    /**
     * 데이터를 저장한다.
     * @param key
     * @param value
     */
    private void setPreference(String key, String value) {
        SharedPreferences pref = mContext.getSharedPreferences(Constants.PREFERENCE, MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(key, value);
        editor.commit();
    }

    /**
     * 데이터를 가져오는 함수
     * @param key
     * @return
     */
    public String getPreferenceString(String key){
        SharedPreferences pref = mContext.getSharedPreferences(Constants.PREFERENCE, MODE_PRIVATE);
        return pref.getString(key, "");
    }
}
