package com.samsung.android.clab.homex;


import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class GuideAdapter extends BaseAdapter {

    private ArrayList<GuideView> guideViewArrayList = new ArrayList<GuideView>();

    int index = 0;

    public GuideAdapter(){


    }

    public void clearData() {
        // clear the data
        guideViewArrayList.clear();
    }


    @Override
    public int getCount() {
        return guideViewArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return guideViewArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final int pos = position;
        final Context context = parent.getContext();

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.guidelist_item, parent, false);
        }

        ImageView iconImageView = (ImageView) convertView.findViewById(R.id.icon_guide) ;
        TextView titleTextView = (TextView) convertView.findViewById(R.id.title_guide) ;

        GuideView guideView = guideViewArrayList.get(position);

        // 아이템 내 각 위젯에 데이터 반영
        iconImageView.setImageDrawable(guideView.getGuideIcon());
        titleTextView.setText(guideView.getGuideTitle());

        if(index == position) {
            iconImageView.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_guide));
            titleTextView.setTextColor(Color.parseColor("#00e2ff"));
        }
        else if(position > index)
        {
            iconImageView.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_guide_02));
            titleTextView.setTextColor(Color.parseColor("#e5e8ed"));
        }
        else if(position < index)
        {
            iconImageView.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_guide_02));
            titleTextView.setTextColor(Color.parseColor("#e5e8ed"));
        }
        else
        {

        }


        return convertView;

    }

    public void addItem(Drawable icon, String title) {
        GuideView item = new GuideView();

        item.setGuideIcon(icon);
        item.setGuideTitle(title);

        guideViewArrayList.add(item);
    }


}