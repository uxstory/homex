package com.samsung.android.clab.homex.activity;

import android.content.Intent;
import android.content.res.XmlResourceParser;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.samsung.android.clab.homex.Category;
import com.samsung.android.clab.homex.Course;
import com.samsung.android.clab.homex.Program;
import com.samsung.android.clab.homex.R;
import com.samsung.android.clab.homex.Workout;
import com.samsung.android.clab.homex.adapter.ContentsAdapter;

import java.util.ArrayList;

public class CategoryMoreActivity extends AppCompatActivity implements CategoryMoreAdapter.AdapterListener {

    private RecyclerView mRecyclerView;
    private CategoryMoreAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ImageView buttonBack;

    private ArrayList<Category> categoryList = new ArrayList<>();
    private Category category;

    private ArrayList<Program> programList;
    private Program program;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_more);

        mRecyclerView = findViewById(R.id.list_category);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        buttonBack = findViewById(R.id.btn_backcategory);
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        // 카테고리 파싱
        readCategory(getResources().getXml(R.xml.categories));

        mAdapter = new CategoryMoreAdapter(this, categoryList);
        mAdapter.setAdapterListener(this);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onItemClick(Category item, int position) {
        if(item.getReady().equals("true")) {
            int resId = getResources().getIdentifier(item.getContents(), "xml", getPackageName());
            ArrayList<Program> programList = readProgram(getResources().getXml(resId));

            Intent intent = new Intent(this, CategoryActivity.class);
            intent.putParcelableArrayListExtra("program", programList);
            intent.putExtra("title", item.getTitle());
            intent.putExtra("description", item.getDesc());
            startActivity(intent);
        } else {
            Toast.makeText(this, "준비 중입니다...", Toast.LENGTH_SHORT).show();
        }
    }

    public void readCategory(XmlResourceParser parser)
    {
        Log.d("START", "read Program Start");
        try {
            int eventType = parser.getEventType();
            boolean isEnd = false;

            while (eventType != XmlResourceParser.END_DOCUMENT && isEnd == false) {
                String tagname;

                switch(eventType)
                {
                    case XmlResourceParser.START_TAG:
                        tagname = parser.getName();

                        if(tagname != null)
                        {
                            Log.d("tagName", tagname);
                        }

                        if(tagname.equalsIgnoreCase("param"))
                        {
                            String paramName = parser.getAttributeValue(0);

                            if(paramName.equalsIgnoreCase("title"))
                            {
                                category.setTitle(parser.getAttributeValue(null, "value"));
                            }
                            else if(paramName.equalsIgnoreCase("desc"))
                            {
                                category.setDesc(parser.getAttributeValue(null, "value"));
                            }
                            else if(paramName.equalsIgnoreCase("icon"))
                            {
                                category.setIcon(parser.getAttributeValue(null, "value"));
                            }
                            else if(paramName.equalsIgnoreCase("contents"))
                            {
                                category.setContents(parser.getAttributeValue(null, "value"));
                            }
                            else if(paramName.equalsIgnoreCase("ready"))
                            {
                                category.setReady(parser.getAttributeValue(null, "value"));
                            }
                        }
                        else if(tagname.equalsIgnoreCase("item"))
                        {
                            category = new Category();
                        }

                        break;

                    case XmlResourceParser.TEXT:
                        break;

                    case XmlResourceParser.END_TAG:
                        tagname = parser.getName();

                        if(tagname.equalsIgnoreCase("categories"))
                        {
                            isEnd = true;
                        }
                        else if(tagname.equalsIgnoreCase("item"))
                        {

                            if(category != null)
                            {
                                categoryList.add(category);
                            }
                        }
                        break;

                    default:
                        break;
                }
                eventType = parser.next();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public ArrayList<Program> readProgram(XmlResourceParser parser)
    {
        Log.d("START", "read Program Start");
        try {
            int eventType = parser.getEventType();
            boolean isEnd = false;

            while (eventType != XmlResourceParser.END_DOCUMENT && isEnd == false) {
                String tagname;

                switch(eventType)
                {
                    case XmlResourceParser.START_TAG:
                        tagname = parser.getName();

                        if(tagname != null)
                        {
                            Log.d("tagName", tagname);
                        }

                        if(tagname.equalsIgnoreCase("contents"))
                        {
                            programList = new ArrayList<>();
                        }

                        else if(tagname.equalsIgnoreCase("param"))
                        {
                            String paramName = parser.getAttributeValue(0);

                            if(paramName.equalsIgnoreCase("title"))
                            {
                                program.setTitle(parser.getAttributeValue(null, "value"));
                            }
                            else if(paramName.equalsIgnoreCase("desc"))
                            {
                                program.setDesc(parser.getAttributeValue(null, "value"));
                            }
                            else if(paramName.equalsIgnoreCase("level"))
                            {
                                program.setLevel(parser.getAttributeValue(null, "value"));
                            }
                            else if(paramName.equalsIgnoreCase("steps"))
                            {
                                program.setSteps(parser.getAttributeValue(null, "value"));
                            }
                            else if(paramName.equalsIgnoreCase("icon"))
                            {
                                program.setIcon(parser.getAttributeValue(null, "value"));
                            }
                            else if(paramName.equalsIgnoreCase("course"))
                            {
                                program.setCourse(parser.getAttributeValue(null, "value"));
                            }

                        }
                        else if(tagname.equalsIgnoreCase("item"))
                        {
                            program = new Program();
                        }

                        break;

                    case XmlResourceParser.TEXT:
                        break;

                    case XmlResourceParser.END_TAG:
                        tagname = parser.getName();

                        if(tagname.equalsIgnoreCase("contents"))
                        {
                            isEnd = true;
                        }
                        else if(tagname.equalsIgnoreCase("item"))
                        {

                            if(program != null)
                            {
                                programList.add(program);
                            }
                        }
                        break;

                    default:
                        break;
                }
                eventType = parser.next();
            }

            return programList;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }
}
