package com.samsung.android.clab.homex.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.samsung.android.clab.homex.HistoryData;
import com.samsung.android.clab.homex.HistoryDetailData;
import com.samsung.android.clab.homex.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.ItemHolder> {

    private ArrayList<HistoryData> mItems;
    private Context mContext;
    private Listener mListener;

    public HomeAdapter(Context context, Listener listener) {
        this.mContext = context;
        this.mListener = listener;
    }

    @NonNull
    @Override
    public ItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ItemHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.home_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ItemHolder itemHolder, int positioin) {
        HistoryData item = mItems.get(positioin);

        itemHolder.textViewMonth.setText(item.getYearMonth());

        itemHolder.layoutHistoryContainer.removeAllViews();
        for(int i = 0 ; i < item.getHistoryDetailData().size() ; i++) {
            HistoryHolder holder = new HistoryHolder(mContext, itemHolder.layoutHistoryContainer);
            HistoryDetailData data = item.getHistoryDetailData().get(i);

            StringBuilder sb = new StringBuilder(data.getDate());
            sb.append(",").append(data.getDayOfWeek());
            holder.textViewDate.setText(sb.toString());

            if(data.getExerciseTime() > 60) {
                int hour = data.getExerciseTime() / 60;
                int mins = data.getExerciseTime() % 60;

                StringBuilder sb3 = new StringBuilder();
                sb3.append(hour).append("시간");
                sb3.append(mins).append("분");
                holder.textViewExerciseTime.setText(sb3.toString());
            } else {
                StringBuilder sb2 = new StringBuilder();
                sb2.append(data.getExerciseTime());
                sb2.append("분");
                holder.textViewExerciseTime.setText(sb2.toString());
            }

            int[] heartRate = data.getHeartRate();
            StringBuilder sb4 = new StringBuilder();
            sb4.append(heartRate[0]).append("/");
            sb4.append(heartRate[1]).append("/");
            sb4.append(heartRate[2]);
            holder.textViewHearRate.setText(sb4.toString());

            StringBuilder sb1 = new StringBuilder();
            sb1.append(data.getKcal());
            sb1.append("kcal 소모");
            holder.textViewKcal.setText(sb1.toString());

            holder.textViewProgram.setText(data.getProgram());

            itemHolder.layoutHistoryContainer.addView(holder.rootView);
        }

        itemHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mListener != null) {
                    mListener.onItemClicked(positioin);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public void setItems(ArrayList<HistoryData> items) {
        this.mItems = items;
    }

    class ItemHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewMonth) TextView textViewMonth;
        @BindView(R.id.layoutHistoryContainer) LinearLayout layoutHistoryContainer;

        public ItemHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    class HistoryHolder {
        @BindView(R.id.textViewDate) TextView textViewDate;
        @BindView(R.id.textViewProgram) TextView textViewProgram;
        @BindView(R.id.textViewExerciseTime) TextView textViewExerciseTime;
        @BindView(R.id.textViewHearRate) TextView textViewHearRate;
        @BindView(R.id.textViewKcal) TextView textViewKcal;

        private CardView rootView;

        public HistoryHolder(Context context, ViewGroup root) {
            rootView = (CardView) LayoutInflater.from(context).inflate(R.layout.layout_history_item, root, false);
            ButterKnife.bind(this, rootView);
        }
    }

    public interface Listener {
        void onItemClicked(int position);
    }
}
