package com.samsung.android.clab.homex.dialog;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatDialog;
import android.view.Window;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.samsung.android.clab.homex.Constants;
import com.samsung.android.clab.homex.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.content.Context.MODE_PRIVATE;

public class QuitTimeDialog extends AppCompatDialog {

    @BindView(R.id.quitTime) RadioGroup radioGroupQuitTime;
    @BindView(R.id.threeMin) RadioButton threeMins;
    @BindView(R.id.fiveMin) RadioButton fiveMins;
    @BindView(R.id.tenMin) RadioButton tenMins;

    private Context mContext;
    private OnQuitTimeListener mListener;

    private String quitTime;

    public interface OnQuitTimeListener {
        void onQuitTime(String min);
    }

    private RadioGroup.OnCheckedChangeListener quitTimeOnCheckedChangeListener = (group, checkedId) -> {
        if(checkedId == R.id.threeMin) {
            quitTime = "3";
        } else if(checkedId == R.id.fiveMin) {
            quitTime = "5";
        } else if(checkedId == R.id.tenMin) {
            quitTime = "10";
        }

        setPreference(Constants.PREF_KEY_QUIT_TIME, quitTime);
    };

    public QuitTimeDialog(Context context, OnQuitTimeListener listener) {
        super(context);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.dialog_quit_time);
        ButterKnife.bind(this);

        this.mContext = context;
        this.mListener = listener;

        radioGroupQuitTime.setOnCheckedChangeListener(quitTimeOnCheckedChangeListener);

        quitTime = getPreferenceString(Constants.PREF_KEY_QUIT_TIME);
        if(quitTime.equals("3")) {
            threeMins.setChecked(true);
        } else if(quitTime.equals("5")) {
            fiveMins.setChecked(true);
        } else if(quitTime.equals("10")) {
            tenMins.setChecked(true);
        } else {
            threeMins.setChecked(true);
        }
    }

    @OnClick(R.id.buttonConfirm)
    public void onClick() {
        mListener.onQuitTime(quitTime);
        dismiss();
    }

    /**
     * 데이터를 저장한다.
     * @param key
     * @param value
     */
    private void setPreference(String key, String value) {
        SharedPreferences pref = mContext.getSharedPreferences(Constants.PREFERENCE, MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(key, value);
        editor.commit();
    }

    /**
     * 데이터를 가져오는 함수
     * @param key
     * @return
     */
    public String getPreferenceString(String key){
        SharedPreferences pref = mContext.getSharedPreferences(Constants.PREFERENCE, MODE_PRIVATE);
        return pref.getString(key, "");
    }
}
