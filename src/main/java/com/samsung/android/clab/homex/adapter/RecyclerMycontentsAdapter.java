package com.samsung.android.clab.homex.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.samsung.android.clab.homex.Course;
import com.samsung.android.clab.homex.R;

import java.util.ArrayList;

public class RecyclerMycontentsAdapter extends RecyclerView.Adapter<RecyclerMycontentsAdapter.ViewHolder> {

    private ArrayList<Course> myData = null ;
    private AdapterListener mListener;
    private Context mContext;

    public interface AdapterListener {
        void onItemClick(Course item, int position);
    }

    public RecyclerMycontentsAdapter(Context context, ArrayList<Course> list, AdapterListener listener) {
        myData = list;
        this.mListener = listener;
        this.mContext = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        Context context = viewGroup.getContext() ;

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) ;

        View view = inflater.inflate(R.layout.layout_myitem, viewGroup, false) ;

        RecyclerMycontentsAdapter.ViewHolder vh = new RecyclerMycontentsAdapter.ViewHolder(view) ;

        return vh ;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

        Course item = myData.get(i) ;

        int resID = mContext.getResources().getIdentifier(item.getIcon(), "drawable", mContext.getPackageName());
        viewHolder.icon.setImageResource(resID);
        viewHolder.title.setText(item.getTitle());

        viewHolder.itemView.setOnClickListener(v -> {
            if(mListener != null) {
                mListener.onItemClick(item, i);
            }
        });
    }

    @Override
    public int getItemCount() {

        return myData.size();
    }


    // 아이템 뷰를 저장
    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView icon ;
        TextView title ;

        ViewHolder(View itemView) {
            super(itemView) ;

            // 뷰 객체에 대한 참조. (hold strong reference)
            icon = itemView.findViewById(R.id.icon_item) ;
            title = itemView.findViewById(R.id.text_itemname) ;
        }
    }

}
