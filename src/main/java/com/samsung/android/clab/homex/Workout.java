package com.samsung.android.clab.homex;

import java.io.Serializable;

public class Workout implements Serializable {

    private String title;
    private int repeat;
    private int heart_avg;
    private String icon;
    private String video;
    private String tracker;
    private String actionicon_active;
    private String actionicon_out;
    private int guideTime = 10;

    public String getActionicon_active() {
        return actionicon_active;
    }

    public void setActionicon_active(String actionicon_active) {
        this.actionicon_active = actionicon_active;
    }

    public String getActionicon_out() {
        return actionicon_out;
    }

    public void setActionicon_out(String actionicon_out) {
        this.actionicon_out = actionicon_out;
    }

    public int getGuideTime() { return guideTime; }
    public void setGuideTime(int time) { guideTime = time; }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getRepeat() {
        return repeat;
    }

    public void setRepeat(int repeat) {
        this.repeat = repeat;
    }

    public int getHerart_avg() { return heart_avg; }

    public void setHerart_avg(int herart_avg) {
        this.heart_avg = herart_avg;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getTracker() {
        return tracker;
    }

    public void setTracker(String tracker) {
        this.tracker = tracker;
    }


    @Override
    public String toString() {
        return "title : " + title + "\n"
                + "repeat : " + repeat + "\n"
                + "heart_avg : " + heart_avg + "\n"
                + "icon : " + icon + "\n"
                + "video : " + video + "\n"
                + "tracker : " + tracker;

    }
}
