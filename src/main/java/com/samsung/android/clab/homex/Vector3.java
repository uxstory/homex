package com.samsung.android.clab.homex;

import android.opengl.Matrix;

import android.support.graphics.drawable.VectorDrawableCompat;

public class Vector3 {

    public float x = 0;
    public float y = 0;
    public float z = 0;

    public Vector3()
    {

    }

    public Vector3(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Vector3(String v)
    {
        Set(v);

    }

    public void Set(String v) {

        String[] sArray = v.split(",");


        this.x = Float.parseFloat(sArray[0]);
        this.y = Float.parseFloat(sArray[1]);
        this.z = Float.parseFloat(sArray[2]);

        //Log.d("POS" , "v3.x" + v3.x + "v3.y : " +v3.y + "v3.z" + v3.z);
    }

    @Override
    public String toString() {
        return this.x + "," + this.y + "," + this.z;
    }
}
