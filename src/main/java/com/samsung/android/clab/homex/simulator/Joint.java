package com.samsung.android.clab.homex.simulator;

public class Joint {
    private static final String TAG = "MotionData";

    private int maxJoints = JointType.JOINT_TYPE_MAX.ordinal();

    public class Vector3 {
        float x;
        float y;
        float z;
    }

    //UX Story define
    static public String[] JointName = {"Head", "Neck", "Torso", "Waist",
            "LeftCollar", "LeftShoulder", "LeftElbow", "LeftWrist", "LeftHand", "LeftFingertip",
            "RightCollar","RightShoulder","RightElbow","RightWrist","RightHand", "RightFingertip",
            "LeftHip","LeftKnee","LeftAnkle","LeftFoot",
            "RightHip","RightKnee","RightAnkle","RightFoot"};

    public static int JOINT_SIZE = 16;

    private Vector3[] mJoints = new Vector3[JointName.length];

    public void setJoint(int index, float x, float y, float z) {
        if (mJoints[index] == null) {
            mJoints[index] = new Vector3();
            mJoints[index].x = x;
            mJoints[index].y = y;
            mJoints[index].z = z;
        }
    }

    public Vector3 getJointVector3(int index) {
        return mJoints[index];
    }

    public long mTime;


    //for Skeleton value fx[] (sg82.lee)
    // API for UX Story - July app version (Different coordinates)
    public float[] getConvertedValueX(float min, float max) {
        float[] fx = new float[maxJoints];

        fx[JointType.JOINT_NONE.ordinal()] = 0;
        for (int i=0; i < JointName.length; i++) {
            fx[i+1] = mJoints[i].x;
        }
        return fx;
    }

    //for Skeleton value fy[] (sg82.lee)
    // API for UX Story - July app version (Different coordinates)
    public float[] getConvertedValueY(float min, float max) {
        float[] fy = new float[maxJoints];

        fy[JointType.JOINT_NONE.ordinal()] = 0;
        for (int i=0; i < JointName.length; i++) {
            fy[i+1] = mJoints[i].y;
        }
        return fy;
    }

    public float[] getConvertedValueZ(float min, float max) {
        float[] fz = new float[maxJoints];

        fz[JointType.JOINT_NONE.ordinal()] = 0;
        for (int i=0; i < JointName.length; i++) {
            fz[i+1] = mJoints[i].z;
        }
        return fz;
    }

    /*//for Skeleton value fx[] (sg82.lee)
    // API for UX Story - July app version (Different coordinates)
    public float[] getConvertedValueX(float min, float max) {
        float[] fx = new float[maxJoints];

        fx[JointType.JOINT_NONE.ordinal()] = 0;
        fx[JointType.JOINT_HEAD.ordinal()] =            mJoints[0].x;
        fx[JointType.JOINT_NECK.ordinal()] =            mJoints[1].x;
        fx[JointType.JOINT_TORSO.ordinal()] =           mJoints[2].x;

        fx[JointType.JOINT_WAIST.ordinal()] =           mJoints[3].x;
        fx[JointType.JOINT_LEFT_COLLAR.ordinal()] =     mJoints[1].x;   //neck instead of collar
        fx[JointType.JOINT_LEFT_SHOULDER.ordinal()] =   mJoints[4].x;
        fx[JointType.JOINT_LEFT_ELBOW.ordinal()] =      mJoints[6].x;
        fx[JointType.JOINT_LEFT_WRIST.ordinal()] =      mJoints[8].x;
        fx[JointType.JOINT_LEFT_HAND.ordinal()] =       mJoints[8].x;   //left wrist instead of left hand
        fx[JointType.JOINT_LEFT_FINGERTIP.ordinal()] =  mJoints[8].x;   //left wrist instead of left fingertrip

        fx[JointType.JOINT_RIGHT_COLLAR.ordinal()] =    mJoints[1].x;   //neck instead of collar
        fx[JointType.JOINT_RIGHT_SHOULDER.ordinal()] =  mJoints[5].x;
        fx[JointType.JOINT_RIGHT_ELBOW.ordinal()] =     mJoints[7].x;
        fx[JointType.JOINT_RIGHT_WRIST.ordinal()] =     mJoints[9].x;
        fx[JointType.JOINT_RIGHT_HAND.ordinal()] =      mJoints[9].x;   //right wrist instead of right hand
        fx[JointType.JOINT_RIGHT_FINGERTIP.ordinal()] = mJoints[9].x;   //right wrist instead of left fingertrip

        fx[JointType.JOINT_LEFT_HIP.ordinal()] =        mJoints[10].x;
        fx[JointType.JOINT_LEFT_KNEE.ordinal()] =       mJoints[12].x;
        fx[JointType.JOINT_LEFT_ANKLE.ordinal()] =      mJoints[14].x;
        fx[JointType.JOINT_LEFT_FOOT.ordinal()] =       mJoints[14].x;   //left ankle instead of left foot

        fx[JointType.JOINT_RIGHT_HIP.ordinal()] =       mJoints[11].x;
        fx[JointType.JOINT_RIGHT_KNEE.ordinal()] =      mJoints[13].x;
        fx[JointType.JOINT_RIGHT_ANKLE.ordinal()] =     mJoints[15].x;
        fx[JointType.JOINT_RIGHT_FOOT.ordinal()] =      mJoints[15].x;   //right ankle instead of right foot

        for (int i=0; i<maxJoints; i++) {
            float minOffset = Math.abs(min) + 100;
            float maxOffset = Math.abs(max) + 100;
            float totalOffset = minOffset + maxOffset;


            fx[i] = fx[i] + minOffset;
            fx[i] = totalOffset - fx[i];
            fx[i] = fx[i] / totalOffset;;
            *//*fx[i] = fx[i] + minOffset;
            if (fx[i] < 0)
                fx[i] = 0;
            fx[i] = fx[i] / totalOffset;*//*
        }

        return fx;
    }

    //for Skeleton value fy[] (sg82.lee)
    // API for UX Story - July app version (Different coordinates)
    public float[] getConvertedValueY(float min, float max) {
        float[] fy = new float[maxJoints];

        fy[JointType.JOINT_NONE.ordinal()] = 0;
        fy[JointType.JOINT_HEAD.ordinal()] =            mJoints[0].y;
        fy[JointType.JOINT_NECK.ordinal()] =            mJoints[1].y;
        fy[JointType.JOINT_TORSO.ordinal()] =           mJoints[2].y;

        fy[JointType.JOINT_WAIST.ordinal()] =           mJoints[3].y;
        fy[JointType.JOINT_LEFT_COLLAR.ordinal()] =     mJoints[1].y;   //neck instead of collar
        fy[JointType.JOINT_LEFT_SHOULDER.ordinal()] =   mJoints[4].y;
        fy[JointType.JOINT_LEFT_ELBOW.ordinal()] =      mJoints[6].y;
        fy[JointType.JOINT_LEFT_WRIST.ordinal()] =      mJoints[8].y;
        fy[JointType.JOINT_LEFT_HAND.ordinal()] =       mJoints[8].y;   //left wrist instead of left hand
        fy[JointType.JOINT_LEFT_FINGERTIP.ordinal()] =  mJoints[8].y;   //left wrist instead of left fingertrip

        fy[JointType.JOINT_RIGHT_COLLAR.ordinal()] =    mJoints[1].y;   //neck instead of collar
        fy[JointType.JOINT_RIGHT_SHOULDER.ordinal()] =  mJoints[5].y;
        fy[JointType.JOINT_RIGHT_ELBOW.ordinal()] =     mJoints[7].y;
        fy[JointType.JOINT_RIGHT_WRIST.ordinal()] =     mJoints[9].y;
        fy[JointType.JOINT_RIGHT_HAND.ordinal()] =      mJoints[9].y;   //right wrist instead of right hand
        fy[JointType.JOINT_RIGHT_FINGERTIP.ordinal()] = mJoints[9].y;   //right wrist instead of left fingertrip

        fy[JointType.JOINT_LEFT_HIP.ordinal()] =        mJoints[10].y;
        fy[JointType.JOINT_LEFT_KNEE.ordinal()] =       mJoints[12].y;
        fy[JointType.JOINT_LEFT_ANKLE.ordinal()] =      mJoints[14].y;
        fy[JointType.JOINT_LEFT_FOOT.ordinal()] =       mJoints[14].y;   //left ankle instead of left foot

        fy[JointType.JOINT_RIGHT_HIP.ordinal()] =       mJoints[11].y;
        fy[JointType.JOINT_RIGHT_KNEE.ordinal()] =      mJoints[13].y;
        fy[JointType.JOINT_RIGHT_ANKLE.ordinal()] =     mJoints[15].y;
        fy[JointType.JOINT_RIGHT_FOOT.ordinal()] =      mJoints[15].y;   //right ankle instead of right foot

        for (int i=0; i<maxJoints; i++) {
            float minOffset = Math.abs(min) + 100;
            float maxOffset = Math.abs(min) + 100;
            float totalOffset = minOffset + maxOffset;

            fy[i] = fy[i] + minOffset;
            fy[i] = totalOffset - fy[i];
            fy[i] = fy[i] / totalOffset;;


            *//*fy[i] = fy[i] + minOffset;
            if (fy[i] < 0)
                fy[i] = 0;
            fy[i] = fy[i] / totalOffset;*//*
        }

        return fy;
    }*/
}
