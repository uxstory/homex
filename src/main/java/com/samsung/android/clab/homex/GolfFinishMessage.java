package com.samsung.android.clab.homex;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

public class GolfFinishMessage extends FrameLayout {

    TextView golfDescription;

    public GolfFinishMessage(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public GolfFinishMessage(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public GolfFinishMessage(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public GolfFinishMessage(@NonNull Context context, String messge) {
        super(context);
        init(context, messge);
    }

    private void init(Context context, String messge) {
        View view = LayoutInflater.from(context).inflate(R.layout.view_golf_description, null, false);

        golfDescription = view.findViewById(R.id.golfDescription);
        golfDescription.setText(messge);

        addView(view);
    }
}
