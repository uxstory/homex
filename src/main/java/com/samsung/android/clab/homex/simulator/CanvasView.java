package com.samsung.android.clab.homex.simulator;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class CanvasView extends View {
    public static String LOG_TAG = CanvasView.class.getSimpleName();
    public final boolean DISPLAY_POINT = true;

    int mWidth, mHeight;
    boolean mDrawSkeleton;
    Skeleton mSkeleton;
    long mTotalTime;
    long mCurrentTime;

    public CanvasView(Context context) {
        super(context);

        prepare();
    }

    public CanvasView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        prepare();
    }

    private void prepare() {
        mSkeleton = new Skeleton();
        mDrawSkeleton = true;

        Log.d(LOG_TAG, "CanvasView prepared.");
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        // in pixels
        mWidth = getWidth();
        mHeight = getHeight();

        drawSkeleton(canvas, true);
        drawTimeText(canvas);
    }

    private void drawTimeText(Canvas canvas) {
        Paint paint = new Paint();
        paint.setColor(Color.BLUE);
        paint.setTextSize(70);

        canvas.drawText(getTimeText(mCurrentTime) +  " / " + getTimeText(mTotalTime),
                100, 150,  paint);
    }

    private String getTimeText(long time) {
        SimpleDateFormat format = new SimpleDateFormat("mm:ss:SSS");
        return format.format(time);
    }

    private void drawLine(Canvas canvas, Paint paint, Skeleton.Skeletal start, Skeleton.Skeletal end) {
        canvas.drawLine(start.x, start.y, end.x, end.y, paint);
    }

    private void drawSkeletonLine(Canvas canvas, Paint paint) {
        drawLine(canvas, paint, mSkeleton.get(JointType.JOINT_HEAD), mSkeleton.get(JointType.JOINT_NECK));

        drawLine(canvas, paint, mSkeleton.get(JointType.JOINT_NECK), mSkeleton.get(JointType.JOINT_LEFT_COLLAR));
        drawLine(canvas, paint, mSkeleton.get(JointType.JOINT_LEFT_COLLAR), mSkeleton.get(JointType.JOINT_LEFT_SHOULDER));
        drawLine(canvas, paint, mSkeleton.get(JointType.JOINT_LEFT_SHOULDER), mSkeleton.get(JointType.JOINT_LEFT_ELBOW));
        drawLine(canvas, paint, mSkeleton.get(JointType.JOINT_LEFT_ELBOW), mSkeleton.get(JointType.JOINT_LEFT_WRIST));

        drawLine(canvas, paint, mSkeleton.get(JointType.JOINT_NECK), mSkeleton.get(JointType.JOINT_RIGHT_COLLAR));
        drawLine(canvas, paint, mSkeleton.get(JointType.JOINT_RIGHT_COLLAR), mSkeleton.get(JointType.JOINT_RIGHT_SHOULDER));
        drawLine(canvas, paint, mSkeleton.get(JointType.JOINT_RIGHT_SHOULDER), mSkeleton.get(JointType.JOINT_RIGHT_ELBOW));
        drawLine(canvas, paint, mSkeleton.get(JointType.JOINT_RIGHT_ELBOW), mSkeleton.get(JointType.JOINT_RIGHT_WRIST));

        drawLine(canvas, paint, mSkeleton.get(JointType.JOINT_LEFT_COLLAR), mSkeleton.get(JointType.JOINT_TORSO));
        drawLine(canvas, paint, mSkeleton.get(JointType.JOINT_RIGHT_COLLAR), mSkeleton.get(JointType.JOINT_TORSO));

        drawLine(canvas, paint, mSkeleton.get(JointType.JOINT_TORSO), mSkeleton.get(JointType.JOINT_WAIST));

        drawLine(canvas, paint, mSkeleton.get(JointType.JOINT_WAIST), mSkeleton.get(JointType.JOINT_LEFT_HIP));
        drawLine(canvas, paint, mSkeleton.get(JointType.JOINT_LEFT_HIP), mSkeleton.get(JointType.JOINT_LEFT_KNEE));
        drawLine(canvas, paint, mSkeleton.get(JointType.JOINT_LEFT_KNEE), mSkeleton.get(JointType.JOINT_LEFT_ANKLE));

        drawLine(canvas, paint, mSkeleton.get(JointType.JOINT_WAIST), mSkeleton.get(JointType.JOINT_RIGHT_HIP));
        drawLine(canvas, paint, mSkeleton.get(JointType.JOINT_RIGHT_HIP), mSkeleton.get(JointType.JOINT_RIGHT_KNEE));
        drawLine(canvas, paint, mSkeleton.get(JointType.JOINT_RIGHT_KNEE), mSkeleton.get(JointType.JOINT_RIGHT_ANKLE));
    }

    private void drawSkeleton(Canvas canvas, boolean withLine) {
        if (!mDrawSkeleton) {
            return;
        }

        Paint paint = new Paint();

        paint.setColor(Color.RED);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(10);

        List<String> displayTextListLeft = new ArrayList<>();
        List<String> displayTextListRight = new ArrayList<>();

        for (JointType jointType : JointType.values()) {
            if (jointType == JointType.JOINT_NONE || jointType == JointType.JOINT_TYPE_MAX) {
                continue;
            }
            Skeleton.Skeletal skeletal = mSkeleton.get(jointType);
            canvas.drawPoint(skeletal.x, skeletal.y, paint);
            if (DISPLAY_POINT == true) {
                Skeleton.SkeletalOrigin skeletalOrigin = mSkeleton.getOriginValue(jointType);
                if (jointType == JointType.JOINT_HEAD) {
                    displayTextListLeft.add("head(0) : " + skeletalOrigin.toString());
                }
                if (jointType == JointType.JOINT_NECK) {
                    displayTextListLeft.add("neck(1) : " + skeletalOrigin.toString());
                }
                else if (jointType == JointType.JOINT_TORSO) {
                    displayTextListLeft.add("torso(2) : " + skeletalOrigin.toString());
                    displayTextListLeft.add("* head(0)-torso(2) : " + mSkeleton.getOriginValueDistance(JointType.JOINT_HEAD, JointType.JOINT_TORSO));
                    displayTextListLeft.add("* neck(1)-torso(2) : " + mSkeleton.getOriginValueDistance(JointType.JOINT_NECK, JointType.JOINT_TORSO));
                } else if (jointType == JointType.JOINT_WAIST) {
                    displayTextListLeft.add("");
                    displayTextListLeft.add("waist(3) : " + skeletalOrigin.toString());
                    displayTextListLeft.add("* neck(1)-waist(3) : " + mSkeleton.getOriginValueDistance(JointType.JOINT_NECK, JointType.JOINT_WAIST));
                    displayTextListLeft.add("* torso(2)-waist(3) : " + mSkeleton.getOriginValueDistance(JointType.JOINT_TORSO, JointType.JOINT_WAIST));
                }

                if (jointType == JointType.JOINT_LEFT_COLLAR) {
                    displayTextListLeft.add("");
                    displayTextListLeft.add("Lcollar(4) : " + skeletalOrigin.toString());
                    displayTextListLeft.add("* Lcollar(4)-Rcollar(10) : " + mSkeleton.getOriginValueDistance(JointType.JOINT_LEFT_COLLAR, JointType.JOINT_RIGHT_COLLAR));
                }

                else if (jointType == JointType.JOINT_LEFT_SHOULDER) {
                    displayTextListLeft.add("");
                    displayTextListLeft.add("Lshoulder(5) : " + skeletalOrigin.toString());
                } else if (jointType == JointType.JOINT_LEFT_ELBOW) {
                    displayTextListLeft.add("Lelbow(6) : " + skeletalOrigin.toString());
                    displayTextListLeft.add("* torso(2)-Lelbow(6) : " + mSkeleton.getOriginValueDistance(JointType.JOINT_TORSO, JointType.JOINT_LEFT_ELBOW));
                } else if (jointType == JointType.JOINT_LEFT_WRIST) {
                    displayTextListLeft.add("Lwrist(7) : " + skeletalOrigin.toString());
                }

                else if (jointType == JointType.JOINT_RIGHT_SHOULDER) {
                    displayTextListLeft.add("");
                    displayTextListLeft.add("Rshoulder(11) : " + skeletalOrigin.toString());
                } else if (jointType == JointType.JOINT_RIGHT_ELBOW) {
                    displayTextListLeft.add("Relbow(12) : " + skeletalOrigin.toString());
                    displayTextListLeft.add("* torso(2)-Relbow(12) : " + mSkeleton.getOriginValueDistance(JointType.JOINT_TORSO, JointType.JOINT_RIGHT_ELBOW));
                } else if (jointType == JointType.JOINT_RIGHT_WRIST) {
                    displayTextListLeft.add("Rwrist(13) : " + skeletalOrigin.toString());
                    displayTextListLeft.add("* Lwrist(7)-Rwrist(13) : " + mSkeleton.getOriginValueDistance(JointType.JOINT_LEFT_WRIST, JointType.JOINT_RIGHT_WRIST));
                }

                else if (jointType == JointType.JOINT_LEFT_HIP) {
                    displayTextListRight.add("leftHip(16) : " + skeletalOrigin.toString());
                } else if (jointType == JointType.JOINT_LEFT_KNEE) {
                    displayTextListRight.add("leftKnee(17) : " + skeletalOrigin.toString());
                } else if (jointType == JointType.JOINT_LEFT_ANKLE) {
                    displayTextListRight.add("leftankle(18) : " + skeletalOrigin.toString());
                    displayTextListRight.add("* Lhip(16)-Lknee(17) : " + mSkeleton.getOriginValueDistance(JointType.JOINT_LEFT_HIP, JointType.JOINT_LEFT_KNEE));
                    displayTextListRight.add("* Lknee(17)-Lankle(18) : " + mSkeleton.getOriginValueDistance(JointType.JOINT_LEFT_KNEE, JointType.JOINT_LEFT_ANKLE));
                }

                else if (jointType == JointType.JOINT_RIGHT_HIP) {
                    displayTextListRight.add("");
                    displayTextListRight.add("rightHip(20) : " + skeletalOrigin.toString());
                    displayTextListRight.add("* leftHip(16)-rightHip(20) : " + mSkeleton.getOriginValueDistance(JointType.JOINT_LEFT_HIP, JointType.JOINT_RIGHT_HIP));
                } else if (jointType == JointType.JOINT_RIGHT_KNEE) {
                    displayTextListRight.add("rightKnee(21) : " + skeletalOrigin.toString());
                } else if (jointType == JointType.JOINT_RIGHT_ANKLE) {
                    displayTextListRight.add("rightAnkle(22) : " + skeletalOrigin.toString());
                    displayTextListRight.add("* Rhip(20)-Rknee(21) : " + mSkeleton.getOriginValueDistance(JointType.JOINT_RIGHT_HIP, JointType.JOINT_RIGHT_KNEE));
                    displayTextListRight.add("* Rknee(21)-Rankle(22) : " + mSkeleton.getOriginValueDistance(JointType.JOINT_RIGHT_KNEE, JointType.JOINT_RIGHT_ANKLE));
                    displayTextListRight.add("");
                    displayTextListRight.add("* Lankle(18)-Rankle(22) :" + mSkeleton.getOriginValueDistance(JointType.JOINT_RIGHT_ANKLE, JointType.JOINT_LEFT_ANKLE));
                }
            }
        }

        if (withLine) {
            paint.setStyle(Paint.Style.FILL);
            paint.setStrokeWidth(1);
            drawSkeletonLine(canvas, paint);
        }
        if (DISPLAY_POINT) {
            drawSkeletonPositionText(canvas, displayTextListLeft, displayTextListRight);
        }
    }

    private void drawSkeletonPositionText(Canvas canvas, List<String> displayTextListLeft, List<String> displayTextListRight) {
        Paint paint = new Paint();
        paint.setColor(Color.BLUE);
        paint.setTextSize(35);

        for (int i=0; i<displayTextListLeft.size(); i++ ) {
            canvas.drawText(displayTextListLeft.get(i),
                    100, 250+(i*40),  paint);
        }

        for (int i=0; i<displayTextListRight.size(); i++ ) {
            canvas.drawText(displayTextListRight.get(i),
                    1900, 250+(i*40),  paint);
        }

        canvas.drawText("Right(X-value is small) <",
                100, 1270,  paint);
        canvas.drawText("> Left(X-value is big)",
                510, 1270,  paint);
        canvas.drawText("^ Up(Y-value is small)",
                487, 1220,  paint);
        canvas.drawText("v  down(Y-value is big)",
                487, 1320,  paint);
    }

    public void updateSkeletonInformation(final float[] fx, final float[] fy, final float[] fz,
                                          final long totalTime, final long currentTime) {
        mSkeleton.setSkeleton(fx, fy, fz, mWidth, mHeight);
        mTotalTime = totalTime;
        mCurrentTime = currentTime;

        invalidate();
    }
}
