package com.samsung.android.clab.homex;

import android.os.Parcel;
import android.os.Parcelable;

public class Program implements Parcelable {
    private String title;
    private String desc;
    private String level;
    private String steps;
    private String icon;
    private String course;

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getSteps() {
        return steps;
    }

    public void setSteps(String steps) {
        this.steps = steps;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeString(this.desc);
        dest.writeString(this.level);
        dest.writeString(this.steps);
        dest.writeString(this.icon);
        dest.writeString(this.course);
    }

    public Program() {
    }

    protected Program(Parcel in) {
        this.title = in.readString();
        this.desc = in.readString();
        this.level = in.readString();
        this.steps = in.readString();
        this.icon = in.readString();
        this.course = in.readString();
    }

    public static final Parcelable.Creator<Program> CREATOR = new Parcelable.Creator<Program>() {
        @Override
        public Program createFromParcel(Parcel source) {
            return new Program(source);
        }

        @Override
        public Program[] newArray(int size) {
            return new Program[size];
        }
    };
}
