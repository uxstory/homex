package com.samsung.android.clab.homex.simulator;

import android.app.Activity;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.Toast;

import com.samsung.android.clab.homex.R;

import java.util.List;

public class TesterActivity extends Activity implements Runnable {
    public static String LOG_TAG = TesterActivity.class.getSimpleName();

    //private static final long ADD_DELAY = 100;

    CanvasView mCanvasView = null;
    Handler mHandler = new Handler();

    private MotionTester mMotionTester;
    private List<Joint> mJointList;

    private Thread thread;

    private Boolean mKeepRunning;

    public static Context mContext;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.simulator_activity);

        String fileName = getIntent().getStringExtra("FileName");
        mMotionTester = new MotionTester();
        mMotionTester.loadJointData(fileName);
        mJointList = mMotionTester.getSavedJointList();

        if (mJointList.size() <= 0) {
            Toast.makeText(this, "no Joint data in XML", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

        mKeepRunning = true;

        mContext = this;

    }

    @Override
    public void onResume() {
        super.onResume();

        mCanvasView = findViewById(R.id.canvasView);

        if (mJointList.size() > 0) {
            thread = new Thread(this);
            thread.start();
        }

    }

    @Override
    public void onPause () {
        super.onPause();
    }

    public void run(){
        int count = mJointList.size();
        int jointIndex = 0;
        long startTime = mJointList.get(0).mTime;
        long totlaTime = mJointList.get(count-1).mTime - startTime;


        while(true){
            if (mKeepRunning == false) {
                continue;
            }
            Joint joint = mJointList.get(jointIndex);
            float[] fx = joint.getConvertedValueX(mMotionTester.mMinX, mMotionTester.mMaxX);
            float[] fy = joint.getConvertedValueY(mMotionTester.mMinY, mMotionTester.mMaxY);
            float[] fz = joint.getConvertedValueZ(mMotionTester.mMinY, mMotionTester.mMaxY);
            TesterActivity.this.mCanvasView.updateSkeletonInformation(fx, fy, fz, totlaTime, joint.mTime-startTime);
            jointIndex++;
            Log.v(LOG_TAG, "TesterActivity onDraw index :" + jointIndex);
            try {
                if (jointIndex >= count) {
                    jointIndex = 0;
                    mKeepRunning = false;
                } if (jointIndex > 0) {
                    Joint currentJoint = mJointList.get(jointIndex-1);
                    Joint nextJoint = mJointList.get(jointIndex);
                    long msec = nextJoint.mTime - currentJoint.mTime;
                    Thread.sleep(msec);
                }
            } catch (InterruptedException ex) {
                //Log.v(TAG, "InterruptedException while sleeping");
            }

        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        final int action = event.getActionMasked();
        if (action == MotionEvent.ACTION_UP) {

            if (mKeepRunning == false) {
                mKeepRunning = true;
            } else {
                mKeepRunning = false;
            }
        }
        return super.onTouchEvent(event);
    }
}

