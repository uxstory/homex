package com.samsung.android.clab.homex;

public class HistoryDetailData {

    private String date;
    private String dayOfWeek;
    private String program;
    private int exerciseTime;
    private int[] heartRate;
    private int kcal;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(String dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public String getProgram() {
        return program;
    }

    public void setProgram(String program) {
        this.program = program;
    }

    public int getExerciseTime() {
        return exerciseTime;
    }

    public void setExerciseTime(int exerciseTime) {
        this.exerciseTime = exerciseTime;
    }

    public int[] getHeartRate() {
        return heartRate;
    }

    public void setHeartRate(int[] heartRate) {
        this.heartRate = heartRate;
    }

    public int getKcal() {
        return kcal;
    }

    public void setKcal(int kcal) {
        this.kcal = kcal;
    }
}
