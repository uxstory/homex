package com.samsung.android.clab.homex.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.samsung.android.clab.homex.Course;
import com.samsung.android.clab.homex.R;

import java.util.ArrayList;

public class ContentsAdapter extends RecyclerView.Adapter<ContentsAdapter.ContentsViewHolder> {

    private Context mContext;
    private  ArrayList<Course> mContentsList;
    private AdapterListener mListener;

    public interface AdapterListener {
        void onItemClick(Course item, int position);
    }

    public static class ContentsViewHolder extends RecyclerView.ViewHolder
    {
        public ImageView mImagerView;
        public TextView mTextView1;
        public TextView mTextView2;

        public ContentsViewHolder(View itemView)
        {
            super(itemView);
            mImagerView = itemView.findViewById(R.id.icon_mycontents);

            mTextView1 = itemView.findViewById(R.id.title_exercise);
            mTextView2  = itemView.findViewById(R.id.title_step);
        }

    }


    public ContentsAdapter(Context context, ArrayList<Course> exampleList)
    {
        this.mContext = context;
        mContentsList = exampleList;
    }


    @NonNull
    @Override
    public ContentsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewtype) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_mycontents, parent, false);
        ContentsViewHolder cvh = new ContentsViewHolder(v);

        return  cvh;

    }

    @Override
    public void onBindViewHolder(@NonNull ContentsViewHolder holder, int position) {
        Course currentItem = mContentsList.get(position);

        int resID = mContext.getResources().getIdentifier(currentItem.getIcon(), "drawable", mContext.getPackageName());
        holder.mImagerView.setImageResource(resID);
        holder.mTextView1.setText(currentItem.getTitle());

        StringBuilder sb = new StringBuilder();
        sb.append(currentItem.getStep());
        sb.append("setp(s) / ");
        sb.append(currentItem.getSet());
        sb.append("set(s) / for ");
        sb.append(currentItem.getLevel());
        holder.mTextView2.setText(sb.toString());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mListener != null) {
                    mListener.onItemClick(currentItem, position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mContentsList.size();
    }

    public void setAdapterListener(AdapterListener listener) {
        this.mListener = listener;
    }
}
