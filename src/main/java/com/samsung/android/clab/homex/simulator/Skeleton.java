package com.samsung.android.clab.homex.simulator;

public class Skeleton {

    public static class Skeletal {
        public int x;
        public int y;
    }

    private Skeletal[] mSkeletals;
    private int maxJoints = JointType.JOINT_TYPE_MAX.ordinal();

    public static class SkeletalOrigin {
        public float x;
        public float y;
        public float z;

        @Override
        public String toString() {
            return String.format("%.3f", x) + ", " + String.format("%.3f", y) + ", " + String.format("%.1f", z);
            //return Float.toString(x) + ", " + Float.toString(y) + ", " + Float.toString(z);
        }
    }

    private SkeletalOrigin[] mSkeletalsOrigin;


    public Skeleton() {
        mSkeletals = new Skeletal[maxJoints];

        for (int i=0; i<maxJoints; i++) {
            mSkeletals[i] = new Skeletal();
        }

        mSkeletalsOrigin = new SkeletalOrigin[maxJoints];

        for (int i=0; i<maxJoints; i++) {
            mSkeletalsOrigin[i] = new SkeletalOrigin();
        }
    }

    public void setSkeleton(final float[] fx, final float[] fy, final float[] fz, final int width, final int height) {
        for (int i=0; i<maxJoints; i++) {
            mSkeletals[i].x = (int)(fx[i] * width);
            mSkeletals[i].y = (int)(fy[i] * height);
            mSkeletalsOrigin[i].x = fx[i];
            mSkeletalsOrigin[i].y = fy[i];
            mSkeletalsOrigin[i].z = fz[i];
        }
    }

    public Skeletal get(JointType jointType) {
        return mSkeletals[jointType.ordinal()];
    }

    public SkeletalOrigin getOriginValue(JointType jointType) {
        return mSkeletalsOrigin[jointType.ordinal()];
    }

    public String getOriginValueDistance(JointType jointTypeA, JointType jointTypeB) {
        Float distanceX, distanceY, distanceZ;
        SkeletalOrigin SkeletalOriginA = mSkeletalsOrigin[jointTypeA.ordinal()];
        SkeletalOrigin SkeletalOriginB = mSkeletalsOrigin[jointTypeB.ordinal()];

        if (SkeletalOriginB.x > SkeletalOriginA.x) {
            distanceX = SkeletalOriginB.x - SkeletalOriginA.x;
        } else {
            distanceX = SkeletalOriginA.x - SkeletalOriginB.x;
        }
        if (SkeletalOriginB.y > SkeletalOriginA.y) {
            distanceY = SkeletalOriginB.y - SkeletalOriginA.y;
        } else {
            distanceY = SkeletalOriginA.y - SkeletalOriginB.y;
        }
        if (SkeletalOriginB.z > SkeletalOriginA.z) {
            distanceZ = SkeletalOriginB.z - SkeletalOriginA.z;
        } else {
            distanceZ = SkeletalOriginA.z - SkeletalOriginB.z;
        }

        //return Float.toString(distanceX) + ", " + Float.toString(distanceY) + ", " + Float.toString(distanceZ);
        return String.format("%.3f", distanceX) + ", " + String.format("%.3f", distanceY) + ", " + String.format("%.1f", distanceZ);
    }
}

