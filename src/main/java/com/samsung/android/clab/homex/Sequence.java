package com.samsung.android.clab.homex;

import java.util.List;

public class Sequence {

    private int index;
    private String description;

    private boolean refered;
    private SuccessMethod successMethod;

    private String actionimg;

    public String getActionimg() {
        return actionimg;
    }

    public void setActionimg(String actionimg) {
        this.actionimg = actionimg;
    }

    List<CheckMethod> passList;
    List<CheckMethod> successList;

    public SuccessMethod getSuccessMethod() {
        return successMethod;
    }

    public void setSuccessMethod(SuccessMethod successMethod) {
        this.successMethod = successMethod;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<CheckMethod> getPassList() {
        return passList;
    }

    public void setPassList(List<CheckMethod> passList) {
        this.passList = passList;
    }

    public List<CheckMethod> getSuccessList() {
        return successList;
    }

    public void setSuccessList(List<CheckMethod> successList) {
        this.successList = successList;
    }

    public boolean isRefered() {
        return refered;
    }

    public void setRefered(boolean refered) {
        this.refered = refered;
    }

    @Override
    public String toString()
    {
        String methodName = "";

        if(successMethod !=null)
        {
            methodName = successMethod.getMethod();
        }
        return "index : " + index + "\n" + "desc : " + description + "\n" + "methodName : " + methodName;
    }
}
