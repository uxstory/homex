package com.samsung.android.clab.homex.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.samsung.android.clab.homex.Program;
import com.samsung.android.clab.homex.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProgramAdapter extends RecyclerView.Adapter<ProgramAdapter.ProgramHolder> {

    private ArrayList<Program> mItems;
    private AdapterListener mListener;
    private Context mContext;

    public interface AdapterListener {
        void onItemClick(Program item);
    }

    public ProgramAdapter(ArrayList<Program> items, AdapterListener listener) {
        this.mItems = items;
        this.mListener = listener;
    }

    @NonNull
    @Override
    public ProgramHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        mContext = viewGroup.getContext() ;

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.program_item, viewGroup, false) ;

        return new ProgramHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProgramHolder viewHolder, int position) {
        Program item = mItems.get(position);

//        Glide.with(this)
//                .load(profileFile)
//                .apply(RequestOptions.circleCropTransform())
//                .into(viewHolder.imageViewIcon);

        int resID = mContext.getResources().getIdentifier(item.getIcon(), "drawable", mContext.getPackageName());
        viewHolder.imageViewIcon.setImageResource(resID);
        viewHolder.textViewTitle.setText(item.getTitle());

        StringBuilder sb = new StringBuilder();
        sb.append(item.getSteps());
        sb.append("step(s), for ");
        sb.append(item.getLevel());
        viewHolder.textViewLevel.setText(sb.toString());

        viewHolder.itemView.setOnClickListener(v -> {
            if(mListener != null) {
                mListener.onItemClick(item);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    class ProgramHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imageViewIcon)
        ImageView imageViewIcon;
        @BindView(R.id.textViewTitle)
        TextView textViewTitle;
        @BindView(R.id.textViewLevel)
        TextView textViewLevel;

        public ProgramHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
